﻿using CedarAPI.App_Start;
using DataManagement.Repository;
using DataManagement.Repository.Interfaces;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using System;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace CedarAPI.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUserRepository>().To<UserRepository>().InRequestScope();
            kernel.Bind<IInboxRepository>().To<InboxRepository>().InRequestScope();
            kernel.Bind<IWORepository>().To<WORepository>().InRequestScope();
            kernel.Bind<IPersonRepository>().To<PersonRepository>().InRequestScope();
            kernel.Bind<IFailureRepository>().To<FailureRepository>().InRequestScope();
            kernel.Bind<IQRCodeRepository>().To<QRCodeRepository>().InRequestScope();
            kernel.Bind<IWRRepository>().To<WRRepository>().InRequestScope();
            kernel.Bind<IDeptRepository>().To<DeptRepository>().InRequestScope();
            kernel.Bind<ISiteRepository>().To<SiteRepository>().InRequestScope();
            kernel.Bind<IPartRepository>().To<PartRepository>().InRequestScope();
            kernel.Bind<IStoreRepository>().To<StoreRepository>().InRequestScope();
            kernel.Bind<IPartTypesRepository>().To<PartTypesRepository>().InRequestScope();
            kernel.Bind<IExpenseTypeRepository>().To<ExpenseTypeRepository>().InRequestScope();
            kernel.Bind<IDashboardRepository>().To<DashboardRepository>().InRequestScope();
            kernel.Bind<IMasterRepository>().To<MasterRepository>().InRequestScope();
            //kernel.Bind<IRepo>().ToMethod(ctx => new Repo("Ninject Rocks!"));
        }
    }
}