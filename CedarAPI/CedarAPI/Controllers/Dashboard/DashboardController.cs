﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/Dashboard")]
    public class DashboardController : ApiController
    {
        IDashboardRepository _dashboardRepository;
        public DashboardController(IDashboardRepository dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        private CedarEntities db = new CedarEntities();
        [Route("Get")]
        public HttpResponseMessage Get([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                DataManagement.Entities.Dashboard dashboard = new DataManagement.Entities.Dashboard();
              
                dashboard = _dashboardRepository.Get(DashboardParams.SiteNo, DashboardParams.WherMaintenanceCost, DashboardParams.WherTotalDowntTime,
                    DashboardParams.WherWorkCompliancy, DashboardParams.WherNumberOfWorkOverDue, DashboardParams.WherNumberOfWorkBacklogOver,
                    DashboardParams.WherNumberOfRepetitiveFailedEQ, DashboardParams.DeptNo);

                DashboardInfo DashboardInfo = new DashboardInfo();
                DashboardInfo.MaintenanceCost = InputVal.ToDecimal(dashboard.ValMaintenanceCost).ToString("N2");
                DashboardInfo.TotalDowntTime = InputVal.ToDecimal(dashboard.ValTotalDowntTime).ToString("N2");
                DashboardInfo.WorkCompliancy = InputVal.ToDecimal(dashboard.ValWorkCompliancy).ToString("P2");
                DashboardInfo.NumberOfWorkOverDue = InputVal.ToDecimal(dashboard.ValNumberOfWorkOverDue).ToString("N0");
                DashboardInfo.NumberOfWorkBacklog = InputVal.ToDecimal(dashboard.ValNumberOfWorkBacklog).ToString("N0");
                DashboardInfo.NumberOfWorkBacklogOver = InputVal.ToDecimal(dashboard.ValNumberOfWorkBacklogOver).ToString("N0");
                DashboardInfo.NumberOfRepetitiveFailedEQ = InputVal.ToDecimal(dashboard.ValNumberOfRepetitiveFailedEQ).ToString("N0");

                return Request.CreateResponse(HttpStatusCode.OK, DashboardInfo);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("GetDept")]
        public HttpResponseMessage GetDept([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                /*
                */
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter PersonNo = new SqlParameter("@PersonNo", DashboardParams.PersonNo);



                var resp = db.Database.SqlQuery<DeptInfo>("sp_Dashboard_Dept_Retrive @PersonNo ," +
                    "@SiteNo  ", SiteNo, PersonNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("Condition")]
        public HttpResponseMessage GetCondition([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                string p_path = HttpContext.Current.Server.MapPath("~");
                int path_level = Convert.ToInt32(WebConfigurationManager.AppSettings["PathLevel"]);
                for (int j = 0; j <= path_level; j++)
                {
                    p_path = Directory.GetParent(p_path).ToString();
                }

                p_path += WebConfigurationManager.AppSettings["dashboardPath"] + "\\";

                string FileName = p_path + DashboardParams.PersonNo + ".xml";

                if (!File.Exists(FileName))
                {
                    FileName = p_path + "0.xml";
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(FileName);
                int cnt = 0;
                ConditionInfo ConditionInfo = new ConditionInfo();
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    if (node != null)
                    {
                        SetValueToControl(node.ChildNodes[cnt].InnerText, node.ChildNodes[cnt + 1].InnerText, ref ConditionInfo);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ConditionInfo);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        protected void SetValueToControl(string controlID, string value, ref ConditionInfo ConditionInfo)
        {
            if (controlID == "cboSite")
            {
                ConditionInfo.cboSite = Convert.ToInt32(value);
            }
            if (controlID == "cboDept")
            {
                ConditionInfo.cboDept = Convert.ToInt32(value);
            }
            else if (controlID == "cboMaintainCost")
            {
                ConditionInfo.cboMaintainCost = value;
            }
            else if (controlID == "cboDowntime")
            {
                ConditionInfo.cboDowntime = value;
            }
            else if (controlID == "cboWorkCompliancy")
            {
                ConditionInfo.cboWorkCompliancy = value;
            }
            else if (controlID == "cboNumberOfWorkOverDue")
            {
                ConditionInfo.cboNumberOfWorkOverDue = value;
            }
            else if (controlID == "cboNumberOfWorkBacklogOver")
            {
                ConditionInfo.cboNumberOfWorkBacklogOver = value;
            }
            else if (controlID == "cboNumberOfRepetitiveFailedEQ")
            {
                ConditionInfo.cboNumberOfRepetitiveFailedEQ = value;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter PUNo = new SqlParameter("@PUNO", DashboardParams.PUNo);
                SqlParameter Where = new SqlParameter("@Wher", DashboardParams.Where);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter Type = new SqlParameter("@Type", DashboardParams.Type);
                SqlParameter Field = new SqlParameter("@Field", InputVal.ToString(DashboardParams.Field));
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.WODate,
                   DashboardParams.WOTYPECODE,
                   DashboardParams.DEPTCODE,
                   DashboardParams.PUCODE,
                   DashboardParams.EQCode,
                   DashboardParams.MH,
                    DashboardParams.MHCost,
                    DashboardParams.MStock,
                    DashboardParams.MDirect,
                    DashboardParams.OutService,
                    DashboardParams.TotalCost));


                var resp = db.Database.SqlQuery<MaintainCostInfo>("sp_Dashboard_Mobile_LV2 @SiteNo ," +
                  " @PUNO , @Wher, @DeptNo, @Type, @Field, @SortBy ", SiteNo, PUNo, Where, DeptNo, Type, Field, SortBy);

                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string WODATE, string WOTYPECODE, string DEPTCODE, string PUCODE, string EQCode, string MH, string MHCost, string MStock, string MDirect, string OutService, string TotalCost)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(WODATE) != "") sort.AppendFormat("  WODATE {0} , ", WODATE);
            if (InputVal.ToString(WOTYPECODE) != "") sort.AppendFormat("  WOTYPECODE {0} , ", WOTYPECODE);
            if (InputVal.ToString(DEPTCODE) != "") sort.AppendFormat("  DEPTCODE {0} , ", DEPTCODE);
            if (InputVal.ToString(PUCODE) != "") sort.AppendFormat("  PUCODE {0} , ", PUCODE);
            if (InputVal.ToString(EQCode) != "") sort.AppendFormat("  EQCode {0} , ", EQCode);
            if (InputVal.ToString(MH) != "") sort.AppendFormat("  MH {0} , ", MH);
            if (InputVal.ToString(MHCost) != "") sort.AppendFormat("  MHCost {0} , ", MHCost);
            if (InputVal.ToString(MStock) != "") sort.AppendFormat(" MaterialStock {0} , ", MStock);
            if (InputVal.ToString(MDirect) != "") sort.AppendFormat(" MaterialDirectPurchase {0} , ", MDirect);
            if (InputVal.ToString(OutService) != "") sort.AppendFormat(" OutsourceServices {0} , ", OutService);
            if (InputVal.ToString(TotalCost) != "") sort.AppendFormat(" TotalCost {0} , ", TotalCost);
            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }


    }
}
