﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/MaintainCost")]
    public class MaintainCostController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage GetLV1([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherMaintenanceCost = new SqlParameter("@WherMaintenanceCost", DashboardParams.WherMaintenanceCost);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.MH,
                    DashboardParams.MHCost,
                    DashboardParams.MStock,
                    DashboardParams.MDirect,
                    DashboardParams.OutService,
                    DashboardParams.TotalCost));



                var resp = db.Database.SqlQuery<MaintainCostInfo>("sp_Dashboard_Mobile_MaintainCost_LV1 @SiteNo ," +
                    "@WherMaintenanceCost, @DeptNo, @GroupBy, @SortBy  ", SiteNo, WherMaintenanceCost, DeptNo, GroupBy, SortBy);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter PUNo = new SqlParameter("@PUNO", DashboardParams.PUNo);
                SqlParameter WherMaintenanceCost = new SqlParameter("@WherMaintenanceCost", DashboardParams.WherMaintenanceCost);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);



                var resp = db.Database.SqlQuery<MaintainCostInfo>("sp_Dashboard_Mobile_MaintainCost_LV2 @SiteNo ," +
                    " @PUNO , @WherMaintenanceCost, @DeptNo ", SiteNo, WherMaintenanceCost, DeptNo, PUNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        public string GenerateSort(string MH, string MHCost, string MStock, string MDirect, string OutService, string TotalCost)
        {
            StringBuilder sort = new StringBuilder();
           
            if (InputVal.ToString(MH) != "") sort.AppendFormat("  MH {0} , ", MH);
            if (InputVal.ToString(MHCost) != "") sort.AppendFormat("  MHCost {0} , ", MHCost);
            if (InputVal.ToString(MStock) != "") sort.AppendFormat(" MaterialStock {0} , ", MStock);
            if (InputVal.ToString(MDirect) != "") sort.AppendFormat(" MaterialDirectPurchase {0} , ", MDirect);
            if (InputVal.ToString(OutService) != "") sort.AppendFormat(" OutsourceServices {0} , ", OutService);
            if (InputVal.ToString(TotalCost) != "") sort.AppendFormat(" TotalCost {0} , ", TotalCost);
            string s = " ";
            if(sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }


    }
}
