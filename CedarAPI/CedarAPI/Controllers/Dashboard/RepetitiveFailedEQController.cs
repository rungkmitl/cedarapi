﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/RepetitiveFailedEQ")]
    public class RepetitiveFailedEQController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage Get([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherNumberOfRepetitiveFailedEQ = new SqlParameter("@WherRepetitiveFailedEQ", DashboardParams.WherNumberOfRepetitiveFailedEQ);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.Total));

                var resp = db.Database.SqlQuery<WorkBacklogInfo>("sp_Dashboard_Mobile_RepetitiveFailedEQ_LV1 @SiteNo ," +
                    "@WherRepetitiveFailedEQ, @DeptNo, @GroupBy, @SortBy  ", SiteNo, WherNumberOfRepetitiveFailedEQ, DeptNo, GroupBy, SortBy);

                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string Total)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(Total) != "") sort.AppendFormat("  Total {0} , ", Total);

            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }


    }
}
