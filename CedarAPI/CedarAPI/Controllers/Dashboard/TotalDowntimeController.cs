﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/TotalDT")]
    public class TotalDowntimeController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage GetLV1([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherTotalDowntTime = new SqlParameter("@WherTotalDowntime", DashboardParams.WherTotalDowntTime);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.NoOfShutDown, DashboardParams.DT_Duration));



                var resp = db.Database.SqlQuery<TotalDTInfo>("sp_Dashboard_Mobile_TotalDowntime_LV1 @SiteNo ," +
                    "@WherTotalDowntime, @DeptNo, @GroupBy, @SortBy  ", SiteNo, WherTotalDowntTime, DeptNo, GroupBy, SortBy);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter PUNo = new SqlParameter("@PUNO", DashboardParams.PUNo);
                SqlParameter WherTotalDowntTime = new SqlParameter("@WherTotalDowntime", DashboardParams.WherTotalDowntTime);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);



                var resp = db.Database.SqlQuery<TotalDTInfo>("sp_Dashboard_TotalDowntime_LV2 @SiteNo ," +
                    " @PUNO , @WherTotalDowntime, @DeptNo ", SiteNo, WherTotalDowntTime, DeptNo, PUNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string NoOfShutDown, string DT_Duration)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(NoOfShutDown) != "") sort.AppendFormat("  NoOfShutDown {0} , ", NoOfShutDown);
            if (InputVal.ToString(DT_Duration) != "") sort.AppendFormat("  DT_Duration {0} , ", DT_Duration);
            
            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }
    }
}
