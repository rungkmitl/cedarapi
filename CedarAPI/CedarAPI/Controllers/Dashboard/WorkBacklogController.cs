﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/WorkBacklog")]
    public class WorkBacklogController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage GetLV1([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.Total));


                var resp = db.Database.SqlQuery<WorkBacklogInfo>("sp_Dashboard_Mobile_WorkBacklog_LV1 @SiteNo ," +
                    " @DeptNo, @GroupBy, @SortBy  ", SiteNo, DeptNo, GroupBy, SortBy);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter WOTypeNo = new SqlParameter("@WOTypeNo", DashboardParams.WOTypeNo);
                SqlParameter WOStatusNo = new SqlParameter("@WOStatusNo", DashboardParams.WOStatusNo);


            var resp = db.Database.SqlQuery<WODetailInfo>("sp_Dashboard_WorkBacklog_LV2 @SiteNo ," +
                    "  @DeptNo , @WOTypeNo, @WOStatusNo ", SiteNo, DeptNo, WOTypeNo, WOStatusNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string Total)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(Total) != "") sort.AppendFormat("  Total {0} , ", Total);
            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }
    }
}
