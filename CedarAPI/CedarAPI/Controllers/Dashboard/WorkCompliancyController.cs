﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/WorkCompliancy")]
    public class WorkCompliancyController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage GetLV1([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherWorkCompliancy = new SqlParameter("@WherWorkCompliancy", DashboardParams.WherWorkCompliancy);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.Total, DashboardParams.Finish, DashboardParams.NonFinish));



                var resp = db.Database.SqlQuery<WorkCompliancyInfo>("sp_Dashboard_Mobile_WorkCompliancy_LV1 @SiteNo ," +
               "@WherWorkCompliancy, @DeptNo, @GroupBy, @SortBy    ", SiteNo, WherWorkCompliancy, DeptNo, GroupBy, SortBy);

                
                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherWorkCompliancy = new SqlParameter("@WherWorkCompliancy", DashboardParams.WherWorkCompliancy);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter WOTypeNo = new SqlParameter("@WOTypeNo", DashboardParams.WOTypeNo);
              

                var resp = db.Database.SqlQuery<WODetailInfo>("sp_Dashboard_WorkCompliancy_LV2 @SiteNo ," +
                    " @WherWorkCompliancy, @DeptNo , @WOTypeNo, Total ", SiteNo, WherWorkCompliancy, DeptNo, WOTypeNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string Total, string  Finish, string NonFinish)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(Total) != "") sort.AppendFormat("  Total {0} , ", Total);
            if (InputVal.ToString(Finish) != "") sort.AppendFormat("  Finish {0} , ", Finish);
            if (InputVal.ToString(NonFinish) != "") sort.AppendFormat("  NonFinish {0} , ", NonFinish);

            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }
    }
}
