﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using CedarAPI.Models.Dashboard;
using System.Text;

namespace CedarAPI.Controllers.Dashboard
{
    [RoutePrefix("api/WorkOverDue")]
    public class WorkOverDueController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("LV1")]
        public HttpResponseMessage GetLV1([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherWorkOverDue = new SqlParameter("@WherWorkOverDue", DashboardParams.WherNumberOfWorkOverDue);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter GroupBy = new SqlParameter("@GroupBy", DashboardParams.GroupBy);
                SqlParameter SortBy = new SqlParameter("@SortBy", GenerateSort(DashboardParams.OverDue, DashboardParams.NonPlan));

                var resp = db.Database.SqlQuery<WorkOverDueInfo>("sp_Dashboard_Mobile_WorkOverDue_LV1 @SiteNo ," +
                    "@WherWorkOverDue, @DeptNo, @GroupBy, @SortBy  ", SiteNo, WherWorkOverDue, DeptNo, GroupBy, SortBy);


            
                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("LV2")]
        public HttpResponseMessage GetLV2([FromUri] DashboardParams DashboardParams)
        {
            try
            {
                SqlParameter SiteNo = new SqlParameter("@SiteNo", DashboardParams.SiteNo);
                SqlParameter WherWorkCompliancy = new SqlParameter("@WherWorkOverDue", DashboardParams.WherNumberOfWorkOverDue);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", DashboardParams.DeptNo);
                SqlParameter WOTypeNo = new SqlParameter("@WOTypeNo", DashboardParams.WOTypeNo);
              

                var resp = db.Database.SqlQuery<WODetailInfo>("sp_Dashboard_WorkOverDue_LV2 @SiteNo ," +
                    " @WherWorkOverDue, @DeptNo , @WOTypeNo, Total ", SiteNo, WherWorkCompliancy, DeptNo, WOTypeNo);



                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        public string GenerateSort(string OverDue, string NonPlan)
        {
            StringBuilder sort = new StringBuilder();

            if (InputVal.ToString(OverDue) != "") sort.AppendFormat("  OverDue {0} , ", OverDue);
            if (InputVal.ToString(NonPlan) != "") sort.AppendFormat("  NonPlan {0} , ", NonPlan);
            
            string s = " ";
            if (sort.Length != 0)
            {
                s = " order by " + sort.ToString().Substring(0, sort.ToString().Length - 2);
            }
            return s;
        }
    }
}
