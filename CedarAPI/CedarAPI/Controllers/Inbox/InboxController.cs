﻿using CedarAPI.Models.Master;
using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataManagement.Entities;
using System.Web;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers.Inbox
{
    [RoutePrefix("api/Inbox")]
    public class InboxController : ApiController
    {
        IInboxRepository _inboxRepository;
        public InboxController(IInboxRepository inboxRepository)
        {
            _inboxRepository = inboxRepository;
        }
        public class Params
        {
            public int personNo { get; set; }
            public string where { get; set; }
            public int DocNo { get; set; }
            public int EventNo { get; set; }
            public string WFType { get; set; }
        }

        [Route("ByPerson")]
        [HttpGet]
        public HttpResponseMessage Inbox([FromUri] Params Params)
        {
            DataManagement.Entities.Inbox inboxObj = new DataManagement.Entities.Inbox();
            inboxObj.UnReadTotal = _inboxRepository.GetUnRead(Params.personNo).UnReadTotal;
            inboxObj.InboxList = _inboxRepository.Inbox(Params.personNo, Params.where);
            return Request.CreateResponse(HttpStatusCode.OK, inboxObj);
        }

        [Route("Read")]
        [HttpPost]
        public HttpResponseMessage Read([FromBody] Params Params)
        {
            _inboxRepository.CheckRead(Params.DocNo, Params.EventNo, Params.WFType);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
