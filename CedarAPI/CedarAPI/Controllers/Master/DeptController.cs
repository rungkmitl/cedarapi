﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/Dept")]
    public class DeptController : ApiController
    {
        IDeptRepository _deptRepository;
        public DeptController(IDeptRepository deptRepository)
        {
            _deptRepository = deptRepository;
        }

        private CedarEntities db = new CedarEntities();

        [Route("Maint/Site/{SiteNo:int}")]
        [HttpGet]
        public HttpResponseMessage GetBySiteNo(int SiteNo)
        {
            string p_strWhere = "";

            p_strWhere = " WHERE Dept_Site.SiteNo=" + SiteNo;
            p_strWhere += " And me.FLAGDEL='F' AND me.FlagSection='T' Order By me.DEPTCODE";

            string p_strSql = " Select me.DEPTNO AS No,me.DEPTCODE AS Code,me.DEPTNAME AS Name, " +
                " me.DEPTPARENT,par.DEPTCODE AS DEPTPARENTCODE,par.DEPTNAME AS DEPTPARENTNAME," +
                " me.COSTCENTERNO,COSTCENTERCODE,COSTCENTERNAME," +
                " me.FLAGSECTION," +
                " me.FLAGDEL" +
                " FROM  Dept me" +
                " left Join Dept par on me.DEPTPARENT = par.DEPTNO" +
                " left Join CostCenter on me.CostCenterNo = CostCenter.CostCenterNo" +
                " INNER JOIN Dept_Site" +
                " ON me.DEPTNO = Dept_Site.DeptNo " + p_strWhere;


            var result = db.Database.SqlQuery<DeptInfo>(p_strSql);

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public class Params
        {
            public int SiteNo { get; set; }
            public int PersonNo { get; set; }
        }

        [Route("Dashboard")]
        [HttpGet]
        public HttpResponseMessage GetForDashboard([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _deptRepository.GetForDashboard(Params.PersonNo, Params.SiteNo));

        }
    }
}