﻿using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{

    public class EQController : ApiController
    {
        public class Params
        {
            public int SiteNo { get; set; }
            public int PersonNo { get; set; }
            public int PUNo { get; set; }
            public string Where { get; set; }
        }
        public class EQInfo
        {
            public int EQNO { get; set; }
            public string EQCODE { get; set; }
            public string EQNAME { get; set; }
            public int PUNO { get; set; }
            public string PUCODE { get; set; }
            public string PUNAME { get; set; }
            public int COSTCENTERNO { get; set; }
            public string COSTCENTERCODE { get; set; }
            public string COSTCENTERNAME { get; set; }
            public int SITENO { get; set; }
        }
        private CedarEntities db = new CedarEntities();

       
        public HttpResponseMessage Get([FromUri] Params Params)
        {
            string p_strSql = "";
            string p_strPU = "";

            if (Params.PUNo > 0)
            {
                p_strPU = " and EQ.PUNO = " + Params.PUNo;
            }

            p_strSql = @"select top 30 EQ.EQNO,EQ.EQCODE,EQ.EQNAME
                                            ,EQ.PUNO,PU.PUCODE, PU.PUNAME
                                            ,isnull(PU.COSTCENTERNO,0) as COSTCENTERNO,COSTCENTER.COSTCENTERCODE, COSTCENTER.COSTCENTERNAME
                                            ,EQ.SITENO
                                    from person_site
                                    inner join EQ on person_site.SiteNO= EQ.SiteNo and EQ.SiteNo=" + Params.SiteNo +
                                 @"left join PU on eq.puno = pu.puno
                                    left join costcenter on pu.costcenterno = costcenter.costcenterno 
                                    
                                    where  person_site.SiteNo>1 and person_site.PERSONNO = " + Params.PersonNo + p_strPU;

            if (Params.Where != "")
            {
                p_strSql += " and EQ.EQCODE like '%" + Params.Where + "%'";
            }

            var result = db.Database.SqlQuery<EQInfo>(p_strSql);






            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}