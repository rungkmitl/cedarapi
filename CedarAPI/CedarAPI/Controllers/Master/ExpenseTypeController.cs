﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/ExpenseType")]
    public class ExpenseTypeController : ApiController
    {
        IExpenseTypeRepository _expenseTypeRepository;
        public ExpenseTypeController(IExpenseTypeRepository expenseTypeRepository)
        {
            _expenseTypeRepository = expenseTypeRepository;
        }

        public class Params
        {
            public string where { get; set; }
            public int personNo { get; set; }
        }

        [Route("Site/{SiteNo:int}")]
        [HttpGet]
        public HttpResponseMessage BySite(int SiteNo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _expenseTypeRepository.GetBySite(SiteNo));
        }
    }
}