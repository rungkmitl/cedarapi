﻿using CedarAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    public class FailureModesController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        public HttpResponseMessage Get()
        {
            var result = from f in db.FailureModes
                         where f.FlagDel == "F"
                         select new
                         {
                             No = f.FailureModeNo,
                             Code = f.FailureModeCode,
                             Name = f.FailureModeName
                         };

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}