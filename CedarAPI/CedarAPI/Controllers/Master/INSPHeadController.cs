﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/INSPHead")]
    public class INSPHeadController : ApiController
    {
        IMasterRepository _MasterRepository;
        public INSPHeadController(IMasterRepository masterRepository)
        {
            _MasterRepository = masterRepository;
        }
        public class Params
        {
            public int SiteNo { get; set; }
            public string Where { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage BySite([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _MasterRepository.GetINSHead(Params.SiteNo, Params.Where));
        }
    }
}
