﻿using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
  
    public class PUController : ApiController
    {
        public class Params
        {
            public double SiteNo { get; set; }
            public double PersonNo { get; set; }
            public string Where { get; set; }
        }
        public class PUInfo
        {
            public int PUNO { get; set; }
            public string PUCODE { get; set; }
            public string PUNAME { get; set; }
            public int COSTCENTERNO { get; set; }
            public string COSTCENTERCODE { get; set; }
            public string COSTCENTERNAME { get; set; }
            public int SITENO { get; set; }
        }
        private CedarEntities db = new CedarEntities();
        
        public HttpResponseMessage Get([FromUri] Params Params)
        {
            string p_strSql = "";


            p_strSql = "select top 30 PU.PUNO,PU.PUCODE,PU.PUNAME," +
               " PU.COSTCENTERNO,COSTCENTERCODE,COSTCENTERNAME," +
               " PU.SITENO" +
               " from person_site" +
               " inner join PU on person_site.SiteNO = PU.SiteNo and PU.SiteNo = " + Params.SiteNo +
               " left join costcenter on pu.costcenterno = costcenter.costcenterno" +
               " where person_site.SiteNo > 1 and person_site.PERSONNO = " + Params.PersonNo;

            if(Params.Where != "")
            {
                p_strSql += " and PU.PUCODE like '%" + Params.Where + "%'";
            }

            var result = db.Database.SqlQuery<PUInfo>(p_strSql);

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}