﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/Part")]
    public class PartController : ApiController
    {
        IPartRepository _partRepository;
        public PartController(IPartRepository partRepository)
        {
            _partRepository = partRepository;
        }

        public class Params
        {
            public string where { get; set; }
        }

        [Route("Person/{PersonNo:int}")]
        [HttpGet]
        public HttpResponseMessage ByPerson(int PersonNo, [FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _partRepository.GetByPerson(PersonNo, Params.where));
        }
    }
}