﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/PartTypes")]
    public class PartTypesController : ApiController
    {
        IPartTypesRepository _partTypesRepository;
        public PartTypesController(IPartTypesRepository partTypesRepository)
        {
            _partTypesRepository = partTypesRepository;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _partTypesRepository.Get());
        }
    }
}