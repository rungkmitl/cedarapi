﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/Person")]
    public class PersonController : ApiController
    {
        IPersonRepository _personRepository;
        public PersonController(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public class Params
        {
            public int siteNo { get; set; }
            public string where { get; set; }
        }
    
        [Route("BySite")]
        [HttpGet]
        public HttpResponseMessage GetBySiteNo([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _personRepository.GetBySite(Params.siteNo, Params.where));
        }
    }
}