﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/Site")]
    public class SiteController : ApiController
    {
        ISiteRepository _siteRepository;
        public SiteController(ISiteRepository siteRepository)
        {
            _siteRepository = siteRepository;
        }

        [Route("ByPerson/{PersonNo:int}")]
        [HttpGet]
        public HttpResponseMessage ByPerson(int PersonNo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _siteRepository.ByPerson(PersonNo));
        }

        
        [HttpGet]
        public HttpResponseMessage ById(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _siteRepository.ById(id));
        }

        
    }
}