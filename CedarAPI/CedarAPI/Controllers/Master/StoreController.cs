﻿using CedarAPI.Models;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/Store")]
    public class StoreController : ApiController
    {
        IStoreRepository _storeRepository;
        public StoreController(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public class Params
        {
            public string where { get; set; }
            public int personNo { get; set; }
        }

        [Route("Part/{PartNo:int}")]
        [HttpGet]
        public HttpResponseMessage ByPerson(int PartNo, [FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _storeRepository.GetByPart(PartNo, Params.personNo, Params.where));
        }
    }
}