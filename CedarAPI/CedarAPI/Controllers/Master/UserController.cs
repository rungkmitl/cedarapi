﻿using CedarAPI.Models.Master;
using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataManagement.Entities;
using System.Web;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        IUserRepository _userRepository;
        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("Login")]
        [HttpGet]
        public HttpResponseMessage Login([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _userRepository.Login(Params.lang, Params.uid, Params.pwd));
        }
    }
    public class Params
    {
        public string lang { get; set; }
        public string uid { get; set; }
        public string pwd { get; set; }
    }
}
