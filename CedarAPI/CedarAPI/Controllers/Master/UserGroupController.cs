﻿using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/UserGroup")]
    public class UserGroupController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("Maint")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var result = from u in db.USERGROUP
                         where u.FlagSection == "T"
                         select new
                         {
                             No = u.USERGROUPNO,
                             Code = u.USERGROUPCODE,
                             Name = u.USERGROUPNAME
                         };

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}