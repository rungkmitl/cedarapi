﻿using CedarAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers.Master
{
    [RoutePrefix("api/WOAction")]
    public class WOActionController : ApiController
    {
        IMasterRepository _MasterRepository;
        public WOActionController(IMasterRepository masterRepository)
        {
            _MasterRepository = masterRepository;
        }
        public class Params
        {
            public int SiteNo { get; set; }
            public string Where { get; set; }
        }
       
        [HttpGet]
        public HttpResponseMessage BySite([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _MasterRepository.GetWOAction(Params.SiteNo, Params.Where));
        }
    }
}