﻿using CedarAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    public class WOProblemController : ApiController
    {
        private CedarEntities db = new CedarEntities();
        
        public HttpResponseMessage Get()
        {
            var result = from p in db.WOProblem
                         where p.FLAGDEL == "F"
                         select new
                         {
                             No = p.WOPROBLEMNO,
                             Code = p.WOPROBLEMCODE,
                             Name = p.WOPROBLEMNAME
                         };
           
            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}