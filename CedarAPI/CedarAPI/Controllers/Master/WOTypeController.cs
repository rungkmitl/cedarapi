﻿using CedarAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CedarAPI.Controllers.Master
{
    public class WOTypeController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        public HttpResponseMessage Get()
        {
            var result = from t in db.WOType
                         where t.FLAGDEL == "F"
                         select new
                         {
                             No = t.WOTYPENO,
                             Code = t.WOTYPECODE,
                             Name = t.WOTYPENAME
                         };

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}