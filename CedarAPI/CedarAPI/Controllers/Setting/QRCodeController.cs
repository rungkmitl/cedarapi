﻿using CedarAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using CedarAPI.Models.Params;
using System;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers
{
    [RoutePrefix("api/QRCode")]
    public class QRCodeController : ApiController
    {
        IQRCodeRepository _qrcodeRepository;
        public QRCodeController(IQRCodeRepository qrcodeRepository)
        {
            _qrcodeRepository = qrcodeRepository;
        }

        [Route("TypeByCode")]
        [HttpGet]
        public HttpResponseMessage InsertSignIn([FromUri] string code)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _qrcodeRepository.GetQRType(code));
        }       
    }
}