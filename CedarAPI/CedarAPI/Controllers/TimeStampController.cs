﻿using CedarAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using CedarAPI.Models.Params;
using System;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers
{
    [RoutePrefix("api/TimeStamp")]
    public class TimeStampController : ApiController
    {
        IWORepository _woRepository;
        public TimeStampController(IWORepository woRepository)
        {
            _woRepository = woRepository;
        }

        CedarEntities db = new CedarEntities();
        [Route("SignIn")]
        [HttpPost]
        public HttpResponseMessage InsertSignIn([FromBody] TimeStampParams TimeStampParams)
        {
            try
            {
                using (CedarEntities db = new CedarEntities())
                {
                    TimeStamp TimeStamp = new TimeStamp();
                    TimeStamp.TimeStampDate = DateTime.Now;
                    TimeStamp.PUCode = TimeStampParams.PUCode;
                    TimeStamp.UpdatedBy = TimeStampParams.UpdatedBy;

                    db.TimeStamp.Add(TimeStamp);
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK);

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
        [Route("SignOut")]
        [HttpPost]
        public HttpResponseMessage InsertSignOut([FromBody] TimeStampParams TimeStampParams)
        {
            try
            {
                int personNO = _woRepository.CheckPincode(TimeStampParams.WONo, TimeStampParams.PinCode);
                if (personNO != 0)
                {
                    _woRepository.InsertSignout(TimeStampParams.PUCode, TimeStampParams.FlagCheckJob, TimeStampParams.WOCode, TimeStampParams.WONo, personNO, TimeStampParams.Remark);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, -99);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }
}