﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CedarAPI.Models;
using System.Web.Configuration;
using System.IO;

namespace CedarAPI.Controllers
{
    public class UploadImageController : ApiController
    {
        public HttpResponseMessage Post()
        {
            ObjUpload ObjUpload = new ObjUpload();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        string extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string p_path = HttpContext.Current.Server.MapPath("~");
                            int path_level = Convert.ToInt32(WebConfigurationManager.AppSettings["PathLevel"]);
                            for (int j = 0; j <= path_level; j++)
                            {
                                p_path = Directory.GetParent(p_path).ToString();
                            }

                            p_path += WebConfigurationManager.AppSettings["attPath"] + "\\";

                            string uid_name = Guid.NewGuid().ToString();
                            string FileName = p_path + uid_name + extension;

                            while (File.Exists(FileName))
                            {
                                uid_name = Guid.NewGuid().ToString();
                                FileName = p_path + uid_name + System.IO.Path.GetExtension(postedFile.FileName).ToString();
                            }

                            postedFile.SaveAs(FileName);

                            ObjUpload.filename = uid_name + System.IO.Path.GetExtension(postedFile.FileName).ToString();
                            ObjUpload.status = "success";
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ObjUpload); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                ObjUpload.errMsg = ex.Message;
                return Request.CreateResponse(HttpStatusCode.NotFound, ObjUpload);
            }
        }
    }
}
