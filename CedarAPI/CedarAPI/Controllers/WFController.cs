﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using CommonTools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers
{

    [RoutePrefix("api/WF")]
    public class WFController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("Action_WR_GWO")]
        [HttpPost]
        public HttpResponseMessage GenerateWO([FromBody] WFParams WFParams)
        {
            try
            {
                SqlParameter WONO = new SqlParameter();
                WONO.ParameterName = "@WONO";
                WONO.Direction = ParameterDirection.Output;
                WONO.SqlDbType = SqlDbType.Int;


                SqlParameter WRNO = new SqlParameter("@WRNO", WFParams.WRNO);
                SqlParameter WOTypeNo = new SqlParameter("@WOTypeNo", WFParams.WOTypeNo);
                SqlParameter DeptNo = new SqlParameter("@DeptNo", WFParams.DeptNo);
                SqlParameter ASSIGN = new SqlParameter("@ASSIGN", WFParams.ASSIGN);
                SqlParameter USERGROUPNO = new SqlParameter("@USERGROUPNO", WFParams.USERGROUPNO);
                SqlParameter UPDATEUSER = new SqlParameter("@UPDATEUSER", WFParams.UPDATEUSER);
                SqlParameter VendorNo = new SqlParameter("@VendorNo", WFParams.VendorNo);

          
                var resp = db.Database.ExecuteSqlCommand("sp_WF_WR_Generate_WO @WONO OUT, @WRNO, @WOTypeNo, @DeptNo, @ASSIGN, @USERGROUPNO, @UPDATEUSER, @VendorNo"
                    , WONO, WRNO, WOTypeNo, DeptNo, ASSIGN, USERGROUPNO, UPDATEUSER, VendorNo);

                return Request.CreateResponse(HttpStatusCode.OK, WONO.Value);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("Action/WO/{WONo:int}/Person/{PersonNo:int}")]
        [HttpGet]
        public HttpResponseMessage WOGetAction(int WONo, int PersonNo)
        {
            try
            {
                SqlParameter WONO = new SqlParameter("@WONO", WONo);
                SqlParameter USERNO = new SqlParameter("@USERNO", PersonNo);

                List<ActionObject> actionList = db.Database.SqlQuery<ActionObject>("mobile_WO_GetAction @WONO, @USERNO", WONO, USERNO).ToList();

                foreach (ActionObject item in actionList)
                {
                    item.RECEIVER = this.GetActionSendTo(WONo, item.ACTIONNO, PersonNo);
                }

                return Request.CreateResponse(HttpStatusCode.OK, actionList);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        public string GetActionSendTo(int wono, int actno, int personno)
        {
            SqlParameter WONO = new SqlParameter("@DOCNO", wono);
            SqlParameter ACTIONNO = new SqlParameter("@ACTIONNO", actno);
            SqlParameter UPDATEUSER = new SqlParameter("@UPDATEUSER", personno);

            ActionObject actionObject = db.Database.SqlQuery<ActionObject>("sp_WFN_EXEC_ROUTE_GETSENDER_WO @DOCNO, @ACTIONNO, @UPDATEUSER"
                , WONO, ACTIONNO, UPDATEUSER).FirstOrDefault();

            if(actionObject != null)
            {
                if(actionObject.SENDTO_GROUP == "")
                {
                    return actionObject.SENDTO_GROUP;
                }
                else
                {
                    return actionObject.SENDTO_PERSON;
                }
            }

            return "";
        }

        [Route("Action_WR")]
        [HttpGet]
        public HttpResponseMessage ActionWR([FromUri] WFParams WFParams)
        {
            try
            {
                var res = db.Database.ExecuteSqlCommand("exec sp_WFN_EXEC_ACTION_WR @DOCNO , @ACTIONNO, @UPDATEUSER"
                    , new SqlParameter("@DOCNO", WFParams.DOCNO)
                    , new SqlParameter("@ACTIONNO", WFParams.ACTIONNO)
                    , new SqlParameter("@UPDATEUSER", WFParams.UPDATEUSER));



                  res = db.Database.ExecuteSqlCommand("exec sp_WFN_EXEC_NODE_WR @WRNO , @ACTIONNO, @DESC, @UPDATEUSER"
                  , new SqlParameter("@WRNO", WFParams.DOCNO)
                  , new SqlParameter("@ACTIONNO", WFParams.ACTIONNO)
                  , new SqlParameter("@DESC", WFParams.DESC)
                    , new SqlParameter("@UPDATEUSER", WFParams.UPDATEUSER));


                string p_strSql = @"select wrno,wrcode,wrdate,wrdesc
                                     ,isnull(wrstatuscode+'  '+wrstatusname,'') as wrstatus
                                     ,nodename
                                     ,wr.puno,pucode
                                     ,wr.eqno,eqcode
                                     ,wr.costcenterno,costcentercode
                                        ,wr.siteno,isnull(sitecode+' - '+sitename,'') as sitename
                                        ,wr.dept_rec,deptcode
                                    from wr 
                                    left join wrstatus on wrstatus.wrstatusno = wr.wrstatusno
                                    left join wf_node on wf_node.NODENO = wr.wfnodeno
                                    left join pu on pu.puno = wr.puno
                                    left join eq on eq.eqno = wr.eqno
                                    left join costcenter on costcenter.costcenterno = wr.costcenterno
                                    left join site on wr.siteno = site.siteno
                                    left join dept on wr.dept_rec = dept.deptno
                                    WHERE WRNO=" + WFParams.DOCNO;

                var result = db.Database.SqlQuery<WRInfo>(p_strSql);


                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)    
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("Action_WO")]
        [HttpPost]
        public HttpResponseMessage ActionWO([FromUri] WFParams WFParams)
        {
            try
            {
                var res = db.Database.ExecuteSqlCommand("exec sp_WFN_EXEC_ACTION_WO @DOCNO , @ACTIONNO, @UPDATEUSER"
                    , new SqlParameter("@DOCNO", WFParams.DOCNO)
                    , new SqlParameter("@ACTIONNO", WFParams.ACTIONNO)
                    , new SqlParameter("@UPDATEUSER", WFParams.UPDATEUSER));



                res = db.Database.ExecuteSqlCommand("exec sp_WFN_EXEC_NODE_WO @WONO , @ACTIONNO, @DESC, @UPDATEUSER"
                , new SqlParameter("@WONO", WFParams.DOCNO)
                , new SqlParameter("@ACTIONNO", WFParams.ACTIONNO)
                , new SqlParameter("@UPDATEUSER", WFParams.UPDATEUSER)
                , new SqlParameter("@DESC", WFParams.DESC));


                return Request.CreateResponse(HttpStatusCode.OK, WFParams.DOCNO);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }


    }
}


