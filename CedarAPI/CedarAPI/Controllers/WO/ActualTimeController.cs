﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using DataManagement.Repository.Interfaces;

namespace CedarAPI.Controllers.WO
{
    public class ActualTimeController : ApiController
    {
        private CedarEntities db = new CedarEntities();
        IActualTimeRepository _actualTimeRepository;
        public ActualTimeController(IActualTimeRepository actualTimeRepository)
        {
            _actualTimeRepository = actualTimeRepository;
        }
        public HttpResponseMessage Get(int id)
        {
            var result = (from wo in db.WO
                          where wo.WONO == id
                          select new
                          {
                              ACT_START_D = wo.ACT_START_D,
                              ACT_START_T = wo.ACT_START_T,
                              ACT_FINISH_D = wo.ACT_FINISH_D,
                              ACT_FINISH_T = wo.ACT_FINISH_T,
                              ACT_DURATION = CommonTools.DurationVal.ToFloat(wo.ACT_DURATION.HasValue? Convert.ToDecimal(wo.ACT_DURATION.Value) : 0)

                          }).FirstOrDefault();


            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        
        public HttpResponseMessage Put(int id, [FromBody] WOParams WOParams)
        {
            DateData DateData = new DateData();

            try
            {
                using (db)
                {
                    var woRow = from p in db.WO
                                where p.WONO == id
                                select p;

                    foreach (var wo in woRow)
                    {
                        wo.METERDONE = InputVal.ToFloat(WOParams.Meter);
                        if (WOParams.Start != "")
                        {
                            CommonTools.InputVal.ConvertStartDateToString(WOParams.Start, ref DateData);
                            wo.ACT_START_D = DateData.StartD;
                            wo.ACT_START_T = DateData.StartT;
                        }
                        else
                        {
                            wo.ACT_START_D = null;
                            wo.ACT_START_T = null;
                        }

                        if (WOParams.End != "")
                        {
                            CommonTools.InputVal.ConvertFinshDateToString(WOParams.End, ref DateData);
                            wo.ACT_FINISH_D = DateData.FinishD;
                            wo.ACT_FINISH_T = DateData.FinishT;
                            wo.ACT_DURATION = InputVal.ToFloat(WOParams.Duration.ToString().Replace(":","."));//CommonTools.InputVal.CalculateDuration(DateData.StartDate, DateData.FinishDate);
                        }
                        else
                        {
                            wo.ACT_FINISH_D = null;
                            wo.ACT_FINISH_T = null;
                            wo.ACT_DURATION = 0;
                        }

                        //_actualTimeRepository.Update(wo.WONO, wo.ACT_START_D, wo.ACT_START_T
                        //    , wo.ACT_FINISH_D, wo.ACT_FINISH_T, wo.ACT_DURATION, wo.ACT_START_T
                        //    , wo.ACT_START_T, wo.ACT_START_T, wo.ACT_START_T, wo.ACT_START_T)
                    }
                    //db.SaveChanges();
                    //wo.ACT_FINISH_T
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }
}
