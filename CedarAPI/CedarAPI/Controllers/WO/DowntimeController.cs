﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using CommonTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers.WO
{
    public class DowntimeController : ApiController
    {
        [Route("UpdateDownTime")]
        [HttpPost]
        public HttpResponseMessage UpdateDownTime([FromBody] WOParams WOParams)
        {
            DateData DateData = new DateData();

            try
            {
                using (CedarEntities db = new CedarEntities())
                {
                    var woRow = from p in db.WO
                                where p.WOCODE == WOParams.WOCode && p.SiteNo == WOParams.SiteNo
                                select p;
                    foreach (var wo in woRow)
                    {
                        wo.FLAGEQ = InputVal.BoolToString(WOParams.FlagEQ);
                        wo.FlagPU = InputVal.BoolToString(WOParams.FlagPU);
                        wo.FlagSafety = InputVal.BoolToString(WOParams.FlagSafety);
                        wo.FlagEnvironment = InputVal.BoolToString(WOParams.FlagEnviroment);

                        if (WOParams.FlagPU)
                        {
                            //wo.PUNO_Effected = WOParams.Puno;
                        }

                        if (WOParams.Start != "")
                        {
                            CommonTools.InputVal.ConvertStartDateToString(WOParams.Start, ref DateData);
                            wo.DT_Start_D = DateData.StartD;
                            wo.DT_Start_T = DateData.StartT;
                        }
                        else
                        {
                            wo.DT_Start_D = null;
                            wo.DT_Start_T = null;
                        }

                        if (WOParams.End != "")
                        {
                            CommonTools.InputVal.ConvertStartDateToString(WOParams.End, ref DateData);
                            wo.DT_Finish_D = DateData.FinishD;
                            wo.DT_Finish_T = DateData.FinishT;
                            wo.DT_Duration = CommonTools.InputVal.CalculateDuration(DateData.StartDate, DateData.FinishDate);
                        }
                        else
                        {
                            wo.DT_Finish_D = null;
                            wo.DT_Finish_T = null;
                            wo.DT_Duration = 0;
                        }
                    }
                    db.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }
}
