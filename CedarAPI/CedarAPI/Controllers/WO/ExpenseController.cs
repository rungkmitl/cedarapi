﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Data.SqlClient;

namespace CedarAPI.Controllers.WO
{
    public class ExpenseController : ApiController
    {
        private CedarEntities db = new CedarEntities();
      
        public HttpResponseMessage Get(int id)
        {
            var result = (from woresource in db.WO_Resource
                          join wo in db.WO on woresource.WONO equals wo.WONO
                          where wo.WONO == id && woresource.OTHERCODE == "1"
                          select new
                          {
                              Travel = (from woresource_sub1 in db.WO_Resource
                                        where woresource.WONO == woresource_sub1.WONO && woresource_sub1.OTHERCODE == "1"
                                        select
                                                   new
                                                   {
                                                       QTY = woresource_sub1.QTY,
                                                       UNITCOST = woresource_sub1.UNITCOST
                                                   }).FirstOrDefault(),
                              Accomodation = (from woresource_sub2 in db.WO_Resource
                                              where woresource.WONO == woresource_sub2.WONO && woresource_sub2.OTHERCODE == "2"
                                              select
                                                         new
                                                         {
                                                             QTY = woresource_sub2.QTY,
                                                             UNITCOST = woresource_sub2.UNITCOST
                                                         }).FirstOrDefault(),
                              Other = (from woresource_sub3 in db.WO_Resource
                                       join attach in db.Attach_Doc on woresource_sub3.WO_RESCNO equals attach.DOCNO into wa
                                       from resource_attach in wa.DefaultIfEmpty()
                                       where woresource.WONO == woresource_sub3.WONO && woresource_sub3.OTHERCODE == "3"
                                       select
                                                  new
                                                  {
                                                      QTY = woresource_sub3.QTY,
                                                      UNITCOST = woresource_sub3.UNITCOST,
                                                      NAME = woresource_sub3.NAME,
                                                      FileName = resource_attach.UNIQUENAME + resource_attach.EXTENSION
                                                  }).FirstOrDefault(),
                              Allowance = (from woresource_sub4 in db.WO_Resource
                                           where woresource.WONO == woresource_sub4.WONO && woresource_sub4.OTHERCODE == "4"
                                           select
                                                      new
                                                      {
                                                          QTY = woresource_sub4.QTY,
                                                          UNITCOST = woresource_sub4.UNITCOST,
                                                          NAME = woresource_sub4.NAME,
                                                      }).FirstOrDefault(),
                              TravelOT = (from woresource_sub5 in db.WO_Resource
                                          where woresource.WONO == woresource_sub5.WONO && woresource_sub5.OTHERCODE == "5"
                                          select
                                                     new
                                                     {
                                                         QTY = woresource_sub5.QTY,
                                                         UNITCOST = woresource_sub5.UNITCOST,
                                                         NAME = woresource_sub5.NAME,
                                                     }).FirstOrDefault()

                          }).FirstOrDefault();

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }



        public HttpResponseMessage Post([FromBody] WOParams WOParams)
        {
            try
            {
                using (CedarEntities db = new CedarEntities())
                {
                    List<WO_Resource> result = (from woresource in db.WO_Resource
                                                join wo in db.WO on woresource.WONO equals wo.WONO
                                                where wo.WOCODE == WOParams.WOCode && wo.SiteNo == WOParams.SiteNo
                                                && (woresource.OTHERCODE == "1" || woresource.OTHERCODE == "2" || woresource.OTHERCODE == "3"
                                                 || woresource.OTHERCODE == "4" || woresource.OTHERCODE == "5")
                                                select woresource).ToList();


                    CedarAPI.Models.WO woresult = (from wo in db.WO
                                                   where wo.WOCODE == WOParams.WOCode && wo.SiteNo == WOParams.SiteNo
                                                   select wo).FirstOrDefault();


                    WO_Resource woresResult = (from woresource in db.WO_Resource
                                               join wo in db.WO on woresource.WONO equals wo.WONO
                                               where wo.WOCODE == WOParams.WOCode && wo.SiteNo == WOParams.SiteNo
                                               select woresource).FirstOrDefault();



                    if (result.Count() == 0)
                    {

                        WO_Resource WOResource = new WO_Resource();

                        DateTime now = DateTime.Now;
                        WOResource.TRDATE = FieldDateTime.DateToString(now);
                        WOResource.TRTime = FieldDateTime.TimeToString(now);
                        WOResource.WONO = woresult.WONO;
                        WOResource.PARTNO = 0;
                        WOResource.OTHERCODE = "1";
                        WOResource.OPNO = woresResult != null ? woresResult.OPNO : 0;
                        WOResource.SymptomNo = woresult.SymptomNo;
                        WOResource.STORENO = 0;
                        WOResource.RESCTYPE = "P";
                        WOResource.NAME = "ค่าที่พัก";
                        WOResource.UNITCOST = WOParams.amtTravel;
                        WOResource.QTY = WOParams.qtyTravel;
                        WOResource.AMOUNT = WOParams.amtTravel * WOParams.qtyTravel;
                        WOResource.EXPNO = 6;
                        WOResource.FLAGACT = "T";
                        WOResource.PartTypeCode = "";
                        WOResource.VendorNo = 0;
                        WOResource.REMARK = "";
                        WOResource.DirectPurchase = "T";
                        WOResource.FlagNotPartCode = 1;
                        WOResource.UNIT = "";

                        db.WO_Resource.Add(WOResource);

                        WO_Resource WOResourceAccomodation = new WO_Resource();
                        WOResourceAccomodation.TRDATE = FieldDateTime.DateToString(now);
                        WOResourceAccomodation.TRTime = FieldDateTime.TimeToString(now);
                        WOResourceAccomodation.WONO = woresult.WONO;
                        WOResourceAccomodation.PARTNO = 0;
                        WOResourceAccomodation.OTHERCODE = "2";
                        WOResourceAccomodation.OPNO = woresResult != null ? woresResult.OPNO : 0;
                        WOResourceAccomodation.SymptomNo = woresult.SymptomNo;
                        WOResourceAccomodation.STORENO = 0;
                        WOResourceAccomodation.RESCTYPE = "P";
                        WOResourceAccomodation.NAME = "ค่าเดินทาง";
                        WOResourceAccomodation.UNITCOST = WOParams.amtAccomodation;
                        WOResourceAccomodation.QTY = WOParams.qtyAccomodation;
                        WOResourceAccomodation.AMOUNT = WOParams.amtAccomodation * WOParams.qtyAccomodation;
                        WOResourceAccomodation.EXPNO = 6;
                        WOResourceAccomodation.FLAGACT = "T";
                        WOResourceAccomodation.PartTypeCode = "";
                        WOResourceAccomodation.VendorNo = 0;
                        WOResourceAccomodation.REMARK = "";
                        WOResourceAccomodation.DirectPurchase = "T";
                        WOResourceAccomodation.FlagNotPartCode = 1;
                        WOResourceAccomodation.UNIT = "";

                        db.WO_Resource.Add(WOResourceAccomodation);

                        WO_Resource WOResourceOther = new WO_Resource();
                        WOResourceOther.TRDATE = FieldDateTime.DateToString(now);
                        WOResourceOther.TRTime = FieldDateTime.TimeToString(now);
                        WOResourceOther.WONO = woresult.WONO;
                        WOResourceOther.PARTNO = 0;
                        WOResourceOther.OTHERCODE = "3";
                        WOResourceOther.OPNO = woresResult != null ? woresResult.OPNO : 0;
                        WOResourceOther.SymptomNo = woresult.SymptomNo;
                        WOResourceOther.STORENO = 0;
                        WOResourceOther.RESCTYPE = "P";
                        WOResourceOther.NAME = WOParams.nameOther;
                        WOResourceOther.UNITCOST = WOParams.amtOther;
                        WOResourceOther.QTY = WOParams.qtyOther;
                        WOResourceOther.AMOUNT = WOParams.amtOther * WOParams.qtyOther;
                        WOResourceOther.EXPNO = 6;
                        WOResourceOther.FLAGACT = "T";
                        WOResourceOther.PartTypeCode = "";
                        WOResourceOther.VendorNo = 0;
                        WOResourceOther.REMARK = "";
                        WOResourceOther.DirectPurchase = "T";
                        WOResourceOther.FlagNotPartCode = 1;
                        WOResourceOther.UNIT = "";


                        db.WO_Resource.Add(WOResourceOther);

                        WO_Resource WOAllowance = new WO_Resource();
                        WOAllowance.TRDATE = FieldDateTime.DateToString(now);
                        WOAllowance.TRTime = FieldDateTime.TimeToString(now);
                        WOAllowance.WONO = woresult.WONO;
                        WOAllowance.PARTNO = 0;
                        WOAllowance.OTHERCODE = "4";
                        WOAllowance.OPNO = woresResult != null ? woresResult.OPNO : 0;
                        WOAllowance.SymptomNo = woresult.SymptomNo;
                        WOAllowance.STORENO = 0;
                        WOAllowance.RESCTYPE = "P";
                        WOAllowance.NAME = "เบี้ยเลี้ยง";
                        WOAllowance.UNITCOST = WOParams.amtAllowance;
                        WOAllowance.QTY = WOParams.qtyAllowance;
                        WOAllowance.AMOUNT = WOParams.amtAllowance * WOParams.qtyAllowance;
                        WOAllowance.EXPNO = 6;
                        WOAllowance.FLAGACT = "T";
                        WOAllowance.PartTypeCode = "";
                        WOAllowance.VendorNo = 0;
                        WOAllowance.REMARK = "";
                        WOAllowance.DirectPurchase = "T";
                        WOAllowance.FlagNotPartCode = 1;
                        WOAllowance.UNIT = "";

                        db.WO_Resource.Add(WOAllowance);


                        WO_Resource WOResourceTravelOT = new WO_Resource();
                        WOResourceTravelOT.TRDATE = FieldDateTime.DateToString(now);
                        WOResourceTravelOT.TRTime = FieldDateTime.TimeToString(now);
                        WOResourceTravelOT.WONO = woresult.WONO;
                        WOResourceTravelOT.PARTNO = 0;
                        WOResourceTravelOT.OTHERCODE = "5";
                        WOResourceTravelOT.OPNO = woresResult != null ? woresResult.OPNO : 0;
                        WOResourceTravelOT.SymptomNo = woresult.SymptomNo;
                        WOResourceTravelOT.STORENO = 0;
                        WOResourceTravelOT.RESCTYPE = "P";
                        WOResourceTravelOT.NAME = "ค่าเดินทางนอกเวลา";
                        WOResourceTravelOT.UNITCOST = WOParams.amtTravelOT;
                        WOResourceTravelOT.QTY = WOParams.qtyTravelOT;
                        WOResourceTravelOT.AMOUNT = WOParams.amtTravelOT * WOParams.qtyTravelOT;
                        WOResourceTravelOT.EXPNO = 6;
                        WOResourceTravelOT.FLAGACT = "T";
                        WOResourceTravelOT.PartTypeCode = "";
                        WOResourceTravelOT.VendorNo = 0;
                        WOResourceTravelOT.REMARK = "";
                        WOResourceTravelOT.DirectPurchase = "T";
                        WOResourceTravelOT.FlagNotPartCode = 1;
                        WOResourceTravelOT.UNIT = "";

                        db.WO_Resource.Add(WOResourceTravelOT);

                        db.SaveChanges();

                        if (WOParams.filename != "")
                        {
                            string p_path = HttpContext.Current.Server.MapPath("~");
                            int path_level = Convert.ToInt32(WebConfigurationManager.AppSettings["PathLevel"]);
                            for (int j = 0; j <= path_level; j++)
                            {
                                p_path = Directory.GetParent(p_path).ToString();
                            }
                            p_path += WebConfigurationManager.AppSettings["attPath"] + "\\";

                            string ext = System.IO.Path.GetExtension(WOParams.filename); ;
                            string uid = WOParams.filename.Replace(ext, "");

                            int id = WOResourceOther.WO_RESCNO;
                            SqlParameter FormID = new SqlParameter("@FormID", "WORES_OTHER01");
                            SqlParameter DocNo = new SqlParameter("@DocNo", id);
                            SqlParameter UNIQUENAME = new SqlParameter("@UNIQUENAME", uid);
                            SqlParameter Extension = new SqlParameter("@EXTENSION", ext);
                            SqlParameter SiteNo = new SqlParameter("@Site", WOParams.SiteNo);
                            SqlParameter UpdateUser = new SqlParameter("@UpdateUser", 0);


                            db.Database.ExecuteSqlCommand("delete from Attach_Doc where DOCNO = @DocNo EXEC sp_Attach_Mobile_Insert @FormID, @DocNo, @UNIQUENAME, @EXTENSION, @Site, @UpdateUser",
                                                                  FormID, DocNo, UNIQUENAME, Extension, SiteNo, UpdateUser);
                        }
                    }
                    else
                    {
                        foreach (WO_Resource item in result)
                        {
                            if (item.OTHERCODE == "1")
                            {
                                item.UNITCOST = WOParams.amtTravel;
                                item.QTY = WOParams.qtyTravel;
                                item.AMOUNT = WOParams.amtTravel * WOParams.qtyTravel;
                            }

                            else if (item.OTHERCODE == "2")
                            {
                                item.UNITCOST = WOParams.amtAccomodation;
                                item.QTY = WOParams.qtyAccomodation;
                                item.AMOUNT = WOParams.amtAccomodation * WOParams.qtyAccomodation;
                            }

                            else if (item.OTHERCODE == "3")
                            {
                                item.NAME = WOParams.nameOther;
                                item.UNITCOST = WOParams.amtOther;
                                item.QTY = WOParams.qtyOther;
                                item.AMOUNT = WOParams.amtOther * WOParams.qtyOther;

                                if (WOParams.filename != "")
                                {
                                    string p_path = HttpContext.Current.Server.MapPath("~");
                                    int path_level = Convert.ToInt32(WebConfigurationManager.AppSettings["PathLevel"]);
                                    for (int j = 0; j <= path_level; j++)
                                    {
                                        p_path = Directory.GetParent(p_path).ToString();
                                    }
                                    p_path += WebConfigurationManager.AppSettings["attPath"] + "\\";

                                    string ext = System.IO.Path.GetExtension(WOParams.filename); ;
                                    string uid = WOParams.filename.Replace(ext, "");

                                    SqlParameter FormID = new SqlParameter("@FormID", "WORES_OTHER01");
                                    SqlParameter DocNo = new SqlParameter("@DocNo", item.WO_RESCNO);
                                    SqlParameter UNIQUENAME = new SqlParameter("@UNIQUENAME", uid);
                                    SqlParameter Extension = new SqlParameter("@EXTENSION", ext);
                                    SqlParameter SiteNo = new SqlParameter("@Site", WOParams.SiteNo);
                                    SqlParameter UpdateUser = new SqlParameter("@UpdateUser", 1);


                                    db.Database.ExecuteSqlCommand("delete from Attach_Doc where DOCNO = @DocNo EXEC sp_Attach_Mobile_Insert @FormID, @DocNo, @UNIQUENAME, @EXTENSION, @Site, @UpdateUser",
                                                                  FormID, DocNo, UNIQUENAME, Extension, SiteNo, UpdateUser);
                                }
                            }
                            else if (item.OTHERCODE == "4")
                            {
                                item.UNITCOST = WOParams.amtAllowance;
                                item.QTY = WOParams.qtyAllowance;
                                item.AMOUNT = WOParams.amtAllowance * WOParams.qtyAllowance;
                            }
                            else if (item.OTHERCODE == "5")
                            {
                                item.UNITCOST = WOParams.amtTravelOT;
                                item.QTY = WOParams.qtyTravelOT;
                                item.AMOUNT = WOParams.amtTravelOT * WOParams.qtyTravelOT;
                            }
                            db.SaveChanges();
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK);

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

    }
}
