﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;
using DataManagement.Repository.Interfaces;
using DataManagement.Entities;

namespace CedarAPI.Controllers.WO
{
    [RoutePrefix("api/Failure")]
    public class FailureController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        IFailureRepository _failureRepository;
        public FailureController(IFailureRepository failureRepository)
        {
            _failureRepository = failureRepository;
        }
      

     
        public HttpResponseMessage Put(int id, [FromBody] WOParams WOParams)
        {
            _failureRepository.UpdateSummaryAction(WOParams.WONo, WOParams.problemNo, WOParams.problemDesc
                , WOParams.actionNo, WOParams.actionDesc, WOParams.causeNo, WOParams.causeDesc, WOParams.actStartDate
                , WOParams.actEndDate, WOParams.assignNo, WOParams.dtStartDate, WOParams.dtEndDate, id, WOParams.TaskProcedure);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
