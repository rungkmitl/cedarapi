﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers.WO
{
    [RoutePrefix("api/Material")]
    public class MaterialController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("WO/{WONo:int}/Site/{SiteNo:int}")]
        [HttpGet]
        public HttpResponseMessage GetMaterial(int WONo, int SiteNo)
        {
            var result = from woresource in db.WO_Resource
                         join wo in db.WO on woresource.WONO equals wo.WONO
                         join catalog in db.Iv_Catalog on woresource.PARTNO equals catalog.PARTNO
                         where wo.WONO == WONo && wo.SiteNo == SiteNo && woresource.FLAGACT == "F"
                         select new
                         {
                             PARTNO = catalog.PARTNO,
                             PARTCODE = catalog.PARTCODE,
                             PARTNAME = catalog.PARTNAME,
                             IMG = catalog.IMG,
                             WO_RESCNO = woresource.WO_RESCNO,
                             QTY = woresource.QTY,
                             RefNo = woresource.RefNo,
                             WO_RESC_ACTUAL = (from woresource_sub in db.WO_Resource
                                               where woresource.RefNo == woresource_sub.WO_RESCNO
                                               select
                                                 new
                                                 {
                                                     QTY = woresource_sub.QTY,
                                                     RefNo = woresource_sub.RefNo
                                                 }
                           ).FirstOrDefault()
                         };


            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPut]
        public HttpResponseMessage Put(int id ,[FromBody] WOParams WOParams)
        {
            WO_Resource result = (from woresource in db.WO_Resource
                                  where woresource.WO_RESCNO == id
                                  select woresource).FirstOrDefault();
            try
            {
                if (result.RefNo == null)
                {
                    WO_Resource woresource_new = result;
                    woresource_new.FLAGACT = "T";
                    woresource_new.QTY = WOParams.QTY_USE;
                    woresource_new.RefNo = result.WO_RESCNO;

                    db.WO_Resource.Add(woresource_new);
                    db.SaveChanges();
                    int newid = woresource_new.WO_RESCNO;

                    result = (from woresource in db.WO_Resource
                              where woresource.WO_RESCNO == id
                              select woresource).FirstOrDefault();

                    result.RefNo = newid;
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, newid);

                }
                else
                {
                    WO_Resource woresource_edit = (from p in db.WO_Resource
                                                   where p.WO_RESCNO == result.RefNo
                                                   select p).FirstOrDefault();

                    woresource_edit.QTY = WOParams.QTY_USE;

                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }
}
