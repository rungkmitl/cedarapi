﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CommonTools;

namespace CedarAPI.Controllers.WO
{
    public class PlanTimeController : ApiController
    {
        CedarEntities db = new CedarEntities();

        public HttpResponseMessage Get(int id)
        {
            var result = (from wo in db.WO
                          where wo.WONO== id
                          select new
                          {
                              SCH_START_D = wo.SCH_START_D,
                              SCH_START_T = wo.SCH_START_T,
                              SCH_FINISH_D = wo.SCH_FINISH_D,
                              SCH_FINISH_T = wo.SCH_FINISH_T,
                              SCH_DURATION = wo.SCH_DURATION

                          }).FirstOrDefault();


            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        
        public HttpResponseMessage Put(int id, [FromBody] WOParams WOParams)
        {
            DateData DateData = new DateData();

            try
            {
                using (db)
                {
                    var woRow = from p in db.WO
                                where p.WONO == id
                                select p;
                    foreach (var wo in woRow)
                    {
                        if (WOParams.Start != "")
                        {
                            CommonTools.InputVal.ConvertStartDateToString(WOParams.Start, ref DateData);
                            wo.SCH_START_D = DateData.StartD;
                            wo.SCH_START_T = DateData.StartT;
                        }
                        else
                        {
                            wo.SCH_START_D = null;
                            wo.SCH_START_T = null;
                        }

                        if (WOParams.End != "")
                        {
                            CommonTools.InputVal.ConvertFinshDateToString(WOParams.End, ref DateData);
                            wo.SCH_FINISH_D = DateData.FinishD;
                            wo.SCH_FINISH_T = DateData.FinishT;
                            wo.SCH_DURATION = CommonTools.InputVal.CalculateDuration(DateData.StartDate, DateData.FinishDate);
                        }
                        else
                        {
                            wo.SCH_FINISH_D = null;
                            wo.SCH_FINISH_T = null;
                            wo.SCH_DURATION = 0;
                        }
                    }
                    db.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }
}
