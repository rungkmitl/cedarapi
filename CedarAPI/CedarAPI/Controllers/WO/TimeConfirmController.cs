﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using CommonTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers.WO
{
    [RoutePrefix("api/TimeConfirm")]
    public class TimeConfirmController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("WO/{WONo:int}/Person/{PersonNo:int}")]
        [HttpGet]
        public HttpResponseMessage Get(int WONo, int PersonNo)
        {
            try
            {
                WO_Resource result = (from w in db.WO_Resource
                                      where w.WONO == WONo && w.PersonNo == PersonNo
                                      select w).AsEnumerable()
                          .Select(w => new WO_Resource
                          {
                              TRDATE = w.TRDATE,
                              TRTime = w.TRTime,
                              WO_RESCNO = w.WO_RESCNO,
                              HOURS = DurationVal.ToFloat(w.HOURS),
                          }).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, ex.Message);
                throw;
            }

        }

        // POST: api/TimeConfirm
        public HttpResponseMessage Post([FromBody]TimeConfirmParams TimeConfirmParams)
        {
            try
            {
                CommonTools.DateData DateData = new CommonTools.DateData();
                WO_Resource WO_Resource = new WO_Resource();
                var t = (from task in db.WO_Task
                         where task.WONo == TimeConfirmParams.WONo
                         select task).FirstOrDefault();

                if (TimeConfirmParams.Start != "")
                {
                    CommonTools.InputVal.ConvertStartDateToString(TimeConfirmParams.Start, ref DateData);
                    WO_Resource.TRDATE = DateData.StartD;
                    WO_Resource.TRTime = DateData.StartT;
                }
               

                DateTime now = DateTime.Now;

                WO_Resource.WONO = TimeConfirmParams.WONo;
                WO_Resource.OPNO = t.TaskNo;
                WO_Resource.TSNO = 0;
                WO_Resource.CRAFTNO = 0;
                WO_Resource.TOOLNO = 0;
                WO_Resource.STORENO = 0;
                WO_Resource.STORELOCNO = 0;
                WO_Resource.PARTNO = 0;
                WO_Resource.RESCTYPE = "L";
                WO_Resource.RESCSUBTYPE = "";
                WO_Resource.UNITCOST = 0;
                WO_Resource.QTY = 1;
                WO_Resource.HOURS = CommonTools.FieldDateTime.ConvertDurationToMinute(TimeConfirmParams.Duration);
                WO_Resource.QTYHOURS = WO_Resource.HOURS;
                WO_Resource.AMOUNT = 0;
                WO_Resource.EXPNO = 12;
                WO_Resource.REMARK = "";
                WO_Resource.FLAGACT = "T";
                WO_Resource.CREATEUSER = TimeConfirmParams.PersonNo;
                WO_Resource.CREATEDATE = CommonTools.FieldDateTime.DateToString(now);
                WO_Resource.UPDATEUSER = TimeConfirmParams.PersonNo;
                WO_Resource.UPDATEDATE = WO_Resource.CREATEDATE;
                WO_Resource.TRDNO = 0;
                WO_Resource.MHTypeNo = 4;
                WO_Resource.Factor = 1;
                WO_Resource.UnitRate = 0;
                WO_Resource.VendorNo = 0;
                WO_Resource.Reserved_Flag = "F";
                WO_Resource.PM_No = 0;
                WO_Resource.NotDone_Flag = "F";
                WO_Resource.ServiceLevel_Flag = "F";
                WO_Resource.SymptomNo = 0;
                WO_Resource.PersonNo = TimeConfirmParams.PersonNo;
                WO_Resource.DirectPurchase = "F";
                WO_Resource.FlagNotPartCode = 0;

                db.WO_Resource.Add(WO_Resource);
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, WO_Resource.WO_RESCNO);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
        }

        // PUT: api/TimeConfirm/5
        public HttpResponseMessage Put(int id, [FromBody]TimeConfirmParams TimeConfirmParams)
        {
            try
            {
                WO_Resource WO_Resource = (from res in db.WO_Resource
                              where res.WO_RESCNO == id
                              select res).FirstOrDefault();

                CommonTools.DateData DateData = new CommonTools.DateData();
               
                if (TimeConfirmParams.Start != "")
                {
                    CommonTools.InputVal.ConvertStartDateToString(TimeConfirmParams.Start, ref DateData);
                    WO_Resource.TRDATE = DateData.StartD;
                    WO_Resource.TRTime = DateData.StartT;
                }
                
              
                DateTime now = DateTime.Now;
              
                //WO_Resource.HOURS = CommonTools.FieldDateTime.ConvertDurationToMinute(TimeConfirmParams.Duration);
                WO_Resource.QTYHOURS = WO_Resource.HOURS;
                WO_Resource.UPDATEUSER = TimeConfirmParams.PersonNo;
                WO_Resource.UPDATEDATE = CommonTools.FieldDateTime.DateToString(now);
              
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, WO_Resource);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
        }

    }
}
