﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using CommonTools;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace CedarAPI.Controllers.WO
{
    [RoutePrefix("api/WO")]
    public class WOController : ApiController
    {
        IWORepository _woRepository;
        public WOController(IWORepository woRepository)
        {
            _woRepository = woRepository;
        }

        private CedarEntities db = new CedarEntities();


        [HttpGet]
        public HttpResponseMessage GetList([FromUri] WOParams Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.Get(Params.DeptNo, Params.FlagSection, Params.PersonNo, Params.PersonCode, Params.Where, Params.PUNo, Params.EQNo));
        }
   
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.Get(id));
        }

        #region PlanedTime
        [Route("PlanedTime/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetPlanedTime(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetPlanTime(id));
        }

        [Route("PlanedTime/{id:int}")]
        [HttpPut]
        public HttpResponseMessage UpdatePlanTime(int id, [FromBody] WOParams WOParams)
        {
            _woRepository.UpdatePlanTime(id, WOParams.Start, WOParams.End);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        #endregion


        [Route("FlowHistory/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetFlowHistory(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetFlowHistory(id));
        }

        [Route("Failure/{WONo:int}")]
        [HttpGet]
        public HttpResponseMessage GetFailure(int WONo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetFailure(WONo));
        }

        #region Manhour
        [Route("Manhours/{WONo:int}")]
        [HttpGet]
        public HttpResponseMessage GetManhours(int WONo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetManhours(WONo));
        }


        [Route("Manhours/{id:int}")]
        [HttpPut]
        public HttpResponseMessage UpdatePlanTime(int id, [FromBody] TimeConfirmParams TimeConfirmParams)
        {
            _woRepository.UpdateManhours(id, TimeConfirmParams.Start, TimeConfirmParams.Duration, TimeConfirmParams.PersonNo);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        #endregion

        #region Material
        [Route("Material/{WONo:int}")]
        [HttpGet]
        public HttpResponseMessage GetMaterial(int WONo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetMaterial(WONo));
        }

        [Route("Material/Resc/{WO_RESCNO:int}")]
        [HttpGet]
        public HttpResponseMessage GetMaterialByWORescNo(int WO_RESCNO)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _woRepository.GetMaterialByWORescNo(WO_RESCNO));
        }

        [Route("Material")]
        [HttpPost]
        public HttpResponseMessage InsertMaterial([FromBody] Material Material)
        {
            _woRepository.InsertMaterial(Material);
            return Request.CreateResponse(HttpStatusCode.OK, Material);
        }

        [Route("Material")]
        [HttpPut]
        public HttpResponseMessage UpdateMaterial([FromBody] Material Material)
        {
            _woRepository.UpdateMaterial(Material);
            return Request.CreateResponse(HttpStatusCode.OK, Material);
        }

        [Route("Material/{WO_RESCNO:int}")]
        [HttpDelete]
        public HttpResponseMessage DeleteMaterialByWORescNo(int WO_RESCNO)
        {
            _woRepository.DeleteMaterial(WO_RESCNO);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        #endregion


    }
}
