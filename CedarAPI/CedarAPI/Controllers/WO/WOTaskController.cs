﻿using CedarAPI.Models;
using CedarAPI.Models.Params;
using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CedarAPI.Controllers.WO
{

    [RoutePrefix("api/WOTask")]
    public class WOTaskController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        [Route("WO/{WONo:int}")]
        [HttpGet]
        public HttpResponseMessage GetByWO(int WONo)
        {
            var result = (from task in db.WO_Task
                          join pu in db.PU on task.PUNo equals pu.PUNO into tp
                          from task_pu in tp.DefaultIfEmpty()
                          join eq in db.EQ on task.EQNo equals eq.EQNO into tq
                          from task_eq in tq.DefaultIfEmpty()
                          join action in db.WOAction on task.ActionNo equals action.WOACTIONNO into ta
                          from task_action in ta.DefaultIfEmpty()
                          join ins in db.INSP_HEAD on task.INSPNO equals ins.INSPNO into tin
                          from task_ins in tin.DefaultIfEmpty()
                          where task.WONo == WONo
                          select new
                          {
                              TaskNo = task.TaskNo,
                              TaskCode = task.TaskCode,
                              TaskName = task.TaskName,
                              TaskDate = task.TaskDate,
                              TaskTime = task.TaskTime,
                              FinishDate = task.FinishDate,
                              FinishTime = task.FinishTime,
                              PUNAME = task_pu.PUNAME,
                              EQCODE = task_eq.EQCODE,
                              EQNAME = task_eq.EQNAME,
                              Done = task.Done,
                              AbNormal = task.Abnormal,
                          }).AsEnumerable()
                         .Select(r => new
                         {
                             TaskNo = r.TaskNo,
                             TaskCode = r.TaskCode,
                             TaskName = r.TaskName,
                             TaskTime = r.TaskTime,
                             TaskDate = r.TaskDate,
                             FinishDate = r.FinishDate,
                             FinishTime = r.FinishTime,
                             FlagDone = InputVal.StringToBool(r.Done),
                             FlagAbNormal = InputVal.StringToBool(r.AbNormal),
                             EQCODE = InputVal.ToString(r.EQCODE),
                             EQDesc = r.PUNAME + AppendString(r.EQCODE, r.EQNAME)

                         });

            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var result = (from task in db.WO_Task
                          join pu in db.PU on task.PUNo equals pu.PUNO into tp
                          from task_pu in tp.DefaultIfEmpty()
                          join eq in db.EQ on task.EQNo equals eq.EQNO into tq
                          from task_eq in tq.DefaultIfEmpty()
                          join action in db.WOAction on task.ActionNo equals action.WOACTIONNO into ta
                          from task_action in ta.DefaultIfEmpty()
                          join ins in db.INSP_HEAD on task.INSPNO equals ins.INSPNO into tin
                          from task_ins in tin.DefaultIfEmpty()
                          where task.TaskNo == id
                          select new
                          {
                              TaskNo = task.TaskNo,
                              TaskCode = task.TaskCode,
                              TaskName = task.TaskName,
                              TaskDate = task.TaskDate,
                              TaskTime = task.TaskTime,
                              TaskDuration = task.TaskDuration,
                              FinishDate = task.FinishDate,
                              FinishTime = task.FinishTime,
                              EQNo = task.EQNo,
                              EQDesc = task_eq.EQCODE + ":" + task_eq.EQNAME,
                              PUNo = task.PUNo,
                              PUDesc = task_pu.PUCODE + ":" + task_pu.PUNAME,
                              Component = task.Component,
                              ActionNo = task.ActionNo,
                              ActionDesc = task_action.WOACTIONCODE + ":" + task_action.WOACTIONNAME,
                              ActionDuration = task.ActDuration,
                              Standard = task.Standard,
                              TaskProcedure = task.TaskProcedure,
                              AbnormalNote = task.AbnormalNote,
                              Done = task.Done,
                              AbNormal = task.Abnormal,
                              INSPNO = task.INSPNO,
                              InspDesc = task_ins.INSPCODE + ":" + task_ins.INSPNAME,
                              Remark = task.Remark
                          }).AsEnumerable()
                         .Select(r => new WOTaskInfo
                         {
                             TaskNo = r.TaskNo,
                             TaskCode = r.TaskCode,
                             TaskName = r.TaskName,
                             TaskTime = r.TaskTime,
                             TaskDate = r.TaskDate,
                             TaskDuration = DurationVal.ToFloat(r.TaskDuration),
                             FinishDate = r.FinishDate,
                             FinishTime = r.FinishTime,
                             EQNo = r.EQNo,
                             EQDesc = r.EQDesc,
                             PUNo = r.PUNo,
                             PUDesc = r.PUDesc,
                             Component = r.Component,
                             ActionNo = r.ActionNo,
                             ActionDesc = r.ActionDesc,
                             ActDuration = DurationVal.ToFloat(r.ActionDuration),
                             Standard = r.Standard,
                             TaskProcedure = r.TaskProcedure,
                             AbnormalNote = r.AbnormalNote,
                             FlagDone = InputVal.StringToBool(r.Done),
                             FlagAbNormal = InputVal.StringToBool(r.AbNormal),
                             INSPNO = r.INSPNO,
                             InspDesc = r.InspDesc,
                             Remark = r.Remark

                         }).FirstOrDefault();



            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody] WOTaskParams WOTaskParams)
        {
            try
            {
                using (CedarEntities db = new CedarEntities())
                {
                    if(WOTaskParams.TaskNo != 0)
                    {
                        var taskRow = from p in db.WO_Task
                                      where p.TaskNo == WOTaskParams.TaskNo
                                      select p;
                        foreach (var task in taskRow)
                        {
                            task.TaskCode = WOTaskParams.TaskCode;
                            task.TaskName = WOTaskParams.TaskName;

                            if (WOTaskParams.TaskDate != "")
                            {
                                DateTime date = new DateTime();
                                try
                                {
                                    date = DateTime.ParseExact(WOTaskParams.TaskDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                                catch (Exception)
                                {
                                    date = Convert.ToDateTime(WOTaskParams.TaskDate);
                                }
                                task.TaskDate = FieldDateTime.DateToString(date);
                                task.TaskTime = FieldDateTime.TimeToString(date);
                            }
                            else
                            {
                                task.TaskDate = null;
                                task.TaskTime = null;
                            }

                            if (WOTaskParams.FinishDate != "")
                            {
                                DateTime date = new DateTime();
                                try
                                {
                                    date = DateTime.ParseExact(WOTaskParams.FinishDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                                catch (Exception)
                                {
                                    date = Convert.ToDateTime(WOTaskParams.TaskDate);
                                }
                                task.FinishDate = FieldDateTime.DateToString(date);
                                task.FinishTime = FieldDateTime.TimeToString(date);
                            }
                            else
                            {
                                task.FinishDate = null;
                                task.FinishTime = null;
                            }

                            task.TaskDuration = FieldDateTime.ConvertDurationToMinute(InputVal.ToDecimal(WOTaskParams.TaskDuration));
                            task.EQNo = WOTaskParams.EQNo;
                            task.PUNo = WOTaskParams.PUNo;
                            task.Component = WOTaskParams.Component;
                            task.ActionNo = WOTaskParams.ActionNo;
                            task.ActDuration = FieldDateTime.ConvertDurationToMinute(InputVal.ToDecimal(WOTaskParams.ActDuration));
                            task.Standard = WOTaskParams.Standard;
                            task.TaskProcedure = WOTaskParams.TaskProcedure;
                            task.AbnormalNote = WOTaskParams.AbnormalNote;
                            task.Done = InputVal.BoolToString(WOTaskParams.FlagDone);
                            task.Abnormal = InputVal.BoolToString(WOTaskParams.FlagAbNormal);
                            task.INSPNO = WOTaskParams.INSPNO;
                            task.Remark = WOTaskParams.Remark;

                        }
                       
                    }
                    else
                    {
                        WO_Task task = new WO_Task();
                        task.TaskCode = WOTaskParams.TaskCode;
                        task.TaskName = WOTaskParams.TaskName;

                        if (WOTaskParams.TaskDate != "")
                        {
                            DateTime date = new DateTime();
                            try
                            {
                                date = DateTime.ParseExact(WOTaskParams.TaskDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                            }
                            catch (Exception)
                            {
                                date = Convert.ToDateTime(WOTaskParams.TaskDate);
                            }
                            task.TaskDate = FieldDateTime.DateToString(date);
                            task.TaskTime = FieldDateTime.TimeToString(date);
                        }
                        else
                        {
                            task.TaskDate = null;
                            task.TaskTime = null;
                        }

                        task.TaskDuration = FieldDateTime.ConvertDurationToMinute(InputVal.ToDecimal(WOTaskParams.TaskDuration));
                        task.EQNo = WOTaskParams.EQNo;
                        task.PUNo = WOTaskParams.PUNo;
                        task.Component = WOTaskParams.Component;
                        task.ActionNo = WOTaskParams.ActionNo;
                        task.Standard = WOTaskParams.Standard;
                        task.TaskProcedure = WOTaskParams.TaskProcedure;
                        task.AbnormalNote = WOTaskParams.AbnormalNote;
                        task.Done = InputVal.BoolToString(WOTaskParams.FlagDone);
                        task.Abnormal = InputVal.BoolToString(WOTaskParams.FlagAbNormal);
                        task.INSPNO = WOTaskParams.INSPNO;
                        task.Remark = WOTaskParams.Remark;
                        task.RCM = "F";
                        task.SymptomNo = 0;
                        task.WOSubTypeNo = 0;
                        task.FLAGDEL = "F";
                        task.FailureModeNo = 0;
                        task.CauseNo = 0;
                        task.InstallRemove = "F";
                        task.NotComplete = "F";
                        task.WONo = WOTaskParams.WONo;


                        db.WO_Task.Add(task);
                    }
                    db.SaveChanges();
                }
                   
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }

        [Route("GetConvertTime/{WONo:float}/")]
        [HttpGet]
        public HttpResponseMessage ConvertTime(float WONo)
        {
            return Request.CreateResponse(HttpStatusCode.OK, FieldDateTime.HoursToFloat(WONo));
        }
        public string AppendString(string eqcode, string eqname)
        {
            if (InputVal.ToString(eqcode) != "")
            {
                return string.Format("/{0}:{1}", eqcode, eqname);
            }
            else
            {
                return "";
            }
        }   
    }
}


