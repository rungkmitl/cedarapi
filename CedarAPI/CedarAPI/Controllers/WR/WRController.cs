﻿using CedarAPI.Models;
using CommonTools;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CedarAPI.Controllers.WR
{
    [RoutePrefix("api/WR")]
    //[EnableCors(origins: "*", headers: "*", methods: "*")]

    public class WRController : ApiController
    {
        private CedarEntities db = new CedarEntities();

        IWRRepository _wrRepository;
        public WRController(IWRRepository wrRepository)
        {
            _wrRepository = wrRepository;
        }

        public class Params
        {
            public int DeptNo { get; set; }
            public string FlagSection { get; set; }
            public int PersonNo { get; set; }
            public string PersonCode { get; set; }
            public string Where { get; set; }
            public int PUNo { get; set; }
            public int EQNo { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetList([FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _wrRepository.Get(Params.DeptNo, Params.FlagSection, Params.PersonNo, Params.PersonCode, Params.Where, Params.PUNo, Params.EQNo));
        }

        [Route("{WRNo:int}")]
        [HttpGet]
        public HttpResponseMessage GetID(int WRNo, [FromUri] Params Params)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _wrRepository.GetByID(WRNo, Params.PersonNo));
        }

        [Route("saveWR")]
        [HttpPost]
        public HttpResponseMessage saveWR([FromUri] WRInfo WRInfo)
        {
            int id =_wrRepository.SaveWR(WRInfo.date, WRInfo.time, WRInfo.puno, WRInfo.eqno, WRInfo.costcenterno, WRInfo.problem, WRInfo.siteno, WRInfo.personno, WRInfo.wrno, WRInfo.attach);
            return Request.CreateResponse(HttpStatusCode.OK, id);

        }

        public string GetActionSendTo(int wrno, int actno, int personno)
        {
            SqlParameter WONO = new SqlParameter("@DOCNO", wrno);
            SqlParameter ACTIONNO = new SqlParameter("@ACTIONNO", actno);
            SqlParameter UPDATEUSER = new SqlParameter("@UPDATEUSER", personno);

            ActionObject actionObject = db.Database.SqlQuery<ActionObject>("sp_WFN_EXEC_ROUTE_GETSENDER_WR @DOCNO, @ACTIONNO, @UPDATEUSER"
                , WONO, ACTIONNO, UPDATEUSER).FirstOrDefault();

            if (actionObject.ACTIONNO != 0)
            {
                if (actionObject.SENDTO_GROUP == "")
                {
                    return actionObject.SENDTO_PERSON;
                }
                else
                {
                    return actionObject.SENDTO_GROUP;
                }
            }

            return "";
        }
    }








}
