﻿using CedarAPI.Models;
using CommonTools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace CedarAPI.Controllers.WR
{
   
    public class WorkLogController : ApiController
    {
        private CedarEntities db = new CedarEntities();


        public HttpResponseMessage Get(int id)
        {
            try
            {
                WorkLogInfo obj_worklog = new WorkLogInfo();

                SqlParameter WRNO = new SqlParameter("@WRNO", id);

                List<WRFollowObj> resp = db.Database.SqlQuery<WRFollowObj>("sp_WF_WRFollowUpWO_Retrive @WRNO", WRNO).ToList();
                for (int i = 0; i < resp.Count; i++)
                {
                    WRFollowObj obj = resp[i];
                    obj_worklog.WORKORDER.Add(new WorkLogInfo.Worklog_WorkOrder(
                          obj.WONo
                            , obj.WOCode
                            , obj.asWODate
                            , obj.WOSTATUSCODE + " - " + obj.WOSTATUSNAME
                            , obj.NODENAME
                        ));
                }

                List<WFTrackedsObj> WFTrackedsObjList = db.Database.SqlQuery<WFTrackedsObj>("sp_WF_SendToHist_Retrive @DOCNO, @WFTYPE"
                    , new SqlParameter("@DOCNO", id)
                    , new SqlParameter("@WFTYPE", "WR")).OrderByDescending(o => o.Event_Order).ToList();


                for (int i = 0; i < WFTrackedsObjList.Count; i++)
                {
                    WFTrackedsObj obj = WFTrackedsObjList[i];
                    obj_worklog.HISTORY.Add(new WorkLogInfo.Worklog_History(
                        obj.asEventDate,
                          obj.asEventTime,
                          obj.Subject,
                          obj.Event_Desc,
                          obj.FromPersonName,
                          obj.ReceivePersonName
                     ));
                }


                return Request.CreateResponse(HttpStatusCode.OK, obj_worklog);

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
                throw;
            }
        }
    }

    public class WRFollowObj
    {
        public int WONo { get; set; }
        public string WOCode { get; set; }
        public DateTime adWODate { get; set; }
        public string asWODate { get; set; }
        public string asWOTime { get; set; }
        public int PUNO { get; set; }
        public string PUCode { get; set; }
        public string PUName { get; set; }
        public int DeptNo { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public int WOTYPENO { get; set; }
        public string WoTypeCode { get; set; }
        public string WoTypeName { get; set; }
        public int WOSTATUSNO { get; set; }
        public string WOSTATUSCODE { get; set; }
        public string WOSTATUSNAME { get; set; }
        public string NODENAME { get; set; }
    }

    public class WFTrackedsObj
    {
        public string asEventDate { get; set; }
        public string asEventTime { get; set; }
        public int Event_Order { get; set; }
        public string Subject { get; set; }
        public string Event_Desc { get; set; }
        public string FromPersonName { get; set; }
        public string ReceivePersonName { get; set; }
    }







}
