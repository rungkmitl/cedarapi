﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class ActionObject
    {
        public int ACTIONNO { get; set; }
        public string ACTIONNAME { get; set; }
        public string PRE_ACTION { get; set; }
        public string RECEIVER { get; set; }
        public int? SENDTO_GROUPNO { get; set; }
        public string SENDTO_GROUP { get; set; }
        public int? SENDTO_PERSONNO { get; set; }
        public string SENDTO_PERSON { get; set; }

        public ActionObject()
        {
            ACTIONNO = 0;
            ACTIONNAME = "";
            PRE_ACTION = "";
            RECEIVER = "";
        }
        public ActionObject(int ActtionNo, string ActionName, string PreAction, string Receiver)
        {
            ACTIONNO = ActtionNo;
            ACTIONNAME = ActionName;
            PRE_ACTION = PreAction;
            RECEIVER = Receiver;
        }
    }
}