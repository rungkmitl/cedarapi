﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class ConditionInfo
    {
        public int cboSite { get; set; }
        public int cboDept { get; set; }
        public string cboMaintainCost { get; set; }
        public string cboDowntime { get; set; }
        public string cboWorkCompliancy { get; set; }
        public string cboNumberOfWorkOverDue { get; set; }
        public string cboNumberOfWorkBacklogOver { get; set; }
        public string cboNumberOfRepetitiveFailedEQ { get; set; }

      

    }
}