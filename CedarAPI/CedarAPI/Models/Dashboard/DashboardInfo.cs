﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class DashboardInfo
    {
        public string MaintenanceCost { get; set; }
        public string TotalDowntTime { get; set; }
        public string WorkCompliancy { get; set; }
        public string NumberOfWorkOverDue { get; set; }
        public string NumberOfWorkBacklog { get; set; }
        public string NumberOfWorkBacklogOver { get; set; }
        public string NumberOfRepetitiveFailedEQ { get; set; }

    }
}