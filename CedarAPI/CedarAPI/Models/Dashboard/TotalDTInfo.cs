﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Dashboard
{
    public class TotalDTInfo
    {
        public int? PUNO { get; set; }
        public string PUCODE { get; set; }
        public string PUNAME { get; set; }
        public int NoOfShutDown { get; set; }
        public decimal DT_Duration { get; set; }
        public int? WONO { get; set; }
        public string WOCODE { get; set; }
        public DateTime WODATE { get; set; }
        public DateTime WOTIME { get; set; }
       
        public decimal MH { get; set; }
        public decimal MHCost { get; set; }
        public double MaterialStock { get; set; }
        public double MaterialDirectPurchase { get; set; }
        public double OutsourceServices { get; set; }
        public double TotalCost { get; set; }
       
        public string EQCode { get; set; }
        public string EQName { get; set; }
        public string WOSTATUSCODE { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public int? DEPTNO { get; set; }
        public string WOTYPECODE { get; set; }
        public int SiteNo { get; set; }
    }
}