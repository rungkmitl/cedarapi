﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Dashboard
{
    public class WODetailInfo
    {

        public int RowNo { get; set; }
        public int WONO { get; set; }
        public string WoCode { get; set; }
        public DateTime WRDATE { get; set; }
        public int DFR { get; set; }
        public string WOSTATUSCODE { get; set; }
        public string WOSTATUSNAME { get; set; }
        public DateTime WODATE { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public string WOTYPECODE { get; set; }
        public string WOTYPENAME { get; set; }
        public string PRIORITYCODE { get; set; }
        public string PRIORITYNAME { get; set; }
        public string REFWRCode { get; set; }
        public string PUCode { get; set; }
        public string PUName { get; set; }
        public string EQCode { get; set; }
        public string EQName { get; set; }
        public string Symptom { get; set; }
        public decimal SCH_DURATION { get; set; }
        public int WaitForShutDown { get; set; }
        public int WaitForMaterial { get; set; }
        public int WaitForOther { get; set; }
        public string PersonCode { get; set; }
        public string PlanFirstName { get; set; }
    }
}