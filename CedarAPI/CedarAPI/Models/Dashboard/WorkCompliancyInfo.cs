﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Dashboard
{
    public class WorkCompliancyInfo
    {
        public int? DEPTNO { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public int? WOTYPENO { get; set; }
        public string WOTYPECODE { get; set; }
        public int Total { get; set; }
        public int Finish { get; set; }
        public int NonFinish { get; set; }
        public int? PUNO { get; set; }
        public string PUCODE { get; set; }
        public string PUNAME { get; set; }

    }
}