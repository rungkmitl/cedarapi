﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Dashboard
{
    public class WorkOverDueInfo
    {
        public int DEPTNO { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public int WOTYPENO { get; set; }
        public string WOTYPECODE { get; set; }
        public int OverDue { get; set; }
        public int NonPlan { get; set; }
        public int PUNO { get; set; }
        public string PUCODE { get; set; }
        public string PUNAME { get; set; }


    }
}