﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Master
{
    public class UserInfo
    {
        public int PersonNo { get; set; }
     
        public string UserID { get; set; }
        public string PERSONCODE { get; set; }
        public string PERSON_NAME { get; set; }
        public string TITLE { get; set; }
        public int DEPTNO { get; set; }
        public string DEPTCODE { get; set; }
        public string DEPTNAME { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public int SiteNo { get; set; }
        public string UserGCode { get; set; }
        public string FlagSection { get; set; }
        public string Password { get; set; }
    }
}