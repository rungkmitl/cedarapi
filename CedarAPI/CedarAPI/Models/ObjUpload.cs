﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class ObjUpload
    {
        public string status { get; set; }
        public string filename { get; set; }
        public string errMsg { get; set; }

        public ObjUpload()
        {
            status = "error";
        }
    }
}