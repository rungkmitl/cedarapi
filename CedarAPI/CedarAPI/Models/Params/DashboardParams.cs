﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Params
{
    public class DashboardParams
    {
        public int SiteNo { get; set; }
        public string WherMaintenanceCost { get; set; }
        public string WherTotalDowntTime { get; set; }
        public string WherWorkCompliancy { get; set; }
        public string WherNumberOfWorkOverDue { get; set; }
        public string WherNumberOfWorkBacklogOver { get; set; }
        public string WherNumberOfRepetitiveFailedEQ { get; set; }
        public decimal ValMaintenanceCost { get; set; }
        public decimal ValTotalDowntTime { get; set; }
        public decimal ValWorkCompliancy { get; set; }
        public decimal ValNumberOfWorkOverDue { get; set; }
        public decimal ValNumberOfWorkBacklog { get; set; }
        public decimal ValNumberOfWorkBacklogOver { get; set; }
        public decimal ValNumberOfRepetitiveFailedEQ { get; set; }
        public int DeptNo { get; set; }
        public int PersonNo { get; set; }
        public int PUNo { get; set; }
        public int WOTypeNo { get; set; }
        public int WOStatusNo { get; set; }
        public string GroupBy { get; set; }
        public string Where { get; set; }
        public string Type { get; set; }
        public string TotalCost { get; set; }
        public string OutService { get; set; }
        public string MDirect { get; set; }
        public string MStock { get; set; }
        public string MHCost { get; set; }
        public string MH { get; set; }
        public string Total { get; set; }
        public string NoOfShutDown { get; set; }
        public string DT_Duration { get; set; }

        public string Finish { get; set; }
        public string NonFinish { get; set; }
        public string OverDue { get; set; }
        public string NonPlan { get; set; }
        public string Field { get; set; }
        public string WODate { get; set; }
        public string WOTYPECODE { get; set; }
        public string DEPTCODE { get; set; }
        public string PUCODE { get; set; }
        public string EQCode { get; set; }








    }
}