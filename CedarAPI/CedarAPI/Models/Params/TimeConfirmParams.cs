﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Params
{
    public class TimeConfirmParams
    {
        public string Start { get; set; }
        public string End { get; set; }
        public int WONo { get; set; }
        public int PersonNo { get; set; }
        public float Duration { get; set; }
    }
}