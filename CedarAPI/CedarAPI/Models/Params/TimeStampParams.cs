﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models.Params
{
    public class TimeStampParams
    {
        public string PUCode { get; set; }
        public string UpdatedBy { get; set; }
        public string WOCode { get; set; }
        public int WONo { get; set; }
        public bool FlagCheckJob { get; set; }
        public string Remark { get; set; }
        public string PinCode { get; set; }
    }
}