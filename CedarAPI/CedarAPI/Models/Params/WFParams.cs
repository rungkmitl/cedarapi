﻿namespace CedarAPI.Models.Params
{
    public class WFParams
    {
        public int WRNO { get; set; }
        public int WOTypeNo { get; set; }
        public int DeptNo { get; set; }
        public int ASSIGN { get; set; }
        public int USERGROUPNO { get; set; }
        public int UPDATEUSER { get; set; }
        public int VendorNo { get; set; }
        public int DOCNO{ get; set; }
        public int ACTIONNO { get; set; }
        public string DESC { get; set; }
        

    }

}