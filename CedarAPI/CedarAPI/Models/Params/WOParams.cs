﻿namespace CedarAPI.Models.Params
{
    public class WOParams
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string WOCode { get; set; }
        public int SiteNo { get; set; }
        public double Duration { get; set; }
        public string Meter { get; set; }
        
        public bool FlagSafety { get; set; }
        public bool FlagEnviroment { get; set; }
        public bool FlagEQ { get; set; }
        public bool FlagPU { get; set; }
        public int WO_RESCNO { get; set; }
        public int QTY_USE { get; set; }
        public int qtyTravel { get; set; }
        public double amtTravel { get; set; }
        public int qtyAccomodation { get; set; }
        public double amtAccomodation { get; set; }
        public string nameOther { get; set; }
        public int qtyOther { get; set; }
        public double amtOther { get; set; }
        public int problemNo { get; set; }
        public string problemDesc { get; set; }
        public int actionNo { get; set; }
        public string actionDesc { get; set; }
        public int causeNo { get; set; }
        public string causeDesc { get; set; }
        public int qtyAllowance { get; set; }
        public double amtAllowance { get; set; }
        public int qtyTravelOT { get; set; }
        public double amtTravelOT { get; set; }
        public string filename { get; set; }
        public int WONo { get; set; }
        public string actStartDate { get; set; }
        public string actEndDate { get; set; }
        public int assignNo { get; set; }
        public string dtStartDate { get; set; }
        public string dtEndDate { get; set; }

        public int DeptNo { get; set; }
        public string FlagSection { get; set; }
        public int PersonNo { get; set; }
        public string PersonCode { get; set; }
        public string Where { get; set; }
        public int PUNo { get; set; }
        public int EQNo { get; set; }
        public string TaskProcedure { get; set; }



    }

}