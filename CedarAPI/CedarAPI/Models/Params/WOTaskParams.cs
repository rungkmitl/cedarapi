﻿namespace CedarAPI.Models.Params
{
    public class WOTaskParams
    {
        public int WONo { get; set; }
        public string WOCode { get; set; }
        public int SiteNo { get; set; }
        public int TaskNo { get; set; }
        public int TaskCode { get; set; }
        public string TaskName { get; set; }
        public string TaskDate { get; set; }

        public string TaskTime { get; set; }
        public string FinishDate { get; set; }
        public string FinishTime { get; set; }
        public int EQNo { get; set; }
        public int PUNo { get; set; }
        public string Component { get; set; }
        public int ActionNo { get; set; }
        public float ActDuration { get; set; }
        public string Standard { get; set; }
        public float TaskDuration { get; set; }
        public string TaskProcedure { get; set; }
        public string AbnormalNote { get; set; }
        public bool FlagDone { get; set; }
        public bool FlagAbNormal { get; set; }
        public int INSPNO { get; set; }
        public string Remark { get; set; }


    }
}