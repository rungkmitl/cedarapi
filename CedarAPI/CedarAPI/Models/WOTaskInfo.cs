﻿using System;

namespace CedarAPI.Models
{
    public class WOTaskInfo
    {
        public int TaskNo { get; set; }
        public Nullable<int> TaskCode { get; set; }
        public string TaskName { get; set; }
        public Nullable<int> SymptomNo { get; set; }
        public Nullable<int> PUNo { get; set; }
        public Nullable<int> EQNo { get; set; }
        public string Component { get; set; }
        public Nullable<int> ActionNo { get; set; }
        public Nullable<int> WOSubTypeNo { get; set; }
        public string Standard { get; set; }
        public string TaskProcedure { get; set; }
        public string RCM { get; set; }
        public string TaskDate { get; set; }
        public Nullable<double> TaskDuration { get; set; }
        public string Done { get; set; }
        public string Abnormal { get; set; }
        public string Remark { get; set; }
        public string FLAGDEL { get; set; }
        public Nullable<int> CREATEUSER { get; set; }
        public string CREATEDATE { get; set; }
        public Nullable<int> UpdateUser { get; set; }
        public string UpdateDate { get; set; }
        public Nullable<int> WONo { get; set; }
        public Nullable<int> FailureModeNo { get; set; }
        public Nullable<int> CauseNo { get; set; }
        public string FinishDate { get; set; }
        public string FinishTime { get; set; }
        public Nullable<double> ActDuration { get; set; }
        public string InstallRemove { get; set; }
        public string NotComplete { get; set; }
        public Nullable<int> PartNo { get; set; }
        public string SerialNo { get; set; }
        public string TaskTime { get; set; }
        public Nullable<int> INSPNO { get; set; }
        public string AbnormalNote { get; set; }
        public string ActionDesc { get; set; }
        public string PUDesc { get; set; }
        public string EQDesc { get; set; }
        public string InspDesc { get; set; }
        public bool FlagDone { get; set; }
        public bool FlagAbNormal { get; set; }


    }

}