﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class WRInfo
    {
        public int wrno { get; set; }
        public string wrcode { get; set; }
        public string wrdesc { get; set; }
        public string wrdate { get; set; }
        public string wrstatus { get; set; }
        public string nodename { get; set; }
        public string pucode { get; set; }
        public string eqcode { get; set; }
        public string costcentercode { get; set; }
        public string sitename { get; set; }
        public int siteno { get; set; }
        public string deptcode { get; set; }
        public string description { get; set; }
        public int? puno { get; set; }
        public string puname { get; set; }
        public int? eqno { get; set; }
        public string eqname { get; set; }
        public int? costcenterno { get; set; }
        public string costcentername { get; set; }
        public int? dept_rec_no { get; set; }
      
        public List<ActionObject> ACTION;
        public List<string> ATTACHMENT;

        public string date { get; set; }
        public string time { get; set; }
        public string problem { get; set; }
        public string attach { get; set; }
        public int personno { get; set; }





    }
}