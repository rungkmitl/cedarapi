﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CedarAPI.Models
{
    public class WorkLogInfo
    {

        public List<Worklog_WorkOrder> WORKORDER;
        public List<Worklog_History> HISTORY;
        public string SESSIONSTATE { set; get; }
        public WorkLogInfo()
        {
            WORKORDER = new List<Worklog_WorkOrder>();
            HISTORY = new List<Worklog_History>();
        }

        public class Worklog_WorkOrder
        {
            public int WONO;
            public string WOCODE;
            public string WODATE;
            public string WOSTATUS;
            public string NODENAME;
            public Worklog_WorkOrder(int wono, string wocode, string wodate, string wostatus, string nodename)
            {
                WONO = wono;
                WOCODE = wocode;
                WODATE = wodate;
                WOSTATUS = wostatus;
                NODENAME = nodename;
            }
        }

        public class Worklog_History
        {
            public string DATE;
            public string TIME;
            public string SUBJECT;
            public string MESSAGE;
            public string SENDERNAME;
            public string RECEIVERNAME;

            public Worklog_History(string date, string time, string subject, string desc, string sendername, string receivername)
            {
                DATE = date;
                TIME = time;
                SUBJECT = subject;
                MESSAGE = desc;
                SENDERNAME = sendername;
                RECEIVERNAME = receivername;
            }
        }

    }

}