﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonTools
{
    public class DateData
    {
        public string StartD { get; set; }
        public string StartT { get; set; }
        public DateTime StartDate { get; set; }
        public string FinishD { get; set; }
        public string FinishT { get; set; }
        public DateTime FinishDate { get; set; }
    }
}