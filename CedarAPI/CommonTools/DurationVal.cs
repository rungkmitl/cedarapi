﻿using System;

namespace CommonTools
{
    public class DurationVal
    {

        public static float ToFloat(object DurationMinute)
        {
            int p_intDura = 0;
            if (DurationMinute == null)
            {
                return 0;
            }

            if (Convert.IsDBNull(DurationMinute) == true)
            {
                return 0;
            }

            p_intDura = Convert.ToInt32(DurationMinute);
            return Convert.ToSingle((p_intDura / 60) + (p_intDura % 60.00 / 100));

        }


    }
}
