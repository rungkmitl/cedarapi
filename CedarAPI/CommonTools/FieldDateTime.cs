﻿using System;

namespace CommonTools
{
    public class FieldDateTime
    {

        public static object GetInputTimeSpan(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }

            if (!(Val is DateTime))
            {
                throw new Exception("Input must be datetime.");
            }

            if (Convert.IsDBNull(Val) == true)
            {
                return Convert.DBNull;
            }
            else
            {
                DateTime p_date = Convert.ToDateTime(Val);
                DateTime p_NewDate = new DateTime(1, 1, 1, p_date.Hour, p_date.Minute, p_date.Second);
                return TimeSpan.FromTicks(p_NewDate.Ticks);
            }
        }

        public static object GetInputDatetime(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }

            if (!(Val is DateTime))
            {
                throw new Exception("Input must be datetime.");
            }

            if (Convert.IsDBNull(Val) == true)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }

        public static string DateToString(DateTime Val)
        {

            if (Val.Year < 1753)
            {
                throw new Exception("Number of Year must more than 1,752.");
            }

            return String.Format("{0:0000}{1:00}{2:00}", Val.Year, Val.Month, Val.Day);
        }

        public static string DateToString(Object Val)
        {
            DateTime p_dateDate;

            if (Convert.IsDBNull(Val) == true || Val == null)
            {
                return "";
            }
            else
            {
                p_dateDate = Convert.ToDateTime(Val);
                if (p_dateDate.Year < 1753)
                {
                    throw new Exception("Number of Year must more than 1,752.");
                }
                return String.Format("{0:0000}{1:00}{2:00}", p_dateDate.Year, p_dateDate.Month, p_dateDate.Day);
            }

        }

        public static object DateToStringNull(Object Val)
        {
            DateTime p_dateDate;

            if (Convert.IsDBNull(Val) == true || Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                p_dateDate = Convert.ToDateTime(Val);
                if (p_dateDate.Year < 1753)
                {
                    throw new Exception("Number of Year must more than 1,752.");
                }
                return String.Format("{0:0000}{1:00}{2:00}", p_dateDate.Year, p_dateDate.Month, p_dateDate.Day);
            }

        }


        public static string TimeToString(DateTime Val)
        {
            return String.Format("{0:00}{1:00}", Val.Hour, Val.Minute);
        }

        public static string TimeToString(Object Val)
        {
            DateTime p_dateDate;
            if (Convert.IsDBNull(Val) == true || Val == null)
            {
                return "";
            }
            else
            {
                p_dateDate = Convert.ToDateTime(Val);
                return String.Format("{0:00}{1:00}", p_dateDate.Hour, p_dateDate.Minute);
            }

        }

        public static object TimeToStringNull(Object Val)
        {
            DateTime p_dateDate;
            if (Convert.IsDBNull(Val) == true || Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                p_dateDate = Convert.ToDateTime(Val);
                return String.Format("{0:00}{1:00}", p_dateDate.Hour, p_dateDate.Minute);
            }
        }

        public static DateTime StringToDate(string Val)
        {
            return new DateTime(Convert.ToInt32(Val.Substring(0, 4)), Convert.ToInt32(Val.Substring(4, 2)), Convert.ToInt32(Val.Substring(6, 2)));
        }

        public static DateTime? StringToDate(object Val)
        {
            if (Val != null && Convert.IsDBNull(Val) == false && Val.ToString().Trim().Length > 0)
            {
                string p_Val = Convert.ToString(Val);
                return new DateTime(Convert.ToInt32(p_Val.Substring(0, 4)), Convert.ToInt32(p_Val.Substring(4, 2)), Convert.ToInt32(p_Val.Substring(6, 2)));
            }
            else
            {
                return null;
            }

        }


        public static DateTime StringToTime(string Val)
        {
            Val = Val.Trim().Replace(":", "");
            if (Val.Trim().Length == 6)
            {
                return new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), Convert.ToInt32(Val.Substring(4, 2)));
            }
            else if (Val.Length == 4)
            {
                return new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), 0);
            }
            else
            {
                return new DateTime(1, 1, 1, 0, 0, 0);
            }

        }

        public static DateTime? StringToTime(object Val)
        {
            if (Val != null)
            {
                string p_Val = Convert.ToString(Val);

                if (p_Val.Length == 0)
                {
                    return null;
                }

                p_Val = p_Val.Trim().Replace(":", "");
                if (p_Val.Length == 6)
                {
                    return new DateTime(1, 1, 1, Convert.ToInt32(p_Val.Substring(0, 2)), Convert.ToInt32(p_Val.Substring(2, 2)), Convert.ToInt32(p_Val.Substring(4, 2)));
                }
                else if (p_Val.Length == 4)
                {
                    return new DateTime(1, 1, 1, Convert.ToInt32(p_Val.Substring(0, 2)), Convert.ToInt32(p_Val.Substring(2, 2)), 0);
                }
                else
                {
                    return new DateTime(1, 1, 1, 0, 0, 0);
                }

            }
            else
            {
                return null;
            }

        }

        public static DateTime? GetDatetimeNull(object Date, object Time)
        {
            DateTime p_Date;
            DateTime p_Time;

            if (Convert.IsDBNull(Date) == true)
            {
                return null;
            }
            else
            {
                p_Date = Convert.ToDateTime(Date);

                if (Time is string && Time != null)
                {
                    if (Time.ToString().Length > 0)
                    {
                        p_Time = Convert.ToDateTime(StringToTime(Time));
                        return new DateTime(p_Date.Year, p_Date.Month, p_Date.Day, p_Time.Hour, p_Time.Minute, p_Time.Second);
                    }
                    else
                    {
                        return new DateTime(p_Date.Year, p_Date.Month, p_Date.Day);
                    }
                }
                else
                {
                    if (Time == null || Convert.IsDBNull(Time) == true)
                    {
                        return new DateTime(p_Date.Year, p_Date.Month, p_Date.Day);
                    }
                    else
                    {
                        p_Time = Convert.ToDateTime(Time);
                        return new DateTime(p_Date.Year, p_Date.Month, p_Date.Day, p_Time.Hour, p_Time.Minute, p_Time.Second);
                    }
                }

            }
        }

        #region "แปลงค่า จำนวนเวลา"

        /// <summary>
        /// แปลงจำนวนชั่วโมงเป็นเลขฐาน 10
        /// </summary>
        /// <param name="TimeHour">จำนวนชั่วโมง เช่น 100.30 หนึ่งร้อยชั่วโมงสามสิบนาที</param>
        /// <returns>แปลงเป็นเลขฐาน10</returns>
        public static float HoursToFloat(float TimeHour)
        {
            int p_intHour = (int)TimeHour;
            float p_fMinute = TimeHour - p_intHour;


            return Convert.ToSingle(p_intHour) + (((p_fMinute * 100) * (100f / 60f)) / 100);
        }

        /// <summary>
        /// แปลงฐานสิบเป็นจำนวนชั่วโมง
        /// </summary>
        /// <param name="Hours">จำนวนชั่วโมงฐานสิบ</param>
        /// <returns>จำนวนชั่วโมงฐาน 60</returns>
        public static float FloatToHours(float Hours)
        {
            int p_intHour = (int)Hours;
            float p_fMinute = Hours - p_intHour;


            return Convert.ToSingle(p_intHour) + (((p_fMinute * 100) / (100f / 60f)) / 100);
        }

        #endregion

        public static int ConvertDurationToMinute (decimal Duration)
        {
            int DurationHours = (int)Duration;
            return (int)((DurationHours * 60) + ((Duration - DurationHours) * 100));
        }

        public static int ConvertDurationToMinute(float Duration)
        {
            int DurationHours = (int)Duration;
            return (int)((DurationHours * 60) + ((Duration - DurationHours) * 100));
        }


    }//End Class
}//End Namespace
