﻿using System;
using System.Globalization;

namespace CommonTools
{
    public class InputVal
    {

        public static int ToInt(object Val)
        {
            if (Val == null)
            {
                return 0;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return 0;
            }
            else
            {
                try
                {
                    return Convert.ToInt32(Val);
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static int? ToIntNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                try
                {
                    return Convert.ToInt32(Val);
                }
                catch
                {
                    return null;
                }
            }
        }

        public static float ToFloat(object Val)
        {
            if (Val == null)
            {
                return 0;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return 0;
            }
            else
            {
                return Convert.ToSingle(Val);
            }
        }

        public static float? ToFloatNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                return Convert.ToSingle(Val);
            }
        }

        public static decimal ToDecimal(object Val)
        {
            if (Val == null)
            {
                return 0;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return 0;
            }
            else
            {
                return Convert.ToDecimal(Val);
            }
        }

        public static decimal? ToDecimalNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }

            if (Val is string)
            {
                if (Val.ToString() == string.Empty)
                {
                    return null;
                }
                else
                {
                    if (Val.ToString().Trim().Length > 0)
                    {
                        return Convert.ToDecimal(Val);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                return Convert.ToDecimal(Val);
            }
        }

        public static string ToString(object Val)
        {
            if (Val == null)
            {
                return "";
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return "";
            }
            else
            {
                return Convert.ToString(Val);
            }
        }

        public static string ToStringNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                return Convert.ToString(Val);
            }
        }


        public static bool ToBool(object Val)
        {
            if (Val == null)
            {
                return false;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return false;
            }
            else
            {
                return Convert.ToBoolean(Val);
            }
        }

        public static bool? ToBoolNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                return Convert.ToBoolean(Val);
            }
        }

        public static string ToShortDateText(object Val)
        {
            if (Val == null)
            {
                return "";
            }


            if (Convert.IsDBNull(Val) == true)
            {
                return "";
            }
            else
            {
                if (Val is DateTime)
                {
                    return Convert.ToDateTime(Val).ToShortDateString();
                }
                else if (Val is string)
                {
                    return ToShortDateText(Val.ToString());
                }
                else
                {
                    return "";
                }

            }
        }

        public static string ToShortDateText(string Val)
        {
            if (Val == null)
            {
                return "";
            }

            if (Val.Length == 0)
            {
                return "";
            }

            DateTime p_Date;

            p_Date = new DateTime(Convert.ToInt32(Val.Substring(0, 4)), Convert.ToInt32(Val.Substring(4, 2)), Convert.ToInt32(Val.Substring(6, 2)));

            return p_Date.ToShortDateString();
        }

        /// <summary>
        /// เมื่อข้อมูลเป็น Null หรือ DBNull ให้ Return null
        /// </summary>
        /// <param name="Val"></param>
        /// <returns></returns>
        public static string ToShortDateTextNull(object Val)
        {
            if (Val == null)
            {
                return null;
            }

            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }
            else
            {
                if (Val is DateTime)
                {
                    return Convert.ToDateTime(Val).ToShortDateString();
                }
                else if (Val is string)
                {
                    return ToShortDateTextNull(Val.ToString());
                }
                else
                {
                    return null;
                }

            }
        }


        /// <summary>
        /// เมื่อข้อมูลเป็น Null หรือ DBNull ให้ Return null
        /// </summary>
        /// <param name="Val"></param>
        /// <returns></returns>
        public static string ToShortDateTextNull(string Val)
        {
            if (Val == null)
            {
                return null;
            }

            if (Val.Length == 0)
            {
                return null;
            }

            DateTime p_Date;

            p_Date = new DateTime(Convert.ToInt32(Val.Substring(0, 4)), Convert.ToInt32(Val.Substring(4, 2)), Convert.ToInt32(Val.Substring(6, 2)));

            return p_Date.ToShortDateString();
        }


        public static string ToTimeText(Object Val)
        {
            if (Val == null)
            {
                return "";
            }

            if (Convert.IsDBNull(Val) == true)
            {
                return "";
            }

            if (Val is DateTime)
            {
                return string.Format("{0:HH:mm}", Convert.ToDateTime(Val));
            }
            else if (Val is String)
            {
                return ToTimeText(Val.ToString());
            }
            else
            {
                return "";
            }


        }

        public static string ToTimeText(string Val)
        {
            DateTime p_Date;
            if (Val.Trim().Length == 6)
            {
                p_Date = new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), Convert.ToInt32(Val.Substring(4, 2)));
            }
            else if (Val.Length == 4)
            {
                p_Date = new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), 0);
            }
            else
            {
                p_Date = new DateTime(1, 1, 1, 0, 0, 0);
            }

            return string.Format("{0:HH:mm}", p_Date);

        }

        /// <summary>
        /// เมื่อข้อมูลเป็น Null หรือ DBNull ให้ Return null
        /// </summary>
        /// <param name="Val"></param>
        /// <returns></returns>
        public static string ToTimeTextNull(Object Val)
        {
            if (Convert.IsDBNull(Val) == true)
            {
                return null;
            }

            if (Val is DateTime)
            {
                return string.Format("{0:HH:mm}", Convert.ToDateTime(Val));
            }
            else if (Val is String)
            {
                return ToTimeTextNull(Val.ToString());
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// เมื่อข้อมูลเป็น Null หรือ DBNull ให้ Return null
        /// </summary>
        /// <param name="Val"></param>
        /// <returns></returns>
        public static string ToTimeTextNull(string Val)
        {
            DateTime p_Date;
            if (Val.Trim().Length == 6)
            {
                p_Date = new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), Convert.ToInt32(Val.Substring(4, 2)));
            }
            else if (Val.Length == 4)
            {
                p_Date = new DateTime(1, 1, 1, Convert.ToInt32(Val.Substring(0, 2)), Convert.ToInt32(Val.Substring(2, 2)), 0);
            }
            else
            {
                return null;
            }

            return string.Format("{0:HH:mm}", p_Date);
        }

        public static string BoolToString(bool val)
        {
            if (val) return "T";
            return "F";
        }
        public static bool StringToBool(string val)
        {
            if (val == "T") return true;
            return false;
        }

        public static double CalculateDuration(DateTime startDate, DateTime finishDate)
        {
            if (finishDate != DateTime.MinValue)
            {
                TimeSpan t = finishDate.Subtract(startDate);
                int hour = (int)t.Hours;
                hour = hour + (int)t.Days * 24;
                double min = t.Minutes * 0.01;

                return (hour * 60) + (((hour + min) - hour) * 100);
            }
            return 0;
        }
        public static double CalculateDurationToMin(DateTime startDate, DateTime finishDate)
        {
            if (finishDate != DateTime.MinValue)
            {
                TimeSpan t = finishDate.Subtract(startDate);
                return t.TotalMinutes;
            }
            return 0;
        }
        public static void ConvertStartDateToString(string startDate, ref DateData DateData)
        {
            try
            {
                DateTime start = DateTime.ParseExact(startDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                string StartD = CommonTools.FieldDateTime.DateToString(start);
                string StartT = CommonTools.FieldDateTime.TimeToString(start);
                DateData.StartD = StartD;
                DateData.StartT = StartT;
                DateData.StartDate = start;
            }
            catch (Exception)
            {
                DateTime start = Convert.ToDateTime(startDate);
                string StartD = CommonTools.FieldDateTime.DateToString(start);
                string StartT = CommonTools.FieldDateTime.TimeToString(start);
                DateData.StartD = StartD;
                DateData.StartT = StartT;
                DateData.StartDate = start;
            }

        }
        public static void ConvertFinshDateToString(string endDate, ref DateData DateData)
        {
            try
            {
                DateTime end = DateTime.ParseExact(endDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                string FinishD = CommonTools.FieldDateTime.DateToString(end);
                string FinishT = CommonTools.FieldDateTime.TimeToString(end);
                DateData.FinishD = FinishD;
                DateData.FinishT = FinishT;
                DateData.FinishDate = end;
            }
            catch (Exception)
            {
                DateTime end = Convert.ToDateTime(endDate);
                string FinishD = CommonTools.FieldDateTime.DateToString(end);
                string FinishT = CommonTools.FieldDateTime.TimeToString(end);
                DateData.FinishD = FinishD;
                DateData.FinishT = FinishT;
                DateData.FinishDate = end;
            }
        }
    }//End Class
}//End Namespace
