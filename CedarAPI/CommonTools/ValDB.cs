﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonTools
{
    public class ValDB
    {

        public static object GetString(string Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetStringNull(string Val)
        {
            if (Val == null)
            {
                return null;
            }
            else
            {
                return Val;
            }
        }
        public static object GetFloat(float Val)
        {
            if (Val == 0)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetFloat(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }

        public static object GetDouble(double Val)
        {
            if (Val == 0)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetDouble(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }

        public static object GetInt(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetInt(int Val)
        {
            if (Val == 0)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }

        public static object GetDecimal(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }

        public static object GetBool(bool? Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetDate(DateTime? Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetDateTime(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                return Val;
            }
        }
        public static object GetTime(object Val)
        {
            if (Val == null)
            {
                return Convert.DBNull;
            }
            else
            {
                DateTime d = Convert.ToDateTime(Val);
                return new TimeSpan(d.Hour, d.Minute, 0);
            }
        }
    }//End Class
}//End Namespace
