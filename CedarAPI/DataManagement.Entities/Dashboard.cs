﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Dashboard
    {
        public float ValMaintenanceCost { get; set; }
        public float ValTotalDowntTime { get; set; }
        public float ValWorkCompliancy { get; set; }
        public float ValNumberOfWorkOverDue { get; set; }
        public float ValNumberOfWorkBacklog { get; set; }
        public float ValNumberOfWorkBacklogOver { get; set; }
        public float ValNumberOfRepetitiveFailedEQ { get; set; }
    }
}
