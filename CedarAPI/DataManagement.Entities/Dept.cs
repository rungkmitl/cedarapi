﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Dept
    {
        public int No { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int DEPTPARENT { get; set; }
        public string DEPTPARENTCODE { get; set; }
        public string DEPTPARENTNAME { get; set; }
        public int COSTCENTERNO { get; set; }
        public string COSTCENTERCODE { get; set; }
        public string COSTCENTERNAME { get; set; }
        public string FLAGSECTION { get; set; }
        public string FLAGDEL { get; set; }
        public int DeptNo { get; set; }
        public string DeptDesc { get; set; }

    }
}
