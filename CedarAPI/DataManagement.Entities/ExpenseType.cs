﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class ExpenseType
    {
        public int EXPNO { get; set; }
        public string EXPCODE { get; set; }
        public string EXPNAME { get; set; }
        public int EXPPARENT { get; set; }
        public string EXPPARENTCODE { get; set; }
        public string EXPPARENTNAME { get; set; }
    }
}
