﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataManagement.Entities
{
    public class Failure
    {
        public int FMItem { get; set; }
        public int FailureModeNo { get; set; }
        public string FailureModeDes { get; set; }
        public int CauseNo { get; set; }
        public string CauseDesc { get; set; }
        public string WOActionDesc { get; set; }
        public int WOActionNo { get; set; }
        public int ProblemNo { get; set; }
        public int WONo { get; set; }
        public string WOACTIONCODE { get; set; }
        public string FailureModeCode { get; set; }
        public string FailureModeName { get; set; }
        public string WOACTIONNAME { get; set; }
        public string WOPROBLEMNAME { get; set; }
        public string WOPROBLEMCODE { get; set; }
        public Failure()
        {
            FMItem = 0;
        }
    }
}