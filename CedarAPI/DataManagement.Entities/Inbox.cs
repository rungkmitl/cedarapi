﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Inbox
    {

        public int UnReadTotal { get; set; }
        public IEnumerable<Detail> InboxList{ get; set; }
        public class Detail
        {
            public string WFTypeName { get; set; }
            public string DocCode { get; set; }
            public string Subject { get; set; }
            public string StatusCode { get; set; }
            public string PersonName { get; set; }
            public DateTime FromDate { get; set; }
            public string asFromDate { get; set; }
            public DateTime FromTime { get; set; }
            public string asFromTime { get; set; }
            public string Readed_Flag { get; set; }
            public string Approved_Flag { get; set; }
            public int DocNo { get; set; }
            public int DocStatusNo { get; set; }
            public string WFDocFlowCode { get; set; }
            public int SiteNo { get; set; }
            public string SiteCode { get; set; }
            public string PUCODE { get; set; }
            public string PUNAME { get; set; }
            public string EQCODE { get; set; }
            public string EQNAME { get; set; }
            public string WO_PROBLEM { get; set; }
            public string WOTypeName { get; set; }
            public string WRDESC { get; set; }
            public int Event_Order { get; set; }
            public bool HasAttachFile { get; set; }
        }
    }
}
