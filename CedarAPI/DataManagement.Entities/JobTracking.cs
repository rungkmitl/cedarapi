﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataManagement.Entities
{
    public class JobTracking
    {
        public string Doccode { get; set; }
        public int Event_Order { get; set; }
        public string asEventDate { get; set; }
        public string asEventTime { get; set; }
        public string Subject { get; set; }
        public string Event_Desc { get; set; }
        public string FromPersonName { get; set; }
        public string ReceivePersonName { get; set; }
    }
}