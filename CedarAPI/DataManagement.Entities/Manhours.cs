﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataManagement.Entities
{
    public class Manhours
    {
        public string TRDate { get; set; }
        public string TRTime { get; set; }
        public int WO_RescNo { get; set; }
        public int WONo { get; set; }
        public float Hours { get; set; }
        public float QtyHours { get; set; }
        public int PERSONNO { get; set; }
        public string PERSON_NAME { get; set; }
        public string PERSONCODE { get; set; }
        public int SiteNo { get; set; }
    }
}