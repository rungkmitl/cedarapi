﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataManagement.Entities
{
    public class Master
    {
        public int No { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}