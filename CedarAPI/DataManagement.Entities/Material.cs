﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Material
    {
        public int No { get; set; }
        public string FLAGACT{ get; set; }
        public bool WOResPlan{ get; set; }
        public bool WOResActual{ get; set; }
        public int WONO{ get; set; }
        public string WOCode{ get; set; }
        public string WODATE{ get; set; }
        public string WOTIME{ get; set; }
        public int TaskNo{ get; set; }
        public int TaskCode{ get; set; }
        public string TaskName{ get; set; }
        public string TrDate{ get; set; }
        public string TRTime{ get; set; }
        public int? SymptomNo{ get; set; }
        public string SymptomCode{ get; set; }
        public int? STORENO{ get; set; }
        public string StoreCode{ get; set; }
        public string StoreName{ get; set; }
        public int? PARTNO{ get; set; }
        public string PartCode{ get; set; }
        public string PartName{ get; set; }
        public string OtherCode{ get; set; }
        public string OtherName{ get; set; }
        public string PartTypeCode{ get; set; }
        public int IVUnitNo{ get; set; }
        public string IVUnitCode{ get; set; }

        /// <summary>
        /// หน่วยสำหรับพัสดุปรเภท Non Stock (ใส่ได้อิสระ)
        /// </summary>
        public string NonStockUnitName{ get; set; }
        public float QTY{ get; set; }
        public float UNITCOST{ get; set; }
        public float AMOUNT{ get; set; }
        public int? VendorNo{ get; set; }
        public string VendorCode{ get; set; }
        public string VendorName{ get; set; }
        public int EXPNO{ get; set; }
        public string ExpCode{ get; set; }
        public string ExpName{ get; set; }
        public string Remark{ get; set; }
        public string TrhCode{ get; set; }
        public string RqHsTrhCode{ get; set; }
        public string RefReceiveCode{ get; set; }
        public string DirectPurchase{ get; set; }
        public int FlagNotPartCode{ get; set; }

        public int UPDATEUSER { get; set; }

        public Material()
        {
            No = 0;
            FLAGACT = "F";
            WOResPlan = false;
            WOResActual = false;
            WONO = 0;
            WOCode = "";
            WODATE = "";
            WOTIME = "";
            TaskNo = 0;
            TaskCode = 0;
            TaskName = "";
            TrDate = "";
            TRTime = "";
            SymptomNo = 0;
            SymptomCode = "";
            STORENO = 0;
            StoreCode = "";
            StoreName = "";
            PARTNO = 0;
            PartCode = "";
            PartName = "";
            OtherCode = "";
            OtherName = "";
            PartTypeCode = "";
            IVUnitNo = 0;
            IVUnitCode = "";
            NonStockUnitName = "";
            QTY = 0;
            UNITCOST = 0;
            AMOUNT = 0;
            VendorNo = 0;
            VendorCode = "";
            VendorName = "";
            EXPNO = 0;
            ExpCode = "";
            ExpName = "";
            Remark = "";
            TrhCode = "";
            RqHsTrhCode = "";
            RefReceiveCode = "";
            DirectPurchase = "F";
            FlagNotPartCode = 0;
        }
    }
}
