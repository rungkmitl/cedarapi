﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Part
    {
        public int PartNo { get; set; }
        public string PartCode { get; set; }
        public string PartName { get; set; }
        public string StockType { get; set; }
        public int IVLUnitNo { get; set; }
        public int StoreNo { get; set; }
        public string StoreName { get; set; }
        public string StoreCode { get; set; }
        public float QOnhand { get; set; }
        public float UnitCost { get; set; }
        public string IMG { get; set; }
        public int IVUNITNO { get; set; }
        public string IVUNITCODE{ get; set; }
        public string IVUNITNAME { get; set; }
        public string STOCKTYPE { get; set; }
    }
}
