﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class PartTypes
    {
        public string PartTypeCode { get; set; }
        public int StoreNo_Default { get; set; }
        public string DefaultCaption { get; set; }
        public string LocalLang { get; set; }
        public string InterLang { get; set; }  
    }
}
