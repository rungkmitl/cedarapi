﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Person
    {
        public int PersonNo { get; set; }
        public string PersonCode { get; set; }
        public string PersonName { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string CraftName { get; set; }
    }
}
