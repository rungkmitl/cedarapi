﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class QRCode
    {
        public string QRType { get; set; }
        public int ID { get; set; }
        public int SiteNo { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
    }
}
