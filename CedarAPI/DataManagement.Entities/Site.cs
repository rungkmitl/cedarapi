﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Site
    {
        public int SiteNo { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
    }
}
