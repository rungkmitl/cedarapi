﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class Store
    {
        public int StoreNo { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
    }
}
