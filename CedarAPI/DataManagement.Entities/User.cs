﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class User
    {
        public int PersonNo { get; set; }
        public string UserID { get; set; }
        public string PersonCode { get; set; }
        public string PersonName { get; set; }
        public string Title { get; set; }
        public int DeptNo { get; set; }
        public string DeptCode{ get; set; }
        public string DeptName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int SiteNo { get; set; }
        public string UserGCode { get; set; }
        public string FlagSection { get; set; }
        public string Password { get; set; }
        public int VendorNo { get; set; }
    }
}
