﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class WO
    {
        public int WONo { get; set; }
        public string WOCode { get; set; }
        public string ProblemName { get; set; }
        public string WODate { get; set; }
        public string WOTime { get; set; }
        public string WRTime { get; set; }
        public string WRDATE { get; set; }
        public string WO_PROBLEM { get; set; }
        public string WO_CAUSE { get; set; }
        public string EQCODE { get; set; }
        public string EQNAME { get; set; }
        public string FlagPU { get; set; }
        public string FlagEQ { get; set; }
        public string WoTypeCode { get; set; }
        public string WoTypeName { get; set; }
        public string PUCode { get; set; }
        public string PUName { get; set; }
        public string DeptREQCode { get; set; }
        public string DeptREQName { get; set; }
        public string RequesterName { get; set; }
        public string Sch_Start_D { get; set; }
        public string Sch_Start_T { get; set; }
        public string Sch_Finish_D { get; set; }
        public string Sch_Finish_T { get; set; }
        public double Sch_Duration { get; set; }
        public decimal PlanMHAmountOfQty { get; set; }
        public string Act_Finish_D { get; set; }
        public string Act_Finish_T { get; set; }
        public string Act_Start_D { get; set; }
        public string Act_Start_T { get; set; }
        public double Act_Duration { get; set; }
        public decimal ActMHAmountOfQty { get; set; }
        public double METERDONE { get; set; }
        public decimal ActMHAmountOfCost { get; set; }
        public double StockAmt { get; set; }
        public double DireactPurchaseAmt { get; set; }
        public double ServiceAmt { get; set; }
        public double ToolAmt { get; set; }
        public string EQFailuresCode { get; set; }
        public string EQFailuresName { get; set; }
        public string Component { get; set; }
        public string ProblemCode { get; set; }
        public string WOACTIONCODE { get; set; }
        public string WOACTIONNAME { get; set; }
        public string WOActionDesc { get; set; }
        public string FailureModeCode { get; set; }
        public string FailureModeName { get; set; }
        public string DT_Start_D { get; set; }
        public string DT_Start_T { get; set; }
        public string DT_Finish_D { get; set; }
        public string DT_Finish_T { get; set; }
        public double DT_Duration { get; set; }
        public string PuEffectCode { get; set; }
        public string PuEffectName { get; set; }
        public string WOStatusDesc { get; set; }
        public string WFStatusDesc { get; set; }
        public int SiteNo { get; set; }
        public string SiteCode { get; set; }
        public string Event_Date { get; set; }
        public string Event_Time { get; set; }
        public string WF_FROM_PERSON_NAME { get; set; }
        public IEnumerable<ActionObject> ACTION;
        public string PU { get; set; }
        public string DOCTYPE { get; set; }
        public string PERSONCODE { get; set; }
        public string PERSON_NAME { get; set; }
        public int DFR { get; set; }
        public string SCH_START_D { get; set; }
        public string SCH_START_T { get; set; }
        public string SCH_FINISH_D { get; set; }
        public string SCH_FINISH_T { get; set; }
        public Failure FailureObj;
        public int workBy { get; set; }
        public string workByName { get; set; }
        public string workByCode { get; set; }
        public IEnumerable<Manhours> ManhoursObj;
        public bool HasAttachFile { get; set; }

        public IEnumerable<AttachFileObject> AttachFileObj { get; set; }
        public class AttachFileObject
        {
            public string FileName { get; set; }
            public string DocName { get; set; }
        }
        public string TaskProcedure { get; set; }
    }
}
