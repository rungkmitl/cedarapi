﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.Entities
{
    public class WR
    {
        public int WRNo { get; set; }
        public string WRCode { get; set; }
        public string WRDesc { get; set; }
        public string WRDate { get; set; }
        public string WRTime { get; set; }
        public string WRStatus { get; set; }
        public string NodeName { get; set; }
        public string PUCode { get; set; }
        public string EQCode { get; set; }
        public string Costcentercode { get; set; }
        public string SiteName { get; set; }
        public int SiteNo { get; set; }
        public string DeptCode { get; set; }
        public string Description { get; set; }
        public int? PUNo { get; set; }
        public string PUName { get; set; }
        public int? EQNo { get; set; }
        public string EQName { get; set; }
        public int? CostcenterNo { get; set; }
        public string CostcenterName { get; set; }
        public int? Dept_Rec_No { get; set; }

        public List<ActionObject> ACTION;
        public List<string> ATTACHMENT;

        public string Date { get; set; }
        public string Time { get; set; }
        public string Problem { get; set; }
        public string Attach { get; set; }
        public int Personno { get; set; }
        public string SiteCode { get; set; }
        public string DeptName { get; set; }
        public bool HasAttachFile { get; set; }
        public List<AttachFileObject> AttachFileObj { get; set;}
        public class AttachFileObject
        {
            public string FileName { get; set; }
            public string DocName { get; set; }
        }
    }

   
}
