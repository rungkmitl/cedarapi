﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IActualTimeRepository
    {
        void Update(int WONo, string ActStartD, string ActStartT, string ActFinishD, string ActFinishT
            , string ActDuration, int Assign, int WarrantyDay, int UpdateUser);
    }
}

