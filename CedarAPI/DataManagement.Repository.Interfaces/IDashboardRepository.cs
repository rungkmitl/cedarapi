﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IDashboardRepository
    {
        Dashboard Get(int SiteNo, string WherMaintenanceCost, string WherTotalDowntTime, string WherWorkCompliancy
            , string WherNumberOfWorkOverDue, string WherNumberOfWorkBacklogOver, string WherNumberOfRepetitiveFailedEQ, int DeptNo);
       
    }
}

