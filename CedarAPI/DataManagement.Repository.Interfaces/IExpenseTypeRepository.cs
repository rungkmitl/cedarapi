﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IExpenseTypeRepository
    {
        IEnumerable<ExpenseType> GetBySite(int siteNo);
     

    }
}

