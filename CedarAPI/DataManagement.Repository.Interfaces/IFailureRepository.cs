﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IFailureRepository
    {
        void UpdateSummaryAction(int woNo, int problemNo, string problemDesc, int actionNo, string actionDesc, int causeNo, string causeDesc, string actionStartDate
            , string actionEndDate, int assignNo, string dtStartDate, string dtEndDate, int id, string taskProcedure);  
    }
}

