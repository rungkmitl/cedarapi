﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IInboxRepository
    {
        IEnumerable<Inbox.Detail> Inbox(int personNo, string where);
        Inbox GetUnRead(int personNo);
        void CheckRead(int DocNo, int EventNo, string WFType);
    }
}

