﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IMasterRepository
    {
        IEnumerable<Master> GetWOAction(int SiteNo, string Where);
        IEnumerable<Master> GetINSHead(int SiteNo, string Where);

    }
}

