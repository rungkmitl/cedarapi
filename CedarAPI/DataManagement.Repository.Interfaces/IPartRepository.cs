﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IPartRepository
    {
        IEnumerable<Part> GetByPerson(int PersonNo, string where);
        //WO GetPlanTime(int id);
        //void UpdatePlanTime(int id, string startDate, string endDate);
        //IEnumerable<JobTracking> GetFlowHistory(int id);
        //WO GetFailure(int WONo);
        //IEnumerable<Manhours> GetManhours(int WONo);
        //void UpdateManhours(int WORescNo, string TRDatetime, float hours, int personNo);

    }
}

