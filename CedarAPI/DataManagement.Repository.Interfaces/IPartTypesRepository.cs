﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IPartTypesRepository
    {
        IEnumerable<PartTypes> Get();
     

    }
}

