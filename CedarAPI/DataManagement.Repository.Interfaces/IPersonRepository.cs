﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IPersonRepository
    {
        IEnumerable<Person> GetBySite(int siteNo, string where);
    }
}

