﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataManagement.Repository.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Get();
        IEnumerable<T> Get(string filter);
        T Get(int id);
        void Add(T entity);
        void Delete(int id);
        void Update(int id, T entity);
    }
}
