﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;

namespace DataManagement.Repository.Interfaces
{
    public interface ISiteRepository
    {
        IEnumerable<Site> ByPerson(int PersonNo);
        Site ById(int SiteNo);
    }
}
