﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IUserRepository
    {
        User Login(string lang, string uid, string pwd);
        User GetByID(int personNo);
    }
}

