﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IWORepository
    {
        IEnumerable<WO> Get(int DeptNo, string FlagSection, int PersonNo, string PersonCode, string Where, int PUNo, int EQNo);
        WO Get(int id);
        WO GetPlanTime(int id);
        void UpdatePlanTime(int id, string startDate, string endDate);
        IEnumerable<JobTracking> GetFlowHistory(int id);
        WO GetFailure(int WONo);
        IEnumerable<Manhours> GetManhours(int WONo);
        void UpdateManhours(int WORescNo, string TRDatetime, float hours, int personNo);
        IEnumerable<Material> GetMaterial(int WONo);
        Material GetMaterialByWORescNo(int WORescNo);
        void InsertMaterial(Material Material);
        void UpdateMaterial(Material Material);
        void DeleteMaterial(int WORescNo);

        int CheckPincode(int WONo, string Pincode);
        int InsertSignout(string PUCode, bool FlagCheckJob, string WOCode, int WONo, int UpdatedBy, string Remark);
    }
}

