﻿using System;
using DataManagement.Entities;
using System.Collections.Generic;
namespace DataManagement.Repository.Interfaces
{
    public interface IWRRepository
    {
        IEnumerable<WR> Get(int DeptNo, string FlagSection, int PersonNo, string PersonCode, string Where, int PUNo, int EQNo);
        WR GetByID(int WRNo, int PersonNo);
        int SaveWR(string Date, string Time, int? PUNo, int? EQNo, int? CostcenterNo, string Problem, int SiteNO, int PersonNO, int WRNo, string Attach);

    }
}

