﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class ActualTimeRepository : BaseRepository, IActualTimeRepository
    {

        public void Update(int WONo, string ActStartD, string ActStartT, string ActFinishD, string ActFinishT
            , string ActDuration, int Assign, int WarrantyDay, int UpdateUser)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@WONo", WONo);
            parameters.Add("@ActStartD", ActStartD);
            parameters.Add("@ActStartT", ActStartT);
            parameters.Add("@ActFinishD", ActFinishD);
            parameters.Add("@ActFinishT", ActFinishT);
            parameters.Add("@ActDuration", ActDuration);
            parameters.Add("@Assign", Assign);
            parameters.Add("@WarrantyDay", WarrantyDay);
            parameters.Add("@UpdateUser", UpdateUser);

            /*string sql = "  declare @now datetime = cast(convert(char(8), getdate(), 112) + ' 23:59:59.99' as datetime) ";
            sql += " select @ValMaintenanceCost = ISNULL(SUM(WO.ActMHAmountOfCost), 0) + ISNULL(SUM(WO.ActMTAmountOfCost), 0) from WO ";
            sql += " where  WO.ACT_FINISH_D between dbo.GetStartDateByType(@WherMaintenanceCost) and @now ";
            sql += " and WO.SiteNo = @SiteNo and WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";

            sql += " select @ValTotalDowntTime = ISNULL(sum(dbo.ConvertMinuteToDuration(isnull(DATEDIFF(MINUTE, convert(datetime, WO.DT_Start_D, 112) + CAST(STUFF(isnull(WO.DT_Start_T, '0000'), 3, 0, ':') AS time), convert(datetime, WO.DT_Finish_D, 112) + CAST(STUFF(isnull(WO.DT_Finish_T, '0000'), 3, 0, ':') AS time)), 0))),0) from WO ";
            sql += " where  WO.DT_Start_D between dbo.GetStartDateByType(@WherTotalDowntTime) and @now ";
            sql += " and WO.FlagPU = 'T' and WO.SiteNo = @SiteNo and  WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";

            sql += " declare @TotalJob float, @FinishJob float ";
            sql += " select @TotalJob = COUNT(*) from WO ";
            sql += " where  WO.WODATE between dbo.GetStartDateByType(@WherWorkCompliancy) and @now ";
            sql += " and WO.SiteNo = @SiteNo and  WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";
            sql += " select @FinishJob = COUNT(*) from WO ";
            sql += " where  WO.ACT_FINISH_D between dbo.GetStartDateByType(@WherWorkCompliancy) and @now ";
            sql += " and WO.SiteNo = @SiteNo and  WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";
            sql += "  if  @TotalJob <> 0";
            sql += " begin  select @ValWorkCompliancy = (@FinishJob / @TotalJob)  end  else set @ValWorkCompliancy = 0 ";

            sql += " select @ValNumberOfWorkOverDue = COUNT(*) from WO ";
            sql += " where WO.WODATE between dbo.GetStartDateByType(@WherNumberOfWorkOverDue) and @now  ";
            sql += " and ACT_FINISH_D > SCH_FINISH_D and WO.SiteNo = @SiteNo and WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";

            sql += " select @ValNumberOfWorkBacklog = count(*) from WO ";
            sql += " left join  WOStatus on WO.WOSTATUSNO = WOStatus.WOSTATUSNO ";
            sql += " where  WOStatus.WOSTATUSCODE < 95 and  WO.SiteNo = @SiteNo and WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";

            sql += " select @ValNumberOfWorkBacklogOver = count(*) from WO ";
            sql += " left join  WOStatus on WO.WOSTATUSNO = WOStatus.WOSTATUSNO ";
            sql += " where  cast(isnull(WO.WRDATE, WO.WODATE) as datetime) <= dbo.GetStartDateByType(@WherNumberOfWorkBacklogOver) ";
            sql += " and  WOStatus.WOSTATUSCODE < 95 and  WO.SiteNo = @SiteNo and WO.DEPTNO = CASE WHEN @DeptNo = 0 THEN WO.DEPTNO ELSE @DeptNo END ";

            sql += " select @ValNumberOfRepetitiveFailedEQ = COUNT(*) ";
            sql += " from ( select count(wo.wono) cnt, EQNO from WO ";
            sql += " where WO.WODATE between dbo.GetStartDateByType(@WherNumberOfRepetitiveFailedEQ) and @now ";
            sql += " and (WO.WOTYPENO = 1 or WO.WOTYPENO = 2) ";
            sql += " and WO.SiteNo = @SiteNo ";
            sql += " group by EQNO ";
            sql += " having count(wo.WONO) > 1 and EQNO <> 0 ) q ";

            SqlMapper.Execute(con, sql, parameters, commandType: Text);
            Dashboard d = new Dashboard();
            //Getting the out parameter value of stored procedure  
            d.ValMaintenanceCost = parameters.Get<float>("@ValMaintenanceCost");
            d.ValNumberOfRepetitiveFailedEQ = parameters.Get<float>("@ValNumberOfRepetitiveFailedEQ");
            d.ValNumberOfWorkBacklog = parameters.Get<float>("@ValNumberOfWorkBacklog");
            d.ValNumberOfWorkBacklogOver = parameters.Get<float>("@ValNumberOfWorkBacklogOver");
            d.ValNumberOfWorkOverDue = parameters.Get<float>("@ValNumberOfWorkOverDue");
            d.ValTotalDowntTime = parameters.Get<float>("@ValTotalDowntTime");
            d.ValWorkCompliancy = parameters.Get<float>("@ValWorkCompliancy");

            return d;*/
            SqlMapper.Execute(con, "sp_WOMain_Action_WorkFinish_Update", parameters, commandType: StoredProcedure);
        }
    }
}
