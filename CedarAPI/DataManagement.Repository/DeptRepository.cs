﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class DeptRepository : BaseRepository, IDeptRepository
    {

        public IEnumerable<Dept> GetForDashboard(int PersonNo, int SiteNo)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@PersonNo", PersonNo);
            parameters.Add("@SiteNo", SiteNo);


            string sql = "  select 0 as DEPTNO, 'All Dept' as DeptDesc " +
                " union" +
                " select Dept.DEPTNO , Dept.DEPTCODE + ' : ' + Dept.DEPTNAME as DeptDesc" +
                " from WO  inner join  dept on Dept.DEPTNO = WO.DEPTNO" +
                " inner join person_site on person_site.SiteNO = WO.SiteNo " +
                " inner join person on person_site.PersonNo = person.PersonNo" +
                " inner join Dept_Site on Dept_Site.DeptNo = WO.SiteNo " +
                " where Person.PersonNo = @PersonNo and WO.SiteNo = @SiteNo" +
                " group by Dept.DEPTNO, Dept.DEPTCODE , Dept.DEPTNAME ";

            return SqlMapper.Query<Dept>(con, sql, parameters, commandType: Text).ToList();

        }
    }
}
