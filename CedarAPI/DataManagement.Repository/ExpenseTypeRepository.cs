﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class ExpenseTypeRepository : BaseRepository, IExpenseTypeRepository
    {

        public IEnumerable<ExpenseType> GetBySite(int siteNo)
        {
            string p_strWhere = " WHERE me.FLAGDEL='F' AND me.SiteNo=" + GetWhereSite("ExpenseType", siteNo);

            //if (InputVal.ToString(where) != "")
            //{
            //    p_strWhere += " AND (Iv_Catalog.EXPCODE like '%" + where + "%'  or Iv_Catalog.PARTNAME like '%" + where + "%')";
            //}

            string sql = "Select me.EXPNO,me.EXPCODE,me.EXPNAME, " +
                "  me.EXPPARENT,par.EXPCODE AS EXPPARENTCODE,par.EXPNAME AS EXPPARENTNAME," +
                "  me.FLAGDEL" +
                " FROM  ExpenseType me" +
                " left Join ExpenseType par on me.EXPPARENT = par.EXPNO" + p_strWhere;
            return SqlMapper.Query<ExpenseType>(con, sql, commandType: Text).ToList();
        }
        protected string GetWhereSite(string Table, int SiteNo)
        {
            return "dbo.GetWhereSite('" + Table + "'," + SiteNo + ")";
        }


    }
}
