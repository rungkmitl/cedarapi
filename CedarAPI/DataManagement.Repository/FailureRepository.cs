﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class FailureRepository : BaseRepository, IFailureRepository
    {

        public void UpdateSummaryAction(int woNo, int problemNo, string problemDesc, int actionNo, string actionDesc, int causeNo, string causeDesc, string actionStartDate
            , string actionEndDate, int assignNo, string dtStartDate, string dtEndDate, int itemNo, string taskProcedure)
        {
            using (var scope = new TransactionScope())
            {
                DynamicParameters parameters = new DynamicParameters();

                double duration = 0, downtimeDuration;
                DateData DateData = new DateData();
                if (actionStartDate != "")
                {
                    CommonTools.InputVal.ConvertStartDateToString(actionStartDate, ref DateData);
                }
                else
                {
                    DateData.StartD = null;
                    DateData.StartT = null;
                }

                if (actionEndDate != "")
                {
                    CommonTools.InputVal.ConvertFinshDateToString(actionEndDate, ref DateData);
                    duration =  DurationVal.ToFloat((InputVal.CalculateDurationToMin(DateData.StartDate, DateData.FinishDate)));
                }
                else
                {
                    DateData.FinishD = null;
                    DateData.FinishT = null;
                    duration = 0;
                }
                DateData downtimeData = new DateData();
                if (InputVal.ToString(dtStartDate) != "")
                {
                    CommonTools.InputVal.ConvertStartDateToString(dtStartDate, ref downtimeData);
                }
                else
                {
                    downtimeData.StartD = null;
                    downtimeData.StartT = null;
                }

                if (InputVal.ToString(dtEndDate) != "")
                {
                    CommonTools.InputVal.ConvertFinshDateToString(dtEndDate, ref downtimeData);
                    downtimeDuration = CommonTools.InputVal.CalculateDuration(downtimeData.StartDate, downtimeData.FinishDate);
                }
                else
                {
                    downtimeData.FinishD = null;
                    downtimeData.FinishT = null;
                    downtimeDuration = 0;
                }


                string sql = " update WO set " +
                    " ACT_START_D = @ACT_START_D" +
                    " ,ACT_START_T = @ACT_START_T" +
                    " ,ACT_FINISH_D = @ACT_FINISH_D" +
                    " ,ACT_FINISH_T = @ACT_FINISH_T" +
                    " ,ACT_DURATION = @ACT_DURATION" +
                    " ,WORKBY = @WORK_BY" +
                    " ,DT_Start_D = @DT_Start_D" +
                    " ,DT_Start_T = @DT_Start_T" +
                    " ,DT_Finish_D = @DT_Finish_D" +
                    " ,DT_Finish_T = @DT_Finish_T" +
                    " ,DT_Duration = @DT_Duration" +
                    " ,taskProcedure =@TaskProcedure"+
                    " where WO.wono = @wono " +

                    " declare @fmitem int = 0, @puno int =0, @eqno int =0 " +
                    " select @fmitem = isnull(WO_Failures.FMITEM,0), @eqno = wo.EQNO, @puno = wo.PUNO " +
                    " from WO_Failures right join wo on " +
                    " WO_Failures.WONo = wo.WONo " +
                    " where WO.wono = @wono " +

                    " if @fmitem = 0  " +
                    " begin " +
                        " insert into WO_Failures (WONo, PUNo, EQNo, FailureModeNo, FailureModeDes, WOActionNo, WOActionDesc, ProblemNo, FlagDel)" +
                        " values( @wono ,@puno, @eqno,  @causeNo , @causeDesc ,@actionNo ,@actionDesc , @problemNo ,'F' )" +
                    " end " +
                    " else" +
                    " begin " +
                        " update WO_Failures set " +
                        " FailureModeNo = @causeNo" +
                        " , FailureModeDes = @causeDesc " +
                        " , WOActionNo = @actionNo" +
                        " , WOActionDesc = @actionDesc" +
                        " , ProblemNo = @problemNo" +
                        " where FMITEM = @fmitem" +
                    " end";

                parameters = new DynamicParameters();
                parameters.Add("@ACT_START_D", DateData.StartD);
                parameters.Add("@ACT_START_T", DateData.StartT);
                parameters.Add("@ACT_FINISH_D", DateData.FinishD);
                parameters.Add("@ACT_FINISH_T", DateData.FinishT);
                parameters.Add("@ACT_DURATION", duration);
                parameters.Add("@WORK_BY", assignNo);
                parameters.Add("@DT_Start_D", downtimeData.StartD);
                parameters.Add("@DT_Start_T", downtimeData.StartT);
                parameters.Add("@DT_Finish_D", downtimeData.FinishD);
                parameters.Add("@DT_Finish_T", downtimeData.FinishT);
                parameters.Add("@DT_Duration", downtimeDuration);
                parameters.Add("@wono", woNo);
                parameters.Add("@causeNo", causeNo);
                parameters.Add("@causeDesc", causeDesc);
                parameters.Add("@actionNo", actionNo);
                parameters.Add("@actionDesc", actionDesc);
                parameters.Add("@problemNo", problemNo);
                parameters.Add("@itemNo", itemNo);
                parameters.Add("@TaskProcedure", taskProcedure);
                

                SqlMapper.Execute(con, sql, parameters, commandType: Text);


                parameters = new DynamicParameters();
                parameters.Add("@WONo", woNo);
                parameters.Add("@ACT_START_D", DateData.StartD);
                parameters.Add("@ACT_START_T", DateData.StartT);
                parameters.Add("@ACT_FINISH_D", DateData.FinishD);
                parameters.Add("@ACT_FINISH_T", DateData.FinishT);
                parameters.Add("@ACT_DURATION", duration);
                parameters.Add("@ASSIGN", assignNo);
                parameters.Add("@WarrantyDay", 0);
                parameters.Add("@UpdateUser", 0);

                SqlMapper.Execute(con, "sp_WOMain_Action_WorkFinish_Update", parameters, commandType: StoredProcedure);


                parameters = new DynamicParameters();
                parameters.Add("@WONO", woNo);
                parameters.Add("@ACTIONNO", 0);
                parameters.Add("@DESC", "");
                parameters.Add("@UPDATEUSER", 0);

                SqlMapper.Execute(con, "sp_WFN_EXEC_NODE_WO", parameters, commandType: StoredProcedure);
               
                scope.Complete();
            }
        }
    }
}
