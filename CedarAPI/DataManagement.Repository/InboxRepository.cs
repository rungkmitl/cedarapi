﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class InboxRepository : BaseRepository, IInboxRepository
    {

        public IEnumerable<Inbox.Detail> Inbox(int personNo, string where)
        {
           
            string p_strWhere = "";

            int p_VendorNo = 0;
          
            User userObj = new UserRepository().GetByID(personNo);
            p_VendorNo = CommonTools.InputVal.ToInt(userObj.VendorNo);

          

            p_strWhere = " where (WRStatus.WRSTATUSCODE < 99 OR WOStatus.WOSTATUSCODE < 99) AND ";

            p_strWhere += " (me.Approved_Flag ='F' AND (Send_for=1 OR (Send_for=2 AND Readed_Flag='F'))) ";

            p_strWhere += " AND (me.Receive_PersonNo = " + personNo;
            p_strWhere += " OR USERGROUP_MEMBER.PERSON = " + personNo + ") ";

            if (p_VendorNo > 0)
            {
                p_strWhere += " AND (me.WFDocFlowCode='WR' OR (me.WFDocFlowCode='WO' AND WO.VendorNo=" + p_VendorNo + ")) ";
            }

            string p_strWhereOutside = "";
            if (InputVal.ToString(where) != "")
            {
                p_strWhereOutside += "where (DocCode like '%" + where + "%' OR PU.PUCODE like '%" + where + "%'  OR PU.PUNAME like '%" + where + "%' " +
                    " OR WO_PU.PUCODE like '%" + where + "%' OR WO_PU.PUNAME like '%" + where + "%' )";
            }

            
            string sql = " SELECT top 50 " +
            " Inbox.* " +
            " ,CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PU.PUNO ELSE WO_PU.PUNO END AS PUNO " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PU.PUCODE ELSE WO_PU.PUCODE END AS PUCODE " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PU.PUNAME ELSE WO_PU.PUNAME END AS PUNAME " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN EQ.EQCODE ELSE WO_EQ.EQCODE END AS EQCODE " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN EQ.EQNAME ELSE WO_EQ.EQNAME END AS EQNAME " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PU.Text3 ELSE WO_PU.Text3 END AS Text3 " +
            " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN WR.WRDESC ELSE WO.WO_PROBLEM END AS WO_PROBLEM " +
            " , Vendor.VENDORNO,Vendor.VENDORCODE,Vendor.VENDORNAME " +
            " ,FMD.PERSONCODE AS FMDCode,FMD.FIRSTNAME + ' ' + FMD.LASTNAME AS FMDName " +
            " , WO.Hotwork,WO.ConfineSpace " +
            " ,WR.WRDESC " +
            " ,WOType.WOTypeCode,WOType.WOTypeName " +
            " ,WOSubType.WOSubTypeCode,WOSubType.WOSubTypeName " +
            " ,CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PU.PULOCTYPENO ELSE WO_PU.PULOCTYPENO END AS PULOCTYPENO " +
              " , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PULocType.PuLocTypeCode ELSE WO_PULocType.PuLocTypeCode END AS PuLocTypeCode " +
               "  , CASE WHEN Inbox.WFDocFlowCode = 'WR' THEN PULocType.PuLocTypeName ELSE WO_PULocType.PuLocTypeName END AS PuLocTypeName " +
            " FROM " +
             "( " +
              "  SELECT " +

                    " WFTypeName, DocCode, Subject, Event_Desc, Send_For, " +
                    " CASE WHEN Send_For = 1 THEN 'Approve' ELSE 'Action' END AS SendForText, " +
                    " isnull(WOSTATUSCODE + ' ' + WOSTATUSNAME, WRSTATUSCODE + ' ' + WRSTATUSNAME) as STATUSCODE, " +
                    " FIRSTNAME + ' ' + LASTNAME as PERSONNAME, " +
                    " From_Time as asFromTime, " +
                    " dbo.ShowFormatTime(From_Time) as FromTime, " +
                    " From_Date as asFromDate, " +
                    " convert(datetime, From_Date, 112) as FromDate, " +
                    " Readed_Flag, Approved_Flag, " +
                    " DocNo, Event_Order, me.DocStatusNo, me.WFDocFlowCode, me.SiteNo, SiteCode, " +
                    " WFStepNo, InboxNo, ACTIONNAME  , " +
                    " ( select top 1 case  when  Attach_Doc.DOCNO is not null then 1 else 0 end from Attach_Doc where DOCNO = me.DocNo) as HasAttachFile " +
                " from WFTrackeds me " +
                " left " +
                " join WFTypes on WFTypes.WFTypeCode = me.WFDocFlowCode " +

            " left Join WRSTATUS on me.DocStatusNo = WRSTATUS.WRSTATUSNO AND me.WFDocFlowCode = 'WR' " +

              "  left Join WOSTATUS on me.DocStatusNo = WOSTATUS.WOSTATUSNO AND me.WFDocFlowCode = 'WO' " +
              "  left join Person on me.From_PersonNo = Person.PERSONNO " +

              "  left join USERGROUP_MEMBER on me.Receive_UserGroupNo = USERGROUP_MEMBER.USERGROUPNO " +

                " left join wf_node_action on me.ACTIONNO = wf_node_action.ACTIONNO " +

               "  LEFT JOIN WO ON WO.WONO = me.DocNo AND me.WFDocFlowCode = 'WO' " +
                "  LEFT JOIN PU ON WO.PUNO = PU.PUNO " +
               " left join[Site] on[Site].SiteNo = me.SiteNo  " + p_strWhere +
             
               
            ") AS Inbox " +
            " LEFT JOIN WR ON WR.WRNO = Inbox.DocNo AND WR.SiteNo = Inbox.SiteNo AND Inbox.WFDocFlowCode = 'WR' " +
            " LEFT JOIN WO ON WO.WONO = Inbox.DocNo AND WO.SiteNo = Inbox.SiteNo AND Inbox.WFDocFlowCode = 'WO' " +
            " LEFT JOIN WOType ON WO.WOTypeNo = WOType.WOTypeNo " +
            " LEFT JOIN WOSubType ON WO.WOSubTypeNo = WOSubType.WOSubTypeNo " +
            " LEFT JOIN PU ON WR.PUNO = PU.PUNO " +
            " LEFT JOIN EQ ON WR.EQNO = EQ.EQNO " +
            " LEFT JOIN PULocType ON PU.PULOCTYPENO = PULocType.PULOCTYPENO " +
            " LEFT JOIN PU AS WO_PU ON WO.PUNO = WO_PU.PUNO " +
            " LEFT JOIN EQ AS WO_EQ ON WO.EQNO = WO_EQ.EQNO " +
            " LEFT JOIN PULocType AS WO_PULocType  ON WO_PU.PULOCTYPENO = WO_PULocType.PULOCTYPENO " +
            " LEFT JOIN Vendor ON WO.VENDORNO = Vendor.VENDORNO " +
            " LEFT JOIN Person AS FMD ON WO.WORKBY = FMD.PERSONNO " +
            p_strWhereOutside +
            " ORDER BY Inbox.FromDate Desc, Inbox.FromTime Desc, Inbox.Event_Order Desc "; 

            
            return SqlMapper.Query<Inbox.Detail>(con, sql, commandType: Text).ToList();

        }

        public Inbox GetUnRead(int personNo)
        {

            string p_strWhere = "";

            int p_VendorNo = 0;

            User userObj = new UserRepository().GetByID(personNo);
            p_VendorNo = CommonTools.InputVal.ToInt(userObj.PersonNo);



            p_strWhere = " where (WRStatus.WRSTATUSCODE < 99 OR WOStatus.WOSTATUSCODE < 99) AND ";

            p_strWhere += " (me.Approved_Flag ='F' AND (Send_for=1 OR (Send_for=2 AND Readed_Flag='F'))) AND Readed_Flag='F' ";

            p_strWhere += " AND (me.Receive_PersonNo = " + personNo;
            p_strWhere += " OR USERGROUP_MEMBER.PERSON = " + personNo + ") ";

            if (p_VendorNo > 0)
            {
                p_strWhere += " AND (me.WFDocFlowCode='WR' OR (me.WFDocFlowCode='WO' AND WO.VendorNo=" + p_VendorNo + ")) ";
            }
            string sql = " SELECT COUNT(*) as UnReadTotal" +
            " FROM " +
             "( " +
              "  SELECT " +

                    " WFTypeName, DocCode, Subject, Event_Desc, Send_For, " +
                    " CASE WHEN Send_For = 1 THEN 'Approve' ELSE 'Action' END AS SendForText, " +
                    " isnull(WOSTATUSCODE + ' ' + WOSTATUSNAME, WRSTATUSCODE + ' ' + WRSTATUSNAME) as STATUSCODE, " +
                    " FIRSTNAME + ' ' + LASTNAME as PERSONNAME, " +
                    " From_Time as asFromTime, " +
                    " dbo.ShowFormatTime(From_Time) as FromTime, " +
                    " From_Date as asFromDate, " +
                    " convert(datetime, From_Date, 112) as FromDate, " +
                    " Readed_Flag, Approved_Flag, " +
                    " DocNo, Event_Order, me.DocStatusNo, me.WFDocFlowCode, me.SiteNo, SiteCode, " +
                    " WFStepNo, InboxNo, ACTIONNAME " +
                " from WFTrackeds me " +
                " left " +
                " join WFTypes on WFTypes.WFTypeCode = me.WFDocFlowCode " +

            " left Join WRSTATUS on me.DocStatusNo = WRSTATUS.WRSTATUSNO AND me.WFDocFlowCode = 'WR' " +

              "  left Join WOSTATUS on me.DocStatusNo = WOSTATUS.WOSTATUSNO AND me.WFDocFlowCode = 'WO' " +
              "  left join Person on me.From_PersonNo = Person.PERSONNO " +

              "  left join USERGROUP_MEMBER on me.Receive_UserGroupNo = USERGROUP_MEMBER.USERGROUPNO " +

                " left join wf_node_action on me.ACTIONNO = wf_node_action.ACTIONNO " +

               "  LEFT JOIN WO ON WO.WONO = me.DocNo AND me.WFDocFlowCode = 'WO' " +
               " left join[Site] on[Site].SiteNo = me.SiteNo  " + p_strWhere +
            ") AS Inbox " +
            " LEFT JOIN WR ON WR.WRNO = Inbox.DocNo AND WR.SiteNo = Inbox.SiteNo AND Inbox.WFDocFlowCode = 'WR' " +
            " LEFT JOIN WO ON WO.WONO = Inbox.DocNo AND WO.SiteNo = Inbox.SiteNo AND Inbox.WFDocFlowCode = 'WO' " +
            " LEFT JOIN WOType ON WO.WOTypeNo = WOType.WOTypeNo " +
            " LEFT JOIN WOSubType ON WO.WOSubTypeNo = WOSubType.WOSubTypeNo " +
            " LEFT JOIN PU ON WR.PUNO = PU.PUNO " +
            " LEFT JOIN EQ ON WR.EQNO = EQ.EQNO " +
            " LEFT JOIN PULocType ON PU.PULOCTYPENO = PULocType.PULOCTYPENO " +
            " LEFT JOIN PU AS WO_PU ON WO.PUNO = WO_PU.PUNO " +
            " LEFT JOIN EQ AS WO_EQ ON WO.EQNO = WO_EQ.EQNO " +
            " LEFT JOIN PULocType AS WO_PULocType  ON WO_PU.PULOCTYPENO = WO_PULocType.PULOCTYPENO " +
            " LEFT JOIN Vendor ON WO.VENDORNO = Vendor.VENDORNO " +
            " LEFT JOIN Person AS FMD ON WO.WORKBY = FMD.PERSONNO ";

          

            return SqlMapper.Query<Inbox>(con, sql, commandType: Text).FirstOrDefault();

        }

        public void CheckRead(int DocNo, int EventNo, string WFType)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@DocNo", DocNo);
            parameters.Add("@WFType", WFType);
            parameters.Add("@Event_Order", EventNo);
            SqlMapper.Execute(con, "sp_WaitApprove_Read", parameters, commandType: StoredProcedure);
        }
    }
}
