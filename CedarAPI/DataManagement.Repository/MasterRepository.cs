﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class MasterRepository : BaseRepository, IMasterRepository
    {

        public IEnumerable<Master> GetWOAction(int SiteNo, string Where)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@SiteNo", SiteNo);

            string p_StrWhere = "";
            if (InputVal.ToString(Where) != "")
            {
                p_StrWhere += string.Format(" AND (WOACTIONCODE like '%{0}%' OR WOACTIONNAME like '%{0}%') " , Where);
            }

            string sql = "  select top 30 No = WOACTIONNO, Code = WOACTIONCODE, Name = WOACTIONNAME " +
                " from WOAction " +
                " where FLAGDEL = 'F' " +
                p_StrWhere;

            return SqlMapper.Query<Master>(con, sql, parameters, commandType: Text).ToList();
        }
        public IEnumerable<Master> GetINSHead(int SiteNo, string Where)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@SiteNo", SiteNo);

            string p_StrWhere = "";
            if (InputVal.ToString(Where) != "")
            {
                p_StrWhere += string.Format(" AND (INSPCODE like '%{0}%' OR INSPNAME like '%{0}%') ", Where);
            }

            string sql = "  select top 30 No = INSPNO, Code = INSPCODE, Name = INSPNAME " +
                " from INSP_HEAD " +
                " where FLAGDEL = 'F' " +
                p_StrWhere;

            return SqlMapper.Query<Master>(con, sql, parameters, commandType: Text).ToList();
        }
    }
}
