﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class PartRepository : BaseRepository, IPartRepository
    {

        public IEnumerable<Part> GetByPerson(int personNo, string where)
        {
            string p_strWhere = " WHERE Person_StoreLoc.PersonNo =" + personNo;

            if (InputVal.ToString(where) != "")
            {
                p_strWhere += " AND (Iv_Catalog.PARTCODE like '%" + where + "%'  or Iv_Catalog.PARTNAME like '%" + where + "%')";
            }

            string sql = " SELECT top 50 Iv_Catalog.PARTNO, Iv_Catalog.PARTCODE, Iv_Catalog.PARTNAME ,Iv_Store.UnitCost,IV_STORE.QOnHand,Iv_Catalog.IMG " +
                    " ,IV_STORE.StoreNo,Store.StoreCode,Store.StoreName " +
                    " ,IVUNITCODE,IVUNITNAME, Iv_Catalog.IVUNITNO  " +
                    " , Iv_Catalog.STOCKTYPE  " +
                    " FROM Iv_Catalog " +
                    "  LEFT JOIN IVUnit ON Iv_Catalog.IVUNITNO = IVUNIT.IVUNITNO " +
                  "   INNER JOIN Iv_Store ON Iv_Catalog.PartNo = Iv_Store.PartNo" +
                  " INNER JOIN Person_StoreLoc " +
                  " ON Iv_Store.StoreNo = Person_StoreLoc.STORELocNO " +
                  " LEFT JOIN Store " +
                  " ON Iv_Store.StoreNo = Store.StoreNo "  + p_strWhere;

           
            return SqlMapper.Query<Part>(con, sql, commandType: Text).ToList();
        }

     
    }
}
