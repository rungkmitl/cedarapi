﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class PartTypesRepository : BaseRepository, IPartTypesRepository
    {

        public IEnumerable<PartTypes> Get()
        {
            string sql = "SELECT * FROM Iv_PartTypes ";

            return SqlMapper.Query<PartTypes>(con, sql, commandType: Text).ToList();
        }

     
    }
}
