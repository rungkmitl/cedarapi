﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class PersonRepository : BaseRepository, IPersonRepository
    {

        public IEnumerable<Person> GetBySite(int siteNo, string where)
        {
            string p_strWhere = "WHERE Person.FLAGDEL='F' AND Dept_Site.SiteNo=" + siteNo;

            if (InputVal.ToString(where) != "")
            {
                p_strWhere += " AND (Person.PERSON_NAME like '%" + where + "%'  or Person.PERSONCODE like '%" + where + "%'  or Dept.DEPTCODE like '%" + where + "%')";
            }

            string sql = " SELECT top 50 Person.PERSONNO, Person.PERSONCODE, Person.FIRSTNAME, Person.LASTNAME,Person.PERSON_NAME,Person.FIRSTNAME + ' ' + Person.LASTNAME AS PersonName, " +
                    " Person.DEPTNO, Dept.DEPTCODE,Dept.DEPTNAME, " +
                    " Person.TITLENO, Title.TITLECODE, Title.TITLENAME, Person.CRAFTNO, Craft.CRAFTCODE,  " +
                    " Craft.CRAFTNAME, Person.RATE, Person.CREWNO, Crew.CREWCODE, Crew.CREWNAME, Person.PersonApproveNo,  " +
                    " Person_2.PERSONCODE AS PersonApproveCode, Person_2.FIRSTNAME AS PersonApproveFirstName, " +
                    " Person.FLAGDEL,Person.PHONE,Person.EMAIL,Person.Host,Person.Port,Person.EnableSsl, " +
                  "    Person.TITLE " +
                  " FROM  Person LEFT OUTER JOIN Person AS Person_2 ON Person.PersonApproveNo = Person_2.PERSONNO " +
                  " LEFT OUTER JOIN Person AS Person_1 ON Person.UPDATEUSER = Person_1.PERSONNO " +
                  " LEFT OUTER JOIN Crew ON Person.CREWNO = Crew.CREWNO " +
                  " LEFT OUTER JOIN Craft ON Person.CRAFTNO = Craft.CRAFTNO " +
                 "  LEFT OUTER JOIN Dept ON Person.DEPTNO = Dept.DEPTNO " +
                  " LEFT OUTER JOIN Title ON Person.TITLENO = Title.TITLENO  " +
                  " INNER JOIN Dept_Site ON DEPT.DEPTNO = Dept_Site.DEPTNO " + p_strWhere;

           
            return SqlMapper.Query<Person>(con, sql, commandType: Text).ToList();
        }

     
    }
}
