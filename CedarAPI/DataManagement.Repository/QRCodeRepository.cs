﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class QRCodeRepository : BaseRepository, IQRCodeRepository
    {
        public QRCode GetQRType(string qrVal)
        {
            string sql = " declare @code nvarchar(200) = @qrVal, @type nvarchar(200), @cnt int =0, @id int = 0, @siteno int= 0 , @sitecode nvarchar(10)= '' , @sitename nvarchar(50)= '' " +
                " if @type is null " +
                " begin " +
                " select @cnt = count(*), @id = max(PU.PUNO), @siteno= max(PU.siteno), @sitecode= max(Site.SiteCode), @sitename= max(Site.SiteName) " +
                " from PU inner join Site on Site.SiteNo = PU.SiteNo " +
                " where PU.PUCODE = @code " +
                " if @cnt > 0 " +
                " set @type = 'PU' " +
                " end " +
                "  if @type is null " +
                " begin " +
                " select @cnt = count(*), @id = max(EQ.eqno), @siteno= max(EQ.siteno), @sitecode= max(Site.SiteCode), @sitename= max(Site.SiteName) " +
                " from EQ inner join Site on Site.SiteNo = EQ.SiteNo  " +
                " where EQ.EQCODE = @code " +
                " if @cnt > 0 " +
                " set @type = 'EQ' " +
                " end " +
                " if @type is null" +
                " begin " +
                " select @cnt = count(*), @id = max(wo.wono)" +
                "  from wo" +
                "  where wo.WOCODE = @code" +
                "  if @cnt > 0 " +
                "  set @type = 'WO'" +
                " end" +
                " select @id as ID, @type as QRType, @siteno as SiteNo , @sitecode as SiteCode , @sitename as SiteName";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@qrVal", qrVal);
           
            return SqlMapper.Query<QRCode>(con, sql, parameters, commandType: Text).FirstOrDefault();
        }

      
    }
}
