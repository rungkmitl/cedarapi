﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class SiteRepository : BaseRepository, ISiteRepository
    {
        public IEnumerable<Site> ByPerson(int PersonNo)
        {
            string sql = " select Site.SiteNo,Site.SiteCode, Site.SiteName as SiteName " +
                " from person_site " +
                " inner join Site on person_site.SiteNO = Site.SiteNo " +
                " where person_site.SiteNo > 1 and PERSONNO= " + PersonNo; 

            return SqlMapper.Query<Site>(con, sql, commandType: Text).ToList();
        }

        public Site ById(int SiteNo)
        {
            string sql = " select Site.SiteNo,Site.SiteCode, Site.SiteName as SiteName " +
                " from Site where SiteNo= " + SiteNo;
               
            return SqlMapper.Query<Site>(con, sql, commandType: Text).FirstOrDefault();
        }


    }
}
