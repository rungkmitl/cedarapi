﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class StoreRepository : BaseRepository, IStoreRepository
    {

        public IEnumerable<Store> GetByPart(int partNo, int personNo, string where)
        {
            string p_strWhere = "WHERE Store.FLAGDEL='F' ";
            p_strWhere += " AND Iv_Store.PartNo=" + partNo;
            p_strWhere += " AND Person_StoreLoc.PersonNo=" + personNo;
            if (InputVal.ToString(where) != "")
            {
                p_strWhere += " AND (Store.StoreCODE like '%" + where + "%'  or Store.StoreNAME like '%" + where + "%')";
            }

            string sql = " SELECT Store.* FROM Store " +
                " INNER JOIN Iv_Store ON Store.StoreNo = Iv_Store.StoreNo " +
                " INNER JOIN Person_StoreLoc " +
                " ON Store.StoreNo = Person_StoreLoc.STORELocNO "  + p_strWhere;

           
            return SqlMapper.Query<Store>(con, sql, commandType: Text).ToList();
        }

     
    }
}
