﻿using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class UserRepository : BaseRepository, IUserRepository
    {

        public User Login(string lang, string uid, string pwd)
        {
            string p_strCryp;
            CommonTools.Cryptography p_Cryp = new CommonTools.Cryptography();


            p_strCryp = p_Cryp.GetMd5Hash(System.Security.Cryptography.MD5.Create(), pwd);


            string sql = " DECLARE @KeyCount int=0 " +
                 " SELECT @KeyCount = COUNT(*) FROM PersonAccess" +
                " IF @KeyCount = 0 OR @UserID = 'ADMIN'" +
                " BEGIN " +
                        "SELECT _secUsers.*,  " +
                "PersonCode,  " +
                "PERSON_NAME as PersonName,  " +
                "Title ,  " +
                "Person.DeptNo, " +
                "Dept.DeptCode, " +
                "DEPT.DeptName, " +
                "Person.Firstname, " +
                "Person.Lastname, " +
                "Person.Phone, " +
                "Person.Email, " +
                "Person.SiteNo, " +
                "_secUserGroups.UserGCode, " +
                "isnull(Dept.FlagSection, 'F') as FlagSection " +
                " FROM _secUsers LEFT JOIN Person on Person.PERSONNO = _secUsers.PersonNo " +
                " LEFT JOIN Dept ON Person.DEPTNO = Dept.DEPTNO " +
                " LEFT JOIN _secUserGroups ON _secUserGroups.GroupNo = _secUsers.GroupNo " +
                " WHERE UserID = @UserID AND Passwd = @Password " +
                " END " +
                " ELSE " +
                " BEGIN " +

                        " SELECT _secUsers.*,  " +
                        "PERSONCODE,  " +
                        "PERSON_NAME,  " +
                        "TITLE ,  " +
                        "Person.DEPTNO, " +
                        "Dept.DEPTCODE, " +
                        "DEPT.DEPTNAME, " +
                        "Person.FIRSTNAME, " +
                        "Person.LASTNAME, " +
                        "Person.PHONE, " +
                        "Person.EMAIL, " +
                        "Person.SiteNo, " +
                        "_secUserGroups.UserGCode, " +
                        "isnull(Dept.FlagSection, 'F') as FlagSection" +
                        " FROM _secUsers LEFT JOIN Person on Person.PERSONNO = _secUsers.PersonNo " +
                        " INNER JOIN PersonAccess ON _secUsers.PersonAccessNo = PersonAccess.[No]" +
                        " LEFT JOIN Dept ON Person.DEPTNO = Dept.DEPTNO " +
                        " LEFT JOIN _secUserGroups ON _secUserGroups.GroupNo = _secUsers.GroupNo " +
                        " WHERE UserID = @UserID AND Passwd = @Password " +
                 " END";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserID", uid);
            parameters.Add("@Password", p_strCryp);

            User user = SqlMapper.Query<User>(con, sql, parameters, commandType: Text).FirstOrDefault();
            user.Password = p_strCryp;
            return user;

        }

        public User GetByID(int personNo)
        {


            string sql = " SELECT    Person.PERSONNO, Person.PERSONCODE, Person.FIRSTNAME, Person.LASTNAME,Person.PERSON_NAME,Person.FIRSTNAME + ' ' + Person.LASTNAME AS PersonName, "
                      + " Person.DEPTNO, Dept.DEPTCODE,Dept.DEPTNAME, "
                      + " Person.TITLENO, Title.TITLECODE, Title.TITLENAME, Person.CRAFTNO, Craft.CRAFTCODE,  "
                      + " Craft.CRAFTNAME, Person.RATE, Person.CREWNO, Crew.CREWCODE, Crew.CREWNAME, Person.PersonApproveNo,  "
                      + " Person_2.PERSONCODE AS PersonApproveCode, Person_2.FIRSTNAME AS PersonApproveFirstName, "
                      + " Person.FLAGDEL,Person.PHONE,Person.EMAIL,Person.Host,Person.Port,Person.EnableSsl,  "
                      + " Person.TITLE, Person.VendorNo,Vendor.VENDORCODE,Vendor.VENDORNAME,  "
                      + " Dept.FlagSection  "
                      + "  FROM         Person LEFT OUTER JOIN  "

                      + "   Person AS Person_2 ON Person.PersonApproveNo = Person_2.PERSONNO  "
                      + " LEFT OUTER JOIN  "
                      + "     Person AS Person_1 ON Person.UPDATEUSER = Person_1.PERSONNO LEFT OUTER JOIN  "
                      + "  Crew ON Person.CREWNO = Crew.CREWNO LEFT OUTER JOIN  "
                      + "  Craft ON Person.CRAFTNO = Craft.CRAFTNO LEFT OUTER JOIN  "
                      + "   Dept ON Person.DEPTNO = Dept.DEPTNO LEFT OUTER JOIN  "
                      + "   Title ON Person.TITLENO = Title.TITLENO LEFT JOIN  "

                      + "   Vendor ON Person.VendorNo = Vendor.VENDORNO "
                      + " WHERE Person.PERSONNO = " + personNo;


            User user = SqlMapper.Query<User>(con, sql, commandType: Text).FirstOrDefault();

            return user;

        }
    }
}
