﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class WORepository : BaseRepository, IWORepository
    {

        public IEnumerable<WO> Get(int DeptNo, string FlagSection, int PersonNo, string PersonCode, string Where, int PUNo, int EQNo)
        {
            DynamicParameters parameters = new DynamicParameters();
            string p_strWhere = "  where WOSTATUSCODE<95 and PERSON_SITE.PERSONNO = " + PersonNo;
  
            if (InputVal.ToString(Where) != "")
            {
                p_strWhere += string.Format(" AND (WO.WOCode like '%{0}%' OR PU.PUCODE like '%{0}%' OR PU.PUNAME like '%{0}%' " +
                    " OR site.SiteCode like '%{0}%' OR WO_PROBLEM like '%{0}%' OR person_assign.PERSONCODE like '%{0}%' OR person_assign.PERSON_NAME like '%{0}%'" +
                    " OR WOStatus.WOSTATUSNAME like '%{0}%' OR WOStatus.WOSTATUSCODE like '%{0}%'  )", Where);
            }
            if (InputVal.ToInt(PUNo) != 0)
            {
                parameters.Add("@PUNo", PUNo);
                p_strWhere += " AND WO.PUNo = @PUNo";
            }
            if (InputVal.ToInt(EQNo) != 0)
            {
                parameters.Add("@EQNo", EQNo);
                p_strWhere += " AND WO.EQNo = @EQNo";
            }



            string sql = "select top 30 " +
            " wo.WONo , " +
            " WOCODE as WOCode, " +
            " PUCODE + ':' + PUNAME as PU," +
            " wo.SiteNo,site.SiteCode, " +
            " 'WO' as DOCTYPE ," +
            " WO_PROBLEM," +
            " person_assign.PERSONCODE ," +
            " person_assign.PERSON_NAME ," +
            " SCH_START_D as Sch_Start_D ," +
            " SCH_FINISH_D ," +
            " DATEDIFF(day, wo.wodate, GETDATE()) DFR ," +
            " WOStatus.WOSTATUSCODE + '-' + WOStatus.WOSTATUSNAME WOStatusDesc, " +
            " ( select top 1 case  when  Attach_Doc.DOCNO is not null then 1 else 0 end from Attach_Doc where DOCNO = wo.WONo) as HasAttachFile " +
            " from wo " +
            " left join WOStatus on wo.WOSTATUSNO = WOStatus.WOSTATUSNO " +
            " left join PU on wo.PUNO = pu.PUNO " +
            " left join Site on wo.SiteNo = site.SiteNo " +
            " left join person_site on person_site.siteno = wo.siteno " +
            " left join person on person_site.personno = person.personno " +
            " left join Person as person_assign on wo.ASSIGN = person_assign.PERSONNO " +
            " left join _secUsers on person.personno = _secUsers.personno " +
              p_strWhere +
              " order by WODATE desc, WOCode desc ";

            return SqlMapper.Query<WO>(con, sql, parameters, commandType: Text).ToList();

        }

        public WO Get(int id)
        {
            string sql = " Select " +
            " WO.WONO, " +
            " WO.WOCode, " +
            " isnull(WOStatus.wostatuscode + '-' + WOStatus.wostatusname, '') as WOStatusDesc, " +
            " nodename as WFStatusDesc, " +
            " (select top 1 WFTrackeds.Event_Date from WFTrackeds where WFTrackeds.DocNo = WO.WONO order by Event_Order desc) as Event_Date, " +
            " (select top 1 WFTrackeds.Event_Time from WFTrackeds where WFTrackeds.DocNo = WO.WONO order by Event_Order desc) as Event_Time, " +
            " (select top 1 Person.PERSON_NAME from WFTrackeds left join Person " +
            "      on WFTrackeds.From_PersonNo = Person.PERSONNO " +
            "     where WFTrackeds.DocNo = WO.WONO order by Event_Order desc) as WF_FROM_PERSON_NAME, " +
            " site.SiteCode, " +
           "  WOProblem.WOPROBLEMNAME as ProblemName, " +
           "  WO.WODate, " +
            "  WO.WOTime, " +
           "  WO.WRDATE, " +
            "  WO.WRTime, " +
           "  WO.WO_PROBLEM, " +
            " WO.WO_CAUSE, " +
            " EQ.EQCODE, " +
            " EQ.EQNAME, " +
            " WO.FlagPU, " +
            " WO.FlagEQ, " +
           "  WoType.WoTypeCode, " +
           "  WoType.WoTypeName, " +
           "  PU.PUCode, " +
            " PU.PUName, " +
           "  DeptREQ.DEPTCODE AS DeptREQCode, " +
           "  DeptREQ.DEPTNAME AS DeptREQName, " +
           "  WO.RequesterName, " +
            " WO.Sch_Start_D, " +
           "  WO.Sch_Start_T, " +
           " WO.Sch_Finish_D, " +
           "  WO.Sch_Finish_T, " +
           "  WO.Sch_Duration, " +
           "  isnull(wo.PlanMHAmountOfQty, 0) as PlanMHAmountOfQty,  " +
           "  WO.Act_Start_D, " +
           "  WO.Act_Start_T, " +
           "  WO.Act_Finish_D, " +
           "  WO.Act_Finish_T, " +
            " WO.Act_Duration, " +
            " WO.METERDONE, " +
           "  isnull(ActMHAmountOfCost, 0) as ActMHAmountOfCost, " +
           "  (SELECT ISNULL(SUM(WR.AMOUNT), 0)  FROM WO_RESOURCE WR WHERE(RESCSUBTYPE = 'S') AND WR.DirectPurchase = 'F' AND WR.WONO = WO.WONO AND WR.FLAGACT = 'T') StockAmt, " +
           "  (SELECT ISNULL(SUM(WR.AMOUNT), 0)  FROM WO_RESOURCE WR WHERE(RESCSUBTYPE = 'S') AND WR.DirectPurchase = 'T' AND WR.WONO = WO.WONO AND WR.FLAGACT = 'T') DireactPurchaseAmt, " +
          "   (SELECT ISNULL(SUM(WR.AMOUNT), 0)  FROM WO_RESOURCE WR WHERE(RESCSUBTYPE = 'V') AND WR.WONO = WO.WONO AND WR.FLAGACT = 'T') ServiceAmt, " +
          "   (SELECT ISNULL(SUM(WR.AMOUNT), 0)  FROM WO_RESOURCE WR WHERE(RESCSUBTYPE = 'T') AND WR.WONO = WO.WONO AND WR.FLAGACT = 'T') ToolAmt, " +
          "   (SELECT ISNULL(SUM(WR.QTYHOURS), 0)  FROM WO_RESOURCE WR WHERE(RESCTYPE = 'L')  AND WR.WONO = WO.WONO AND WR.FLAGACT = 'T') ActMHAmountOfQty, " +
          "   EQFailures.EQCODE as EQFailuresCode,  " +
          "   EQFailures.EQNAME as EQFailuresName,  " +
           "  WO_Failures.Component, " +
           "  WOProblem.WOPROBLEMCODE as ProblemCode, " +
          "   WOAction.WOACTIONCODE, " +
          "   WOAction.WOACTIONNAME, " +
           "  WO_Failures.WOActionDesc, " +
           "  FailureModeCode, " +
           "  FailureModeName, " +
           "  WO.DT_Start_D, " +
          "   WO.DT_Start_T, " +
          "   WO.DT_Finish_D, " +
           "  WO.DT_Finish_T, " +
           "  isnull(WO.DT_Duration, 0) as DT_Duration, " +
          "   PuEffect.PUCODE AS PuEffectCode, " +
           "  PuEffect.PUNAME AS PuEffectName, " +
            "  WO.WorkBy , " +
             " isnull(WO.TaskProcedure,'') as TaskProcedure, " +
          " PSWorkBy.Person_Name as workByName, PSWorkBy.PersonCode as workbyCode " +
           "  from WO " +
              "   left join wf_node on wf_node.nodeno = wo.wfnodeno " +
              "   left Join WoType on WO.WotypeNo = WOType.WotypeNo " +
               "  left Join WoSubType on WO.WoSubTypeNo = WoSubType.WoSubTypeNo " +
                " left Join CostCenter on Wo.CostCenterNo = CostCenter.CostCenterNo " +
                " left Join Dept on Wo.DeptNo = Dept.DeptNo " +
               "  left join Dept AS DeptREQ ON WO.DEPT_REQ = DeptREQ.DEPTNO " +
              "   left Join WoPriority on WO.PriorityNo = WOPriority.PriorityNo " +
               "  left Join Vendor on WO.VendorNo = Vendor.VendorNo " +
              "   left Join Person PSPlan on WO.Assign = PSPlan.PersonNo " +
             "    left Join Person PSWorkBy on WO.WorkBy = PSWorkBy.PersonNo " +
             "    left Join Person PSCompl on WO.COMPLETEUSER = PSCompl.PersonNo " +
             "    left Join Person PSAccep on WO.ACCEPTUSER = PSAccep.PersonNo " +
             "    left Join Person PSMove on WO.MOVEBY = PSMove.PersonNo " +
             "    left Join Person PSCanc on WO.CANCELUSER = PSCanc.PersonNo " +
            "     left Join Person PSRec on wo.Receiver = PSRec.PERSONNO " +
            "     left Join Person PSRecordBy on WO.RecordBy = PSRecordBy.PERSONNO " +
             "    left Join PU ON WO.PUNo = PU.PUNo " +
            "     left Join PULOCTYPE ON PU.PUNo = PULOCTYPE.PULOCTYPENo " +
            "     left Join EQ ON WO.EQNo = EQ.EQNo " +
              "   left Join EQTYPE ON WO.EQTYPENo = EQTYPE.EQTYPENo " +
             "    left Join EQCritical on EQ.EQCriticalNo = EQCritical.EQCriticalNo " +
            "     left Join PM ON WO.PMNo = PM.PMNo " +
            "     left join PU AS PuEffect ON WO.PUNO_Effected = PuEffect.PUNO " +
             " left join WOStatus on WO.WOSTATUSNO = WOStatus.WOSTATUSNO " +
            "     left join WRUrgent ON wo.UrgentNo = WRUrgent.WRURGENTNO " +
             "   left join MeterM on WO.MeterNo = MeterM.MeterNo " +
             "   left join[Procedure] on wo.ProcedureNo =[Procedure].ProcedureNo " +
            "    left join Symptoms ON WO.SymptomNo = Symptoms.SymptomNo " +
            "    left join Site ON WO.SiteNo = Site.SiteNo " +
            "    left join WO_Failures on WO_Failures.WONo = WO.WONO " +
             "   left join EQ as EQFailures on EQFailures.EQNO = WO_Failures.EQNo " +
             "   left join WOAction on WOAction.WOACTIONNO = WO_Failures.WOActionNo " +
            "    left join WOProblem on WOProblem.WOPROBLEMNO = WO_Failures.ProblemNo " +
             "   left join FailureModes on FailureModes.FailureModeNo = WO_Failures.FailureModeNo " +
            "    where wo.wono = " + id;

            WO WO = SqlMapper.Query<WO>(con, sql, commandType: Text).FirstOrDefault();

            sql = @"select CAST(UNIQUENAME as nvarchar(100)) +''+EXTENSION as FileName, DOCNAME from Attach_Doc WHERE FormID='WO01' AND DOCNO=" + id;
            IEnumerable<WO.AttachFileObject> attachFileResult = SqlMapper.Query<WO.AttachFileObject>(con, sql, commandType: Text).ToList();
            WO.AttachFileObj = attachFileResult;
            return WO;

        }

        public WO GetPlanTime(int id)
        {
            string sql = " Select * from wo where wono = " + id;
            return SqlMapper.Query<WO>(con, sql, commandType: Text).FirstOrDefault();
        }
        public void UpdatePlanTime(int id, string startDate, string endDate)
        {
            double duration = 0;
            DateData DateData = new DateData();
            if (startDate != "")
            {
                CommonTools.InputVal.ConvertStartDateToString(startDate, ref DateData);
            }
            else
            {
                DateData.StartD = null;
                DateData.StartT = null;
            }

            if (endDate != "")
            {
                CommonTools.InputVal.ConvertFinshDateToString(endDate, ref DateData);
                duration = CommonTools.InputVal.CalculateDuration(DateData.StartDate, DateData.FinishDate);
            }
            else
            {
                DateData.FinishD = null;
                DateData.FinishT = null;
                duration = 0;
            }

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@SCH_START_D", DateData.StartD);
            parameters.Add("@SCH_START_T", DateData.StartT);
            parameters.Add("@SCH_FINISH_D", DateData.FinishD);
            parameters.Add("@SCH_FINISH_T", DateData.FinishT);
            parameters.Add("@SCH_DURATION", duration);
            parameters.Add("@WONO", id);

            string sql = "   update wo " +
           " set SCH_START_D = @SCH_START_D, " +
           " SCH_START_T = @SCH_START_T , " +
           " SCH_FINISH_D = @SCH_FINISH_D , " +
           " SCH_FINISH_T = @SCH_FINISH_T , " +
           " SCH_DURATION = @SCH_DURATION   " +
           " where WONO = @WONO ";

            SqlMapper.Execute(con, sql, parameters, commandType: Text);
        }

        public IEnumerable<JobTracking> GetFlowHistory(int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@DocNo", id);
            parameters.Add("@WFType", "WO");

            return SqlMapper.Query<JobTracking>(con, "sp_WF_SendToHist_Retrive", parameters, commandType: StoredProcedure).ToList();
        }

        public WO GetFailure(int WONo)
        {
            string sql = " select * " +
            " from WO_Failures left join WOProblem " +
            " on WO_Failures.ProblemNo = WOProblem.WOPROBLEMNO " +
            " left " +
            " join WOAction " +
            " on WO_Failures.WOActionNo = WOAction.WOACTIONNO " +
            " left " +
            " join FailureModes " +
            " on FailureModes.FailureModeNo = WO_Failures.FailureModeNo " +
            " where WO_Failures.FLAGDEL = 'F' and WO_Failures.WONo= " + WONo;
            Failure Failure = SqlMapper.Query<Failure>(con, sql, commandType: Text).FirstOrDefault();

            WO WO = Get(WONo);
            if (Failure != null)
                WO.FailureObj = Failure;
            return WO;
        }

        public IEnumerable<Manhours> GetManhours(int WONo)
        {
            string sql = " select WO_Resource.*, Person.PERSONNO, Person.PERSON_NAME, Person.PERSONCODE " +
            " from WO_Resource left join Person " +
            " on Person.PERSONNO = WO_Resource.PersonNo " +
            " where RESCTYPE = 'L' and FLAGACT = 'T' " +
            " and WO_Resource.WONO = " + WONo;
            return SqlMapper.Query<Manhours>(con, sql, commandType: Text).ToList();
        }

        public void UpdateManhours(int WORescNo, string TRDatetime, float hours, int personNo)
        {

            CommonTools.DateData DateData = new CommonTools.DateData();

            if (TRDatetime != "" && TRDatetime != null)
            {
                CommonTools.InputVal.ConvertStartDateToString(TRDatetime, ref DateData);
            }

            string sql = " update WO_Resource " +
            " set TRDATE = @TRDATE" +
            ", TRTime = @TRTime " +
            ", PersonNo = @PersonNo" +
            ", QTYHOURS = @QTYHOURS " +
            //", Amount = @Amount " +
            ", Hours = @Hours" +
            ", UPDATEUSER = @UPDATEUSER " +
            ", UPDATEDATE =  @UPDATEDATE " +
            " where WO_Resource.WO_RESCNO = @WORESCNO";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@TRDATE", DateData.StartD);
            parameters.Add("@TRTime", DateData.StartT);
            parameters.Add("@QTYHOURS", CommonTools.FieldDateTime.ConvertDurationToMinute(hours));
            parameters.Add("@Hours", CommonTools.FieldDateTime.ConvertDurationToMinute(hours));
            parameters.Add("@UPDATEUSER", personNo);
            parameters.Add("@UPDATEDATE", CommonTools.FieldDateTime.DateToString(DateTime.Now));
            parameters.Add("@WORESCNO", WORescNo);
            parameters.Add("@PersonNo", personNo);

            SqlMapper.Execute(con, sql, parameters, commandType: Text);
        }
        public IEnumerable<Material> GetMaterial(int WONo)
        {
            string p_strWhere = "";
            p_strWhere = " WHERE WO_Resource.RESCTYPE='P' AND ";
            p_strWhere += " WO_Resource.WONO=" + WONo + " AND ";
            p_strWhere += " WO_Resource.FLAGACT='T' ORDER BY OTHERCODE";

            string sql = " SELECT WO_Resource.WONO, " +
                " WO_Resource.DirectPurchase, " +
                " WO_Resource.OTHERCODE, " +
                " WO_Resource.NAME as OTHERName, WO_Resource.WO_RESCNO as No, WO_Resource.OPNO, " +
                " Store.StoreNo, Store.StoreCode, Store.StoreName, " +
                " WO_Resource.PARTNO, IV_Catalog.PartCode, IV_Catalog.PartName, WO_Resource.PartTypeCode, " +
                " WO_Resource.TRDATE, WO_Resource.TRTime, " +
                " WO_Resource.QTY,WO_Resource.UNITCOST,WO_Resource.AMOUNT, " +
                " WO_Resource.ExpNo,ExpenseType.ExpCode,ExpenseType.ExpName, " +
                " WO_Resource.REMARK,WO_Resource.RESCTYPE,WO_Resource.RESCSubTYPE, " +
                " WO_Task.TaskNo , WO_Task.TaskCode, WO_Task.TaskName " +
                " From dbo.WO_Resource " +
                " left Join WO on WO_Resource.WONO = WO.WONo " +
                " left Join Store on WO_Resource.StoreNo = Store.StoreNo " +
                " left Join IV_Catalog on WO_Resource.Partno = IV_Catalog.Partno " +
                " left Join ExpenseType on WO_Resource.ExpNo = ExpenseType.ExpNo " +
                " left join WO_Task on WO_Task.TaskNo = WO_Resource.OPNO " +
                p_strWhere;

            return SqlMapper.Query<Material>(con, sql, commandType: Text).ToList();
        }
        public Material GetMaterialByWORescNo(int WO_RescNo)
        {
            string p_strWhere = "";
            p_strWhere += " WHERE WO_Resource.WO_RESCNO=" + WO_RescNo;

            string sql = " SELECT WO_Resource.WONO, " +
                " WO_Resource.DirectPurchase, WO_Resource.Unit as NonStockUnitName, " +
                " WO_Resource.OTHERCODE, WO_Resource.FlagNotPartCode, " +
                " WO_Resource.NAME as OTHERName, WO_Resource.WO_RESCNO as No, WO_Resource.OPNO, " +
                " Store.StoreNo, Store.StoreCode, Store.StoreName, " +
                " WO_Resource.PARTNO, IV_Catalog.PartCode, IV_Catalog.PartName, WO_Resource.PartTypeCode, " +
                " WO_Resource.TRDATE, WO_Resource.TRTime, " +
                " WO_Resource.QTY,WO_Resource.UNITCOST,WO_Resource.AMOUNT, " +
                " WO_Resource.ExpNo,ExpenseType.ExpCode,ExpenseType.ExpName, " +
                " WO_Resource.REMARK,WO_Resource.RESCTYPE,WO_Resource.RESCSubTYPE, " +
                " WO_Task.TaskNo, WO_Task.TaskCode, WO_Task.TaskName " +
                " From dbo.WO_Resource " +
                " left Join WO on WO_Resource.WONO = WO.WONo " +
                " left Join Store on WO_Resource.StoreNo = Store.StoreNo " +
                " left Join IV_Catalog on WO_Resource.Partno = IV_Catalog.Partno " +
                " left Join ExpenseType on WO_Resource.ExpNo = ExpenseType.ExpNo " +
                " left join WO_Task on WO_Task.TaskNo = WO_Resource.OPNO " +
                p_strWhere;

            return SqlMapper.Query<Material>(con, sql, commandType: Text).FirstOrDefault();
        }
        public void InsertMaterial(Material Material)
        {
            DateData DateData = new DateData();
            if (Material.TrDate != "")
            {
                CommonTools.InputVal.ConvertStartDateToString(Material.TrDate, ref DateData);
            }
            else
            {
                DateData.StartD = null;
                DateData.StartT = null;
            }

            string sql = "sp_WOReSourceMT_Insert";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@WONO", Material.WONO);
            parameters.Add("@OPNO", ValDB.GetInt(Material.TaskNo));
            parameters.Add("@SymptomNo", ValDB.GetInt(Material.SymptomNo));
            parameters.Add("@STORENO", ValDB.GetInt(Material.STORENO));
            parameters.Add("@PARTNO", ValDB.GetInt(Material.PARTNO));
            parameters.Add("@OTHERCODE", ValDB.GetStringNull(Material.OtherCode));
            parameters.Add("@NAME", ValDB.GetStringNull(Material.OtherName));
            parameters.Add("@PartTypeCode", ValDB.GetStringNull(Material.PartTypeCode));
            parameters.Add("@TRDATE", DateData.StartD);
            parameters.Add("@TRTime", DateData.StartT);
            parameters.Add("@QTY", Material.QTY);
            parameters.Add("@UNITCOST", Material.UNITCOST);
            parameters.Add("@AMOUNT", Material.AMOUNT);
            parameters.Add("@VendorNo", ValDB.GetInt(Material.VendorNo));
            parameters.Add("@EXPNO", ValDB.GetInt(Material.EXPNO));
            parameters.Add("@REMARK", ValDB.GetStringNull(Material.Remark));
            parameters.Add("@FLAGACT", "T");
            parameters.Add("@RESCTYPE", "P");
            parameters.Add("@DirectPurchase", Material.DirectPurchase);
            parameters.Add("@FlagNotPartCode", Material.FlagNotPartCode);
            parameters.Add("@Unit", ValDB.GetStringNull(Material.NonStockUnitName));
            parameters.Add("@UPDATEUSER", Material.UPDATEUSER);
            parameters.Add("@WO_RESCNO", dbType: DbType.Int32, direction: ParameterDirection.Output);


            SqlMapper.Execute(con, sql, parameters, commandType: StoredProcedure);
        }

        public void UpdateMaterial(Material Material)
        {
            DateData DateData = new DateData();
            if (Material.TrDate != "")
            {
                CommonTools.InputVal.ConvertStartDateToString(Material.TrDate, ref DateData);
            }
            else
            {
                DateData.StartD = null;
                DateData.StartT = null;
            }

            string sql = "sp_WOReSourceMT_Update";

            DynamicParameters parameters = new DynamicParameters();

            parameters.Add("@WO_RESCNO", Material.No);
            parameters.Add("@WONO", Material.WONO);
            parameters.Add("@OPNO", ValDB.GetInt(Material.TaskNo));
            parameters.Add("@SymptomNo", ValDB.GetInt(Material.SymptomNo));
            parameters.Add("@STORENO", ValDB.GetInt(Material.STORENO));
            parameters.Add("@PARTNO", ValDB.GetInt(Material.PARTNO));
            parameters.Add("@OTHERCODE", ValDB.GetStringNull(Material.OtherCode));
            parameters.Add("@NAME", ValDB.GetStringNull(Material.OtherName));
            parameters.Add("@TRDATE", DateData.StartD);
            parameters.Add("@TRTime", DateData.StartT);
            parameters.Add("@PartTypeCode", ValDB.GetStringNull(Material.PartTypeCode));
            parameters.Add("@QTY", Material.QTY);
            parameters.Add("@UNITCOST", Material.UNITCOST);
            parameters.Add("@AMOUNT", Material.AMOUNT);
            parameters.Add("@VendorNo", ValDB.GetInt(Material.VendorNo));
            parameters.Add("@EXPNO", ValDB.GetInt(Material.EXPNO));
            parameters.Add("@REMARK", ValDB.GetStringNull(Material.Remark));
            parameters.Add("@FLAGACT", "T");
            parameters.Add("@RESCTYPE","P");
            parameters.Add("@DirectPurchase", Material.DirectPurchase);
            parameters.Add("@FlagNotPartCode", Material.FlagNotPartCode);
            parameters.Add("@Unit", ValDB.GetStringNull(Material.NonStockUnitName));
            parameters.Add("@UPDATEUSER", Material.UPDATEUSER);

           
            SqlMapper.Execute(con, sql, parameters, commandType: StoredProcedure);
        }
        public void DeleteMaterial(int WORescNo)
        {
            string sql = "sp_WOResourceMH_Delete";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@WO_RESCNO", WORescNo);
            SqlMapper.Execute(con, sql, parameters, commandType: StoredProcedure);
        }

        public int CheckPincode(int WONo, string Pincode)
        {
            string sql = "  select Person.PersonNo " +
                " from wo " +
                " inner join PU on PU.PUNo = wo.PUNo" +
                " inner join Person on Person.PERSONNO = pu.Person_Email" +
                " inner join _secUsers on _secUsers.PersonNo = Person.PERSONNO " +
                " where wo.WONo = @WONo and _secUsers.Passwd= @Pincode";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@WONo", WONo);
            parameters.Add("@Pincode", Pincode);

            return SqlMapper.Query<int>(con, sql, parameters, commandType: Text).FirstOrDefault();
        }
        public int InsertSignout(string PUCode, bool FlagCheckJob, string WOCode, int WONo, int UpdatedBy, string Remark)
        {
            string sql = " declare @pucode nvarchar(500) " +
                " select @pucode = puno from wo where wono = @WONo " +
                " insert into TimeStamp values(getdate(), @pucode, @FlagCheckJob, @WOCode, @WONo, @UpdatedBy, @Remark)";
            DynamicParameters parameters = new DynamicParameters();
          
            parameters.Add("@FlagCheckJob", FlagCheckJob);
            parameters.Add("@WOCode", WOCode);
            parameters.Add("@WONo", WONo);
            parameters.Add("@UpdatedBy", UpdatedBy);
            parameters.Add("@Remark", Remark);

            return SqlMapper.Execute(con, sql, parameters, commandType: Text);
        }
    }
}
