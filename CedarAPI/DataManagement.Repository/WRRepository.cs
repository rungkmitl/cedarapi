﻿using CommonTools;
using Dapper;
using DataManagement.Entities;
using DataManagement.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using static System.Data.CommandType;

namespace DataManagement.Repository
{
    public class WRRepository : BaseRepository, IWRRepository
    {

        public IEnumerable<WR> Get(int DeptNo, string FlagSection, int PersonNo, string PersonCode, string Where, int PUNo, int EQNo)
        {
            DynamicParameters parameters = new DynamicParameters();
            //parameters.Add("@PersonNo", PersonNo);
            //parameters.Add("@DeptNo", DeptNo);

            string p_strWhere = "";
            //if (DeptNo == 0 || PersonCode.ToLower() == "admin")
            //{
            //    p_strWhere = " where Person_Site.PERSONNO = @PersonNo ";
            //}
            //else if (DeptNo > 0 && FlagSection == "F")
            //{
            //    p_strWhere = " where Person_Site.PERSONNO = @PersonNo and PU.DEPTNO = @DeptNo ";
            //}
            //else if (DeptNo > 0 && FlagSection == "T")
            //{
            //    p_strWhere = " where Person_Site.PERSONNO = @PersonNo and wr.DEPT_REC = @DeptNo ";
            //}

            p_strWhere = " where WRSTATUSCODE < 95 AND PERSON_SITE.PERSONNO = " + PersonNo;
            if (InputVal.ToString(Where) != "")
            {
               
                p_strWhere += string.Format(" AND (WR.WRCode like '%{0}%' OR wr.WRDESC like '%{0}%' OR wrs.WRSTATUSCODE like '%{0}%' " +
                   " OR wrs.WRSTATUSNAME like '%{0}%' OR s.SiteCode like '%{0}%' OR pu.PUCODE like '%{0}%' OR pu.PUNAME like '%{0}%'" +
                   " OR Dept.DEPTCODE like '%{0}%' OR Dept.DEPTNAME like '%{0}%'  )", Where);

                //p_strWhere += " AND WR.WRCode like '%" + Where + "%'";
            }
            if (InputVal.ToInt(PUNo) != 0)
            {
                parameters.Add("@PUNo", PUNo);
                p_strWhere += " AND WR.PUNo = @PUNo";
            }
            if (InputVal.ToInt(EQNo) != 0)
            {
                parameters.Add("@EQNo", EQNo);
                p_strWhere += " AND WR.EQNo = @EQNo";
            }

            string sql = " select top 30 wr.WRNO, wr.WRCODE " +
                " , wr.WRDESC " +
                " , case when wrs.WRSTATUSCODE is null then '' else wrs.WRSTATUSCODE + '-' + wrs.WRSTATUSNAME end as WRStatus " +
                " , s.SiteCode " +
                " , case when pu.PUCODE is null then '' else pu.PUCODE + '-' + pu.PUNAME end as PUName " +
                " , case when Dept.DEPTCODE is null then '' else Dept.DEPTCODE + '-' + Dept.DEPTNAME end as DeptName " +
                " , wr.WRTIME, wr.WRDATE, " +
                 " ( select top 1 case  when  Attach_Doc.DOCNO is not null then 1 else 0 end from Attach_Doc where DOCNO = wr.WRNo) as HasAttachFile " +
                " from wr " +
                " left join WRStatus wrs on wr.WRSTATUSNO = wrs.WRSTATUSNO " +
                " left join Site s on s.SiteNo = wr.SiteNo " +
                " left join PU on PU.PUNO = wr.puno " +
                " left join Dept on Dept.DEPTNO = wr.DEPT_REQ " +
                " left join Person_Site on Person_Site.SiteNO = wr.SiteNo " +
                p_strWhere +
                " order by wr.WRDATE desc,wr.WRCode desc ";

            return SqlMapper.Query<WR>(con, sql, parameters, commandType: Text).ToList();

        }

        public WR GetByID(int WRNo, int PersonNo)
        {
            string p_strSql = @"select wrno,wrcode,wrdate,wrdesc,wrtime
	                                    ,isnull(wrstatuscode+'  '+wrstatusname,'') as wrstatus
	                                    ,nodename
	                                    ,wr.puno,pucode,puname
	                                    ,wr.eqno,eqcode,eqname
	                                    ,wr.costcenterno,costcentercode,costcentername
                                        ,wr.siteno,isnull(sitecode+' - '+sitename,'') as sitename, sitecode
                                        ,wr.dept_rec,deptcode
                                    from wr 
                                    left join wrstatus on wrstatus.wrstatusno = wr.wrstatusno
                                    left join wf_node on wf_node.NODENO = wr.wfnodeno
                                    left join pu on pu.puno = wr.puno
                                    left join eq on eq.eqno = wr.eqno
                                    left join costcenter on costcenter.costcenterno = wr.costcenterno
                                    left join site on wr.siteno = site.siteno
                                    left join dept on wr.dept_rec = dept.deptno
                                    WHERE WRNO=" + WRNo;

            WR wrResult = SqlMapper.Query<WR>(con, p_strSql, commandType: Text).FirstOrDefault();


            p_strSql = @"select WF_NODE_ACTION.ACTIONNO,WF_NODE_ACTION.ACTIONNAME,WF_NODE_ACTION.PRE_ACTION from wr
                                    LEFT JOIN WF_NODE_ACTION ON WR.WFNODENO=WF_NODE_ACTION.NODENO and ACTIONNO is not null AND FLAG_TRIGGER='F'
                                    left join WFTrackeds on docno = " + WRNo
                               + " left join USERGROUP_MEMBER on PERSON=" + PersonNo + " and WFTrackeds.Receive_UserGroupNo=USERGROUPNO"
                               + " WHERE WRNO=" + WRNo
                               + " and (WFTrackeds.Receive_PersonNo = " + PersonNo + " OR USERGROUP_MEMBER.PERSON is not null)"
                               + " group by WF_NODE_ACTION.ACTIONNO,WF_NODE_ACTION.ACTIONNAME,WF_NODE_ACTION.PRE_ACTION ";

            IEnumerable<ActionObject> actionResult = SqlMapper.Query<ActionObject>(con, p_strSql, commandType: Text).ToList();

            wrResult.ACTION = new List<ActionObject>();

            foreach (ActionObject item in actionResult)
            {
                ActionObject actionObj = item;
                int actionNo = InputVal.ToInt(actionObj.ACTIONNO);
                if (actionNo != 0)
                {
                    wrResult.ACTION.Add(new ActionObject(actionNo, actionObj.ACTIONNAME, actionObj.PRE_ACTION, this.GetActionSendTo(WRNo, actionNo, PersonNo)));
                }
            }

            wrResult.ATTACHMENT = new List<string>();
            p_strSql = @"select CAST(UNIQUENAME as nvarchar(100)) +''+EXTENSION as FileName from Attach_Doc WHERE FormID='WR01' AND DOCNO=" + WRNo;

            IEnumerable<WR.AttachFileObject> attachFileResult = SqlMapper.Query<WR.AttachFileObject>(con, p_strSql, commandType: Text).ToList();


            foreach (WR.AttachFileObject item in attachFileResult)
            {
                wrResult.ATTACHMENT.Add(item.FileName);
            }

            return wrResult;

        }
        public string GetActionSendTo(int wrno, int actno, int personno)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@DOCNO", wrno);
            parameters.Add("@ACTIONNO", actno);
            parameters.Add("@USERNO", personno);


            ActionObject actionObject = SqlMapper.Query<ActionObject>(con, "sp_WFN_EXEC_ROUTE_GETSENDER_WR", parameters, commandType: StoredProcedure).FirstOrDefault();

            if (actionObject != null && actionObject.ACTIONNO != 0)
            {
                if (actionObject.SENDTO_GROUP == "")
                {
                    return actionObject.SENDTO_PERSON;
                }
                else
                {
                    return actionObject.SENDTO_GROUP;
                }
            }

            return "";
        }
        public int SaveWR(string Date, string Time, int? PUNo, int? EQNo, int? CostcenterNo, string Problem, int SiteNo, int PersonNo, int WRNo, string Attach)
        {
            using (var scope = new TransactionScope())
            {
                DynamicParameters parameters = new DynamicParameters();


                if (WRNo == 0)
                {
                    parameters = new DynamicParameters();
                    parameters.Add("@WRDATE", Date);
                    parameters.Add("@WRTIME", Time);
                    parameters.Add("@PUNO", PUNo);
                    parameters.Add("@EQNO", EQNo);
                    parameters.Add("@COSTCENTERNO", CostcenterNo);
                    parameters.Add("@PROBLEM", Problem);
                    parameters.Add("@SiteNo", SiteNo);
                    parameters.Add("@UpdateUser", PersonNo);
                    parameters.Add("@WRNO", WRNo, System.Data.DbType.Int32, System.Data.ParameterDirection.Output);

                    SqlMapper.Query<int>(con, "mobile_WR_insert", parameters, commandType: StoredProcedure);

                    WRNo = parameters.Get<int>("@WRNO");

                    parameters = new DynamicParameters();
                    parameters.Add("@WRNO", WRNo);
                    parameters.Add("@ACTIONNO", 0);
                    parameters.Add("@DESC", "");
                    parameters.Add("@UPDATEUSER", PersonNo);



                    SqlMapper.Execute(con, "sp_WFN_EXEC_NODE_WR", parameters, commandType: StoredProcedure);

                }
                else
                {
                    parameters = new DynamicParameters();
                    parameters.Add("@WRNO", WRNo);
                    parameters.Add("@PUNO", PUNo);
                    parameters.Add("@EQNO", EQNo);
                    parameters.Add("@COSTCENTERNO", CostcenterNo);
                    parameters.Add("@PROBLEM", Problem);
                    parameters.Add("@UpdateUser", PersonNo);

                    SqlMapper.Execute(con, "mobile_WR_update", parameters, commandType: StoredProcedure);
                }



                if (Attach != "" && Attach != null)
                {
                    string[] str = Attach.Split(',');
                    foreach (string s in str)
                    {
                        string p_path = HttpContext.Current.Server.MapPath("~");
                        int path_level = Convert.ToInt32(WebConfigurationManager.AppSettings["PathLevel"]);
                        for (int j = 0; j <= path_level; j++)
                        {
                            p_path = Directory.GetParent(p_path).ToString();
                        }
                        p_path += WebConfigurationManager.AppSettings["attPath"] + "\\";

                        string ext = System.IO.Path.GetExtension(s); ;
                        string uid = s.Replace(ext, "");

                        int id = WRNo;

                        parameters = new DynamicParameters();
                        parameters.Add("@FormID", "WR01");
                        parameters.Add("@DocNo", id);
                        parameters.Add("@UNIQUENAME", uid);
                        parameters.Add("@EXTENSION", ext);
                        parameters.Add("@Site", SiteNo);
                        parameters.Add("@UpdateUser", PersonNo);

                        SqlMapper.Execute(con, "sp_Attach_Mobile_Insert", parameters, commandType: StoredProcedure);
                    }
                   
                }


                scope.Complete();
            }
            return WRNo;
        }
    }
}
